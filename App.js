/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useEffect} from 'react';
import {LogBox, Alert} from 'react-native';
import RootStack from './src/routes/Routes';
import messaging from '@react-native-firebase/messaging';
import Auth from './src/asyncStore/Index';
import {AS_FCM_TOKEN} from './src/res/Constants';
import {Text, View} from 'react-native';
import Indicator from './src/commonView/ActivityIndicator';
import Actions from './src/redux/actions/Actions';
import {connect} from 'react-redux';

const auth = new Auth();

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      showIndicator: false,
    };
    Text.defaultProps = Text.defaultProps || {};
    Text.defaultProps.allowFontScaling = false;
  }

  componentDidMount() {
    this.requestUserPermission();
    this.addFirebaseListner();

    //  this.props.showIndicator();
  }

  componentWillReceiveProps(props) {
    if (props.showIndicator != this.props.showIndicator) {
      this.setState({
        showIndicator: this.props.showIndicator,
      });
    }
  }

  addFirebaseListner = async () => {
    useEffect(() => {
      const unsubscribe = messaging().onMessage(async (remoteMessage) => {
        Alert.alert(
          'A new FCM message arrived!',
          JSON.stringify(remoteMessage),
        );
      });

      return unsubscribe;
    }, []);

    messaging().setBackgroundMessageHandler(async (remoteMessage) => {
      console.log('Message handled in the background!', remoteMessage);
    });

    messaging().onNotificationOpenedApp((remoteMessage) => {
      console.log(
        'Notification caused app to open from background state:',
        remoteMessage.notification,
      );
      // navigation.navigate(remoteMessage.data.type);
    });
    // Check whether an initial notification is available
    messaging()
      .getInitialNotification()
      .then((remoteMessage) => {
        if (remoteMessage) {
          console.log(
            'Notification caused app to open from quit state:',
            remoteMessage.notification,
          );
          // setInitialRoute(remoteMessage.data.type); // e.g. "Settings"
        }
        //  setLoading(false);
      });
  };

  requestUserPermission = async () => {
    const authStatus = await messaging().requestPermission();
    const enabled =
      authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
      authStatus === messaging.AuthorizationStatus.PROVISIONAL;
    await messaging().requestPermission({
      sound: true,
      alert: true,
      provisional: true,
      announcement: true,
    });

    if (enabled) {
      this.getFcmToken(); //<---- Add this
      console.log('Authorization status:', authStatus);
    } else {
      Alert.alert('error1');
    }
  };

  getFcmToken = async () => {
    const fcmToken = await messaging().getToken();
    if (fcmToken) {
      auth.setValue(AS_FCM_TOKEN, fcmToken);
      console.log('Your Firebase Token is:', fcmToken);
    } else {
      console.log('Failed', 'No token received');
    }
  };

  render() {
    LogBox.ignoreAllLogs();
    return (
      <View style={{flex: 1}}>
        <RootStack />
        <Indicator loading={this.state.showIndicator} />
      </View>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    showIndicator: state.reducer.showIndicator,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    showIndicator: () => {
      dispatch(Actions.showIndicator());
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
