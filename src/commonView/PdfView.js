import React, {useState, useEffect} from 'react';
import COLOR from '../res/styles/Color';
import Styles from '../res/styles/Styles';
import {
  View,
  Image,
  SafeAreaView,
  Text,
  TouchableOpacity,
  Linking,
} from 'react-native';
import IMAGES from '../res/styles/Images';

import Pdf from 'react-native-pdf';
import LottieView from 'lottie-react-native';

import {useNavigation} from '@react-navigation/native';
import {widthPercentageToDP} from 'react-native-responsive-screen';
import API from '../api/Api';

let api = new API();

function PdfView(props) {
  const navigation = useNavigation();
  const [showAnim, setShowAnim] = useState(true);
  const [url, setUrl] = useState('');

  useEffect(() => {
    console.log('props.route.params.url', props.route.params.url);
    if (props.route.params.url) {
      setUrl(props.route.params.url);
    } else if (props.route.params.id) {
      generatePdf(props.route.params.id);
    }
  }, [props.route.params.url]);

  const generatePdf = (id) => {
    api
      .generatePdfUrl(id)
      .then((res) => {
        if (res.data.response) {
          if (res.data.response.path) {
            setUrl(res.data.response.path);
            setShowAnim(false);
          }
        } else {
        }
      })
      .then((error) => {
        console.log(error);
      });
  };

  return (
    <View style={[{flex: 1, backgroundColor: COLOR.WHITE}]}>
      <SafeAreaView style={{flex: 1}}>
        <View
          style={{
            flexDirection: 'row',
            alignSelf: 'center',
            alignItems: 'center',
            height: 45,
            backgroundColor: COLOR.DARK_BLUE,
            paddingHorizontal: 15,
            width: widthPercentageToDP(100),
          }}>
          <TouchableOpacity
            onPress={() => {
              navigation.goBack();
            }}
            style={[{marginRight: 15, width: 30}]}>
            <Image
              style={{height: 24, width: 24, borderRadius: 20}}
              source={IMAGES.left_arrow_white}
            />
          </TouchableOpacity>
          <Text
            style={[Styles.subheading_label, {color: COLOR.WHITE, flex: 1}]}>
            Inspection report
          </Text>
          <TouchableOpacity
            onPress={() => {
              if (url) {
                Linking.openURL(url);
              }
            }}>
            <Image
              style={{height: 30, width: 40, borderRadius: 20}}
              source={IMAGES.download}
              resizeMode={'contain'}
            />
          </TouchableOpacity>
        </View>
        <View
          style={{
            flex: 1,
            margin: 0,
          }}>
          {showAnim && (
            <View
              style={{
                alignSelf: 'center',
                height: 250,
                width: 250,
                marginBottom: -200,
                marginTop: 100,
              }}>
              <LottieView
                source={require('../res/styles/31062-car-inspection')}
                autoPlay
                loop
              />
            </View>
          )}

          {url != '' && (
            <Pdf
              style={{flex: 1}}
              trustAllCerts={false}
              source={{uri: url}}
              onLoadComplete={(numberOfPages, filePath) => {
                setShowAnim(false);
                console.log(`Number of pages: ${numberOfPages}`);
              }}
              onPageChanged={(page, numberOfPages) => {
                setShowAnim(false);
                console.log(`Current page: ${page}`);
              }}
              onError={(error) => {
                setShowAnim(false);
                console.log(error);
              }}
              onPressLink={(uri) => {
                setShowAnim(false);
                  if (uri) {
                      Linking.openURL(uri);
                  }
              }}
            />
          )}
        </View>
      </SafeAreaView>
    </View>
  );
}

export default PdfView;
