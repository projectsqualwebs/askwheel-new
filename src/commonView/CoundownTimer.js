import CountDown from 'react-native-countdown-component';
 
export const render = (props) => {
    return (
      <CountDown
        until={100}
        onFinish={() => alert('finished')}
        onPress={() => alert('hello')}
        size={20}
      />
    )
}