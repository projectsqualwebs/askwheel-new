import React from 'react';
import {View, StyleSheet, Modal, TouchableOpacity, Image} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  heightPercentageToDP,
} from 'react-native-responsive-screen';
import Gallery from 'react-native-image-gallery';
import FastImage from 'react-native-fast-image';
import IMAGES from '../res/styles/Images';
import COLOR from '../res/styles/Color';

export default GalleryView = (props) => {
  return (
    <Modal
      animationType={'slide'}
      transparent={false}
      visible={props.modalVisible}
      onRequestClose={() => {
        props.handleGalleryView('1');
        console.log('Modal has been closed.');
        props.handleGalleryIndex();
      }}>
      <View style={styles.modal}>
        <View style={styles.modalBody}>
          <TouchableOpacity
            style={{
              flex: 1,
              alignItems: 'center',
              alignSelf: 'center',
              justifyContent: 'center',
            }}
            onPress={() => props.handleGalleryView('1')}>
            <View
              style={{
                width: wp(100),
                height: heightPercentageToDP(50),

                alignSelf: 'center',
              }}>
              <Gallery
              initialPage={props.index==null?0:props.index}
                style={{
                  flex: 1,
                  backgroundColor: '#ffffff',
                }}
                images={props.data.map((val) => {
                  console.log('val is:-', val);
                  return {
                    source: {uri: val},
                    dimensions: {
                      width: wp(100),
                      height: heightPercentageToDP(50),
                    },
                  };
                })}
              />
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modal: {
    flex: 1,

    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  modalBody: {
    justifyContent: 'center',
    backgroundColor: '#000000cc',
    alignItems: 'center',
    alignSelf: 'center',
    flex: 1,
  },
});

//  handleGalleryView() {
//     this.setState({isGalleryViewVisible: false});
//   }

//   <GalleryView
//           data={this.state.inspectionData.files}
//           handleGalleryView={this.handleGalleryView}
//           modalVisible={this.state.isGalleryViewVisible}
//         />
