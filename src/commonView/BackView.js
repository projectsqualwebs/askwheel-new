import React from 'react';
import {TouchableOpacity, Text, StyleSheet, Image} from 'react-native';
import COLOR from '../res/styles/Color';
import IMAGES from '../res/styles/Images';
import Styles from '../res/styles/Styles';

export const BackView = (props) => {
  return (
    <TouchableOpacity
      onPress={() => props.props.navigation.goBack()}
      style={[Styles.back_arrow, {backgroundColor: COLOR.DARK_BLUE, marginLeft: 10,borderRadius: 25}]}>
      <Image
        style={{height: 25, width: 35}}
        resizeMode={'contain'}
        source={IMAGES.left_arrow_white}
      />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({});

export const DotView = (props) => {
  return (
    <Text
      style={{
        color: props.color ? props.color : COLOR.LIGHT_BLUE,
        fontSize: 12,
      }}>
      ●{'  '}
    </Text>
  );
};
