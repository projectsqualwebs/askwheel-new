import {PermissionsAndroid, Alert, Platform} from 'react-native';
import moment from 'moment';
import RNCountry from 'react-native-countries';

export function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}


export let navigationRef;

export const setNavigationRef = (navigator) => {
  navigationRef = navigator;
};

export function navigate(name, params) {
  navigationRef.navigate(name, params);
}


export async function getDate(timestamp, format) {
  return;
}

export async function requestCameraPermission() {
  //Calling the permission function
  const granted = await PermissionsAndroid.request(
    PermissionsAndroid.PERMISSIONS.CAMERA,
    {
      title: 'Allow Camera',
      message: 'App needs access to your camera to capture images',
    },
  );
  if (granted === PermissionsAndroid.RESULTS.GRANTED) {
    return true;
  } else {
    return false;
  }
}

export async function requestGalleryPermission() {
  //Calling the permission function
  const granted = await PermissionsAndroid.request(
    PermissionsAndroid.PERMISSIONS.requestGalleryPermission,
    {
      title: 'Allow Gallery',
      message: 'App needs access to your gallery to pick images',
    },
  );
  if (granted === PermissionsAndroid.RESULTS.GRANTED) {
    return true;
  } else {
    return false;
  }
}

export const createFormData = (photo, body) => {
  const data = new FormData();

  data.append('photo', {
    name: photo.fileName,
    type: photo.type,
    uri:
      Platform.OS === 'android' ? photo.uri : photo.uri.replace('file://', ''),
  });

  Object.keys(body).forEach((key) => {
    data.append(key, body[key]);
  });

  return data;
};

export const getYears = (key) => {
  const year = new Date().getFullYear();
  var dropdownData = [];

  for (let i = 0; i < 50; i++) {
    dropdownData.push({
      label: String(year - i),
      value: String(year - i),
      id: i,
      key: key,
    });
  }
  return dropdownData;
};

export const getCountries = () => {
  let countryNamesWithCodes = RNCountry.getCountryNames;
  var dropdownData = [];
  for (let i = 0; i < countryNamesWithCodes.length; i++) {
    dropdownData.push({
      label: countryNamesWithCodes[i],
      value: countryNamesWithCodes[i],
      id: i,
      key: 'manufacturingCountry',
    });
  }

  return dropdownData;
};

export const insertComma = (val) => {
  if (Platform.OS === 'android') {
    // only android needs polyfill
    require('intl'); // import intl object
    require('intl/locale-data/jsonp/en-IN'); // load the required locale details
    let num = Intl.NumberFormat('en-IN', {}).format(val);
    return num;
  } else {
    let num = Number(parseFloat(val).toFixed(2)).toLocaleString('en', {
      minimumFractionDigits: 0,
    });
    return num;
  }
};

export const getRemainingTime = (t2) => {
  const t1 = new Date().getTime() / 1000;
  return t1 > t2 ? 0 : t2 - t1;
};
