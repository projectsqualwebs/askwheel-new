/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {StatusBar} from 'react-native';

import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer, DefaultTheme} from '@react-navigation/native';
import {
  View,
  Text,
  TouchableOpacity,
  SafeAreaView,
  Image,
  StyleSheet,
} from 'react-native';
import {widthPercentageToDP} from 'react-native-responsive-screen';
import COLOR from '../res/styles/Color';
import InitialLoadingScreen from '../screens/splash/InitialLoadingScreen';
import SplashScreen from '../screens/splash/SplashScreen';
import IMAGES from '../res/styles/Images';
import HomeScreen from '../screens/tabs/home/HomeScreen';
import AuctionScreen from '../screens/tabs/auction/AuctionScreen';
import StatisticsScreen from '../screens/tabs/statistics/StatisticsScreen';
import SettingScreen from '../screens/tabs/settings/SettingScreen';
import VehicleList from '../screens/tabs/home/VehicleList';
import VehicleDetail from '../screens/vehicles/VehicleDetail';
import BezireLineChart from '../screens/tabs/statistics/LineChart';
import VehicleLeads from '../screens/vehicles/VehicleLead';
import VehicleInspection from '../screens/vehicles/VehicleInspection';
import CarInspection from '../screens/vehicles/CarInspection';
import Exterior from '../screens/vehicles/Exterior';
import MyCars from '../screens/tabs/settings/MyCars';
import CarsCategory from '../screens/tabs/settings/CarsCategory';
import MyDetails from '../screens/tabs/settings/MyDetails';
import ManageAccounts from '../screens/tabs/settings/ManageAccounts';
import UserDetails from '../screens/tabs/settings/UserDetails';
import AddNewUser from '../screens/tabs/settings/AddNewUser';
import AddNewListing from '../screens/tabs/settings/AddNewListing';
import ForgetPassword from '../screens/splash/ForgetPassword';
import PdfView from "../commonView/PdfView";
import {setNavigationRef} from "../commonView/Helpers";

const BottonStack = createBottomTabNavigator();
const Stack = createStackNavigator();
const images = [
  IMAGES.home_icon,
  IMAGES.auction_icon,
  IMAGES.send_circle,
  IMAGES.tri_circle,
  IMAGES.gear_icon,
];

export function MyTabBar({state, descriptors, navigation}) {
  return (
    <SafeAreaView style={style.bottom_tab_container}>
      <StatusBar backgroundColor={COLOR.DARK_BLUE} barStyle="light-content" />
      {state.routes.map((route, index) => {
        const {options} = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;
        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
          });

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name);
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        return (
          <TouchableOpacity
            accessibilityRole="button"
            accessibilityStates={isFocused ? ['selected'] : []}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}
            style={style.bottom_tab_text}>
            {index == 2 ? (
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  alignSelf: 'center',
                  marginTop: -40,
                  shadowColor: COLOR.BLACK,
                  shadowOpacity: 0.2,
                  elevation: 3,
                  shadowOffset: {width: 0, height: 10},
                }}>
                <Image
                  style={{
                    width: 80,
                    height: 80,
                    alignItems: 'center',
                    alignSelf: 'center',
                  }}
                  resizeMode={'cover'}
                  source={images[index]}
                />
              </View>
            ) : (
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  alignSelf: 'center',
                }}>
                <Image
                  style={{
                    width: 22,
                    height: 25,
                    marginTop: 0,
                    alignItems: 'center',
                    alignSelf: 'center',
                    overflow: 'hidden',
                    tintColor: isFocused ? COLOR.LIGHT_BLUE : COLOR.GRAY,
                  }}
                  resizeMode={'contain'}
                  source={images[index]}
                />
              </View>
            )}
          </TouchableOpacity>
        );
      })}
    </SafeAreaView>
  );
}

const MyTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    background: '#ffffff',
  },
};

export default function RootStack(props) {
  return (
      <NavigationContainer ref={ref => {
        setNavigationRef(ref);
      }} theme={MyTheme}>
      <Stack.Navigator
        initialRouteName={'InitialLoadingScreen'}
        headerMode="none">
        <Stack.   Screen
          name="InitialLoadingScreen"
          component={InitialLoadingScreen}
        />
        <Stack.Screen name="SplashScreen" component={SplashScreen} />
        <Stack.Screen name="ForgetPassword" component={ForgetPassword}/>
        <Stack.Screen name="HomeStack" component={HomeStack} />
        <Stack.Screen name="VehicleList" component={VehicleList} />
        <Stack.Screen name="VehicleDetail" component={VehicleDetail} />
        <Stack.Screen name="StatisticsScreen" component={StatisticsScreen} />
        <Stack.Screen name="BezireLineChart" component={BezireLineChart} />
        <Stack.Screen name="VehicleLeads" component={VehicleLeads} />
        <Stack.Screen name="VehicleInspection" component={VehicleInspection} />
        <Stack.Screen name="CarInspection" component={CarInspection} />
        <Stack.Screen name="Exterior" component={Exterior} />
        <Stack.Screen name="MyCars" component={MyCars} />
        <Stack.Screen name="CarsCategory" component={CarsCategory} />
        <Stack.Screen name="MyDetails" component={MyDetails} />
        <Stack.Screen name="ManageAccounts" component={ManageAccounts} />
        <Stack.Screen name="UserDetails" component={UserDetails} />
        <Stack.Screen name="AddNewUser" component={AddNewUser} />
        <Stack.Screen name="AddNewListing" component={AddNewListing} />
        <Stack.Screen name="PdfView" component={PdfView} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

function HomeStack() {
  return (
    <BottonStack.Navigator
      style={{backgroundColor: '#ffffff', alignItems: 'center'}}
      tabBar={(props) => <MyTabBar {...props} />}>
      <BottonStack.Screen key={1} name="Home" component={HomeScreen} />
      <BottonStack.Screen key={2} name="Message" component={AuctionScreen} />
      <BottonStack.Screen key={3} name="Lead" component={VehicleLeads} />
      <BottonStack.Screen key={4} name="Trip" component={StatisticsScreen} />
      <BottonStack.Screen key={5} name="Profile" component={SettingScreen} />
    </BottonStack.Navigator>
  );
}

const style = StyleSheet.create({
  bottom_tab_container: {
    flexDirection: 'row',
    height: 50,
    alignSelf: 'center',
    width: widthPercentageToDP(100),
    marginBottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: COLOR.WHITE,
    shadowColor: COLOR.LIGHT_GRAY,
    shadowOffset: {width: 0, height: -3},
    shadowOpacity: 0.4,
    shadowRadius: 2,
    elevation: 4,
    marginTop: 20,
  },
  bottom_tab_text: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
});
