import {Alert} from 'react-native';
import {combineReducers} from 'redux';
import RootReducer from './Reducers';

const appReducer = combineReducers({
  reducer: RootReducer,
});

const rootReducer = (state, action) => {
  if (action.type === 'USER_LOGOUT') {
    state = undefined;
  }
  return appReducer(state, action);
};

export default rootReducer;
