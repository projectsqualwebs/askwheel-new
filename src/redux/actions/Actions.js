import types from '../../redux/Types';
import {AS_USER_DETAIL} from '../../res/Constants';
import API from '../../api/Api';
import Toast from 'react-native-simple-toast';
import Auth from '../../asyncStore/Index';
import {Alert} from 'react-native';

const api = new API();
const auth = new Auth();

const getUserInfo = () => {
  return async (dispatch, getStore) => {
    let userInfo = getStore().reducer.userDetail;
    if (!userInfo) {
      try {
        api
          .getUserDetails()
          .then((json) => {
            let data = JSON.stringify(json.data.response);

            saveUserDetail(data);
            if (json.data.status == 200) {
              dispatch({
                type: types.USER_DETAIL,
                data: json.data.response,
              });
            } else {
              Toast.show(json.data.message);
            }
          })
          .catch(function (error) {
            Toast.show(error.response.data.message);
          });
      } catch (error) {
        console.log('error getting user detail');
      }
    } else {
      return dispatch({
        type: types.USER_DETAIL,
        data: userInfo,
      });
    }
  };
};

const saveUserDetail = (data) => {
  return async (dispatch, getStore) => {
    try {
      await authp.setValue(AS_USER_DETAIL, data);
      await dispatch({type: types.USER_DETAIL, data: JSON.parse(data)});
    } catch (error) {
      console.log(error.message);
    }
  };
};

const getMakeData = () => {
  return async (dispatch, getStore) => {
    try {
      api
        .getMakeData()
        .then((json) => {
          let data = JSON.stringify(json.data.response);
          if (json.status == 200) {
            dispatch({
              type: types.MAKE_DATA,
              data: json.data.response,
            });
          } else {
            Toast.show(json.data.message);
          }
        })
        .catch(function (error) {
          Toast.show(error.response.data.message);
        });
    } catch (error) {
      console.log(error);
    }
  };
};

const getVarientData = (val) => {
  return async (dispatch, getStore) => {
    try {
      api
        .getVarientData(val)
        .then((json) => {
          if (json.status == 200) {
            dispatch({
              type: types.VARIENT_DATA,
              data: json.data.response,
            });
          } else {
            Toast.show(json.data.message);
          }
        })
        .catch(function (error) {
          Toast.show(error.response.data.message);
        });
    } catch (error) {
      console.log(error);
    }
  };
};
const getColorData = (val) => {
  return async (dispatch, getStore) => {
    try {
      api
        .getColorData(val)
        .then((json) => {
          if (json.status == 200) {
            dispatch({
              type: types.COLOR_DATA,
              data: json.data.response,
            });
          } else {
            Toast.show(json.data.message);
          }
        })
        .catch(function (error) {
          Toast.show(error.response.data.message);
        });
    } catch (error) {
      console.log(error);
    }
  };
};

const getModalData = (val) => {
  return async (dispatch, getStore) => {
    try {
      api
        .getModelData(val)
        .then((json) => {
          if (json.status == 200) {
            dispatch({
              type: types.MODEL_DATA,
              data: json.data.response,
            });
          } else {
            Toast.show(json.data.message);
          }
        })
        .catch(function (error) {
          Toast.show(error.response.data.message);
        });
    } catch (error) {
      console.log(error);
    }
  };
};

const inspectionDetail = (val, type) => {
  return async (dispatch, getStore) => {
    let inspectionDetail = getStore().reducer.inspectionDetail;
    try {
      if (type == 1 && inspectionDetail) {
        dispatch({
          type: types.VEHICLE_INSPECTION_DATA,
          data: inspectionDetail,
        });
      } else if (type == 2) {
        dispatch({
          type: types.VEHICLE_INSPECTION_DATA,
          data: val,
        });
      } else if (type == 3) {
        dispatch({
          type: types.REMOVE_VEHICLE_INSPECTION_DATA,
          data: null,
        });
      }
    } catch (error) {
      console.log(error);
    }
  };
};

const getListedVehicles = (val, page) => {
  let currentPage = Math.ceil(page / 10) + 1;
  return async (dispatch, getStore) => {
    let vehicleData = getStore().reducer.listedVehicleData;
    if (val == 1) {
      try {
        api
        .getListedVehicleData(currentPage)
          .then((json) => {
            if (json.status == 200) {
              dispatch({
                type: types.LISTED_VEHICLE_DATA,
                data: [...vehicleData, ...json.data.response.data],
              });
            } else {
              setTimeout(() => {
                Toast.show(json.data.message);
              }, 0);
            }
          })
          .catch(function (error) {
            Toast.show(error.response.data.message);
          });
      } catch (error) {
        console.log('error', error);
        Alert.alert('error');
      }
    } else {
      return dispatch({
        type: types.LISTED_VEHICLE_DATA,
        data: vehicleData,
      });
    }
  };
};

const getVehicleBasedonType = (val,page,type,tabChanged) => {
  let currentPage = Math.ceil(page / 10) + 1;
  return async (dispatch, getStore) => {

    let vehicleData = []
   
    if(tabChanged == 0){
    vehicleData = getStore().reducer.vehicleDataBasedonType;
     }

    if (val == 1) {
      try {
        api
        .getVehicleBasedonType(type, currentPage)
          .then((json) => {
            if (json.status == 200) {
              dispatch({
                type: types.VEHICLE_DATA_BASEDON_TYPE,
                data: [...vehicleData, ...json.data.response.data],
              });
            } else {
              setTimeout(() => {
                Toast.show(json.data.message);
              }, 0);
            }
          })
          .catch(function (error) {
            Toast.show(error.response.data.message);
          });
      } catch (error) {
        console.log('error', error);
        Alert.alert('error');
      }
    } else {
      return dispatch({
        type: types.VEHICLE_DATA_BASEDON_TYPE,
        data: vehicleData,
      });
    }
  };
};

const getVehicleOnBid = (val, page) => {

  // let currentPage = Math.ceil(page / 10) + 1;
  return async (dispatch, getStore) => {
    let vehicleData = getStore().reducer.bidVehicleData;
    if (val == 1) {
      try {
        api
        .getBidVehicleData()
          .then((json) => {
            if (json.status == 200) {
              dispatch({
                type: types.VEHICLE_ON_BID,
                data: [...vehicleData, ...json.data.response.data],
              });
            } else {
              setTimeout(() => {
                Toast.show(json.data.message);
              }, 0);
            }
          })
          .catch(function (error) {
            Toast.show(error.response.data.message);
          });
      } catch (error) {
        console.log('error', error);
        Alert.alert('error');
      }
    } else {
      return dispatch({
        type: types.VEHICLE_ON_BID,
        data: vehicleData,
      });
    }
  };
};

const getStatisticData = (type) => {
  return async (dispatch, getStore) => {
    try {
      api
        .getStatisticData()
        .then((json) => {
          if (json.status == 200) {
            dispatch({
              type: types.VEHICLE_STATISTICS,
              data: json.data.response,
            });
          } else {
            Toast.show(json.data.message);
          }
        })
        .catch(function (error) {
          Toast.show(error.response.data.message);
        });
    } catch (error) {
      console.log(error);
    }
  };
};

const logoutUser = () => {
  return async (dispatch, getStore) => {
    return dispatch({
      type: types.USER_LOGOUT,
      data: null,
    });
  };
};

const showIndicator = (val) => {
  async (dispatch, getStore) => {
    return dispatch({
      type: types.SHOW_INDICATOR,
      data: val == 1 ? true : false,
    });
  };
};

export default {
  getUserInfo,
  saveUserDetail,
  getMakeData,
  getVarientData,
  getModalData,
  inspectionDetail,
  getListedVehicles,
  logoutUser,
  getVehicleOnBid,
  getColorData,
  getStatisticData,
  getVehicleBasedonType,
  showIndicator,
};
