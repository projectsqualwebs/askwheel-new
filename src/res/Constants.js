'use strict';


//URLS
export const U_BASE = 'https://api.askwheels.com/api/v1/';
export const U_IMAGE_BASE = 'https://api.askwheels.com/askwheels-api/storage/';
export const U_LOGIN = 'auth/login';
export const U_REGISTER = 'auth/dealer-signup';
export const U_RESET_PASSWORD = 'auth/reset-password'

export const U_COMMON_ROLES = 'common/roles';
export const U_GET_PERMISSION = 'common/permissions';
export const U_VEHICLE_MAKE = 'common/vehicle-make';
export const U_GET_VEHICLE_MODEL = 'common/vehicle-model/';
export const U_GET_VEHICLE_VARIENT = 'common/vehicle-variant/';
export const U_GET_VEHICLE_COLOR = 'common/vehicle-color/';
export const U_GET_LISTED_VEHICLE = 'vehicle';
export const U_GET_BID_VEHICLE = 'dealer-bid-vehicles';
export const U_FILE_UPLOAD = 'common/file-upload';
export const U_ADD_VEHICLE = 'vehicle/add';
export const U_DEALER_STATISTICS = 'dealer/statistics';

export const U_TERMS_CONDITION = 'https://askwheels.com/terms.html';

export const U_GENERATE_PDF_URL = 'vehicle/pdf/';


//CURRENT_TAB

//Userdefaults
export const AS_INITIAL_ROUTE = 'initial_route_name';
export const AS_USER_DETAIL = 'user_details';
export const AS_USER_TOKEN = 'user_access_token';
export const AS_FCM_TOKEN = 'fcm_token';

