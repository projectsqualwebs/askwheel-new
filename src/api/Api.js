import axios from 'axios';
import Auth from '../asyncStore/Index';
import {
  U_LOGIN,
  AS_USER_TOKEN,
  U_VEHICLE_MAKE,
  U_GET_VEHICLE_VARIENT,
  U_GET_VEHICLE_MODEL,
  U_ADD_VEHICLE,
  U_GET_LISTED_VEHICLE,
  U_GET_BID_VEHICLE,
  U_GET_VEHICLE_COLOR,
  U_DEALER_STATISTICS, U_REGISTER, U_RESET_PASSWORD, U_GENERATE_PDF_URL,
} from '../res/Constants';
import {Alert} from 'react-native';
import Toast from 'react-native-simple-toast';

const baseURL = 'https://api.askwheels.com/api/v1/';

const headers = async () => {
  const headers = {
    'Content-Type': 'application/json',
  };

  const auth = new Auth();
  const token = await auth.getValue(AS_USER_TOKEN);
  await console.log('user token is:-', token);
  if (token) {
    headers['Authorization'] = `Bearer ${token}`;
  }
  return headers;
};

const request = async (method, path, body) => {
  const url = `${baseURL}${path}`;
  const options = {method, url, headers: await headers()};

  if (body) {
    options.data = body;
  }

  console.log('options are:-', options, body);
  return axios(options);
};


export default class API {
  loginAPI(data, type) {
    return request('POST', type == 1 ? U_LOGIN : U_REGISTER, data);
  }

  resetPasswordAPI(data){
    return request('POST', U_RESET_PASSWORD, data);
  }

  getMakeData() {
    return request('GET', U_VEHICLE_MAKE);
  }

  getVarientData(val) {
    if (val) {
      return request('GET', U_GET_VEHICLE_VARIENT + val);
    }
  }

  getColorData(val) {
    if (val) {
      return request('GET', U_GET_VEHICLE_COLOR + val);
    }
  }

  getListedVehicleData(page) {
    return request('GET', U_GET_LISTED_VEHICLE + `?page=${page}`);
  }

  getVehicleBasedonType(val,page) {
    if (val) {
      return request('GET', U_GET_LISTED_VEHICLE + '?type=' + val + `&&page=${page}`);
    }
  }

  getBidVehicleData(page) {
    return request('GET', U_GET_BID_VEHICLE); 
    // + `?page=${page}`
  }

  getStatisticData() {
    return request('GET', U_DEALER_STATISTICS);
  }

  getModelData(val) {
    if (val) {
      return request('GET', U_GET_VEHICLE_MODEL + val);
    }
  }

  saveInspectionData(val) {
    return request('POST', U_ADD_VEHICLE, val);
  }

  generatePdfUrl(id) {
    return request('GET', U_GENERATE_PDF_URL + id);
  }
}
