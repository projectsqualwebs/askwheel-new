import { useNavigation } from "@react-navigation/core";
import React from "react";
import { SafeAreaView,Image,Text } from "react-native";
import { View } from "react-native-animatable";

import COLOR from "../res/styles/Color";
import Styles from "../res/styles/Styles";


function  SimpleGallery ({route}){
        return (
            <View style={Styles.container,{flex:1}}
              <Image
                source={route.params.image}
                style={{minHeight:100,minWidth:100,backgroundColor:COLOR.BLACK}}
              />  
              <Text style={{fontSize:20}}>
              {route.params.text}
              </Text>
            </View>
        );
}
export default SimpleGallery;