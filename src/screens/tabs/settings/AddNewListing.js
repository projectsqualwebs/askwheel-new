import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  SafeAreaView,
  Image,
  Alert,
  Modal,
} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import COLOR from '../../../res/styles/Color';
import Styles from '../../../res/styles/Styles';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {ScrollView, TextInput} from 'react-native-gesture-handler';
import FONTS from '../../../res/styles/Fonts';
import {BackView} from '../../../commonView/BackView';
import IMAGES from '../../../res/styles/Images';
import {getVehicleInspectionCategory} from '../../../appData';
import ActionSheet from '../../../commonView/ActionSheet';
import ImagePicker from 'react-native-image-crop-picker';

import {U_BASE, U_FILE_UPLOAD} from '../../../res/Constants';
import Toast from 'react-native-simple-toast';
import Indicator from '../../../commonView/ActivityIndicator';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import {connect} from 'react-redux';
import Actions from '../../../redux/actions/Actions';
import Moment from 'moment';

class AddNewListing extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pickerMode: '',
      pickerHeading: '',
      currentDatePicker: null,
      isTick: false,
      isLoading: false,
      datePickerVisible: false,
      actionModalVisible: false,
      inspectionData: {
        selectedMake: null,
        selectedVarient: null,
        selectedModel: null,
        FitmentEndorsementRC: '',
        customer: {
          fullName: '',
          email: '',
          phone1: '',
          phone2: '',
          homeAddress: '',
          inspectionAddress: '',
          selectedInspector: '',
          userId: null,
        },
        make: '',
        variant: '',
        model: '',
        color: '',
        bodyType: '',
        engineNumber: '',
        chassisNumber: '',
        loanNumber: '',
        rcAvailable: 'true',
        typeOfInsurance: '',
        insuranceValidityDate: '',
        underHypothecation: '',
        financer: '',
        gasFitment: '',
        FitmentEndorsementRC: '',
        gasRemoval: '',
        remark: '',
        manufacturingYear: '',
        transmission: '',
        manufacturingCountry: '',
        rtoState: '',
        accidental: false,
        manufacturingDate: '',
        registrationDate: '',
        files: [],
        registrationImage: null,
        registrationNumber: '',
        numberOfOwners: '',
        odometerReading: '',
        fuelType: '',
        expectedPrice: '',
        inspectionData: '',
        inspectionTime: '',
        bodyFrame: getVehicleInspectionCategory(),
      },
    };
    this.handleMultiInput = this.handleMultiInput.bind(this);
    this.handleTick = this.handleTick.bind(this);
  }

  componentDidMount() {
    this.props.inspectionDetail(null, 1);
    if (this.props.vehicleInspectionData) {
      this.setState({
        inspectionData: this.props.vehicleInspectionData,
      });
    }
  }

  handleMultiInput(name) {
    let that = this;
    return (text) => {
      that.setState(
        {
          inspectionData: {
            ...that.state.inspectionData,
            customer: {
              ...that.state.inspectionData.customer,
              [name]: text,
            },
          },
        },
        (val) => {
          if (that.state.isTick && name == 'homeAddress') {
            that.setState({
              inspectionData: {
                ...that.state.inspectionData,
                customer: {
                  ...that.state.inspectionData.customer,
                  inspectionAddress: this.state.inspectionData.customer
                    .homeAddress,
                },
              },
            });
          }
        },
      );
    };
  }

  chooseImage = (val) => {
    let options = {
      title: 'Select Image',
      width: 1630,
      height: 750,
      compressImageMaxWidth: 1630,
      compressImageMaxHeight: 750,
    };

    if (val == 1) {
      ImagePicker.openCamera(options).then((response) => {
        console.log(response);
        if (response.didCancel) {
          console.log('User cancelled image picker');
        } else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        } else if (response.customButton) {
          console.log('User tapped custom button: ', response.customButton);
          alert(response.customButton);
        } else {
          this.setState({
            isLoading: true,
            actionModalVisible: false,
          });
          this.callUploadImage(response);
        }
      });
    } else if (val == 2) {
      ImagePicker.openPicker(options).then((response) => {
        console.log(response);
        if (response.didCancel) {
          console.log('User cancelled image picker');
        } else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        } else if (response.customButton) {
          console.log('User tapped custom button: ', response.customButton);
          alert(response.customButton);
        } else {
          this.setState({
            isLoading: true,
            actionModalVisible: false,
          });
          this.callUploadImage(response);
        }
      });
    }
  };

  callUploadImage(res) {
    var data = new FormData();
    var photo = {
      uri: res.uri,
      type: res.type,
      name: 'image.jpg',
    };
    data.append('directory', 'user/documents/');
    data.append('file', photo);
    fetch(U_BASE + U_FILE_UPLOAD, {
      body: data,
      method: 'POST',
      headers: {
        Accept: 'application/json',
        Authorization: 'Bearer ' + this.state.token,
      },
    })
      .then((response) => response.json())
      .catch((error) => {
        console.log('error data is:-', error);
        this.setState({isLoading: false});
      })
      .then((responseData) => {
        console.log('response data is:-', responseData);
        if (responseData !== undefined) {
          this.setState(
            {
              isLoading: false,
              inspectionData: {
                ...this.state.inspectionData,
                customer: {
                  ...this.state.inspectionData.customer,
                  userId: responseData.response.imageUrl,
                },
              },
            },
            (val) => {
              Toast.show('Successfully uploaded image');
            },
          );
        } else {
          Toast.show('Image uploading failed');
          this.setState({isLoading: false});
        }
      })
      .done();
  }

  commonView = (props) => {
    return (
      <View>
        <View
          style={{
            flexDirection: 'row',
            alignSelf: 'center',
            height: 45,

            alignItems: 'center',
          }}>
          <TextInput
            onChangeText={this.handleMultiInput(props.fieldName)}
            value={props.value}
            placeholder={props.placeholderName}
            placeholderTextColor={COLOR.LIGHT_TEXT}
            style={[
              {
                flex: 1,
                fontSize: FONTS.REGULAR,
                fontFamily: FONTS.FAMILY_REGULAR,
                textAlign: 'left',
              },
            ]}
          />
        </View>
        <View style={[Styles.line_view, {marginVertical: 10}]} />
      </View>
    );
  };

  handleTick = () => {
    if (this.state.isTick) {
      let data = this.state.inspectionData;
      data.customer.inspectionAddress = '';
      console.log('data.customer', data.customer.inspectionAddress);
      this.setState({
        isTick: false,
        inspectionData: data,
      });
    } else {
      let data = this.state.inspectionData;
      data.customer.inspectionAddress = data.customer.homeAddress;
      console.log('data.customer', data.customer.inspectionAddress);
      this.setState({
        isTick: true,
        inspectionData: data,
      });
    }
  };

  handleNavigation = () => {
    let that = this;
    let data = that.state.inspectionData.customer;
    if (data.fullName == '') {
      Toast.show('Enter Full Name');
    } else if (data.email == '') {
      Toast.show('Enter Email Address');
    } else if (data.phone1 == '') {
      Toast.show('Enter Phone Number 1');
    } else if (data.homeAddress == '') {
      Toast.show('Enter Home Address');
    } else if (data.inspectionAddress == '') {
      Toast.show('Enter Inspection Address');
    } else {
      this.props.inspectionDetail(this.state.inspectionData, 2);
      this.props.navigation.navigate('VehicleDetail');
    }
  };

  handleSheet = (val) => {
    if (val == 1) {
      this.chooseImage(1);
    } else if (val == 2) {
      this.chooseImage(2);
    } else if (val == 3) {
      this.setState({
        actionModalVisible: false,
      });
    }
  };

  handleDatePickerConfirm = (date) => {
    if (this.state.currentDatePicker == 'inspectionDate') {
      this.setState({
        inspectionData: {
          ...this.state.inspectionData,
          inspectionDate: Moment(date).format('yyyy-MM-DD'),
        },
      });
    } else if (this.state.currentDatePicker == 'inspectionTime') {
      this.setState({
        inspectionData: {
          ...this.state.inspectionData,
          inspectionTime: Moment(date).format('h:mm a'),
        },
      });
    }
    this.setState({
      pickerMode: null,
      datePickerVisible: false,
      currentDatePicker: '',
    });
    // this.hidePicker();
  };

  handleDatePickerCancel = () => {
    this.setState({
      datePickerVisible: false,
      currentDatePicker: '',
    });
  };

  showDatePicker = (val) => {
    if (val == 'inspectionDate') {
      this.setState({
        pickerMode: 'date',
        datePickerVisible: true,
        currentDatePicker: val,
      });
    } else if (val == 'inspectionTime') {
      this.setState({
        pickerMode: 'time',
        datePickerVisible: true,
        currentDatePicker: val,
      });
    }
  };

  hideDatePicker = () => {
    this.setState({
      datePickerVisible: false,
      currentDatePicker: '',
    });
  };

  render() {
    return (
      <View style={[Styles.container, {backgroundColor: COLOR.BLUE_BG}]}>
        <Indicator loading={this.state.isLoading} />
        <View style={styles.navigation_bar}>
          <View style={{width: 100, paddingLeft: 10}}>
            <BackView props={this.props} />
          </View>
          <Text
            style={[
              Styles.button_font,
              {alignSelf: 'center', flex: 1, textAlign: 'center'},
            ]}>
            Add New Listing
          </Text>
          <Text
            style={[
              Styles.body_label,
              {
                color: COLOR.WHITE,
                textAlign: 'right',
                paddingRight: 20,
                width: 100,
              },
            ]}></Text>
        </View>
        <ScrollView style={{flex: 1, backgroundColor: COLOR.BLUE_BG}}>
          <SafeAreaView
            style={{
              flex: 1,
              backgroundColor: COLOR.BLUE_BG,
              alignItems: 'center',
              justifyContent: 'flex-start',
            }}>
            <Text
              style={[
                Styles.button_font,
                {
                  alignSelf: 'center',
                  flex: 1,
                  textAlign: 'left',
                  width: widthPercentageToDP(90),
                  marginVertical: 15,
                  color: COLOR.BLACK,
                },
              ]}>
              Enter User Details
            </Text>
            <View
              style={[
                Styles.shadow_view,
                {
                  paddingHorizontal: 15,
                  width: wp(90),
                  paddingVertical: 15,
                },
              ]}>
              <this.commonView
                fieldName={'fullName'}
                value={this.state.inspectionData.customer.fullName}
                placeholderName={'Full Name'}
              />
              <this.commonView
                fieldName={'email'}
                value={this.state.inspectionData.customer.email}
                placeholderName={'Email Address'}
              />
              <this.commonView
                fieldName={'phone1'}
                value={this.state.inspectionData.customer.phone1}
                placeholderName={'Phone Number 1'}
              />
              <this.commonView
                value={this.state.inspectionData.customer.phone2}
                fieldName={'phone2'}
                placeholderName={'Phone Number 2'}
              />
              <this.commonView
                value={this.state.inspectionData.customer.homeAddress}
                fieldName={'homeAddress'}
                placeholderName={'Home Address'}
              />
              <this.commonView
                value={this.state.inspectionData.customer.inspectionAddress}
                fieldName={'inspectionAddress'}
                placeholderName={'Inspection Address'}
              />
              <TouchableOpacity
                onPress={() => this.showDatePicker('inspectionDate')}>
                <View
                  style={{
                    paddingHorizontal: 10,
                    backgroundColor: COLOR.WHITE,
                    flexDirection: 'row',
                    height: 60,
                    alignItems: 'center',
                    justifyContent: 'space-between',
                  }}>
                  <TextInput
                    placeholder={'Inspection Date'}
                    value={this.state.inspectionData.inspectionDate}
                    style={[Styles.body_label, {flex: 1}]}
                  />
                  <Image
                    style={{height: 25, width: 25}}
                    source={IMAGES.calendar}
                    resizeMode={'contain'}
                  />
                </View>
              </TouchableOpacity>
              <View style={Styles.line_view} />
              <TouchableOpacity
                onPress={() => this.showDatePicker('inspectionTime')}>
                <View
                  style={{
                    paddingHorizontal: 10,
                    backgroundColor: COLOR.WHITE,
                    flexDirection: 'row',
                    height: 60,
                    alignItems: 'center',
                    justifyContent: 'space-between',
                  }}>
                  <TextInput
                    placeholder={'Registration Time'}
                    value={this.state.inspectionData.inspectionTime}
                    style={[Styles.body_label, {flex: 1}]}
                  />
                  <Image
                    style={{height: 25, width: 25}}
                    source={IMAGES.calendar}
                    resizeMode={'contain'}
                  />
                </View>
              </TouchableOpacity>
              <View style={Styles.line_view} />

              <View
                style={{
                  flexDirection: 'row',

                  height: 30,
                  marginTop: 15,
                }}>
                <TouchableOpacity onPress={() => this.handleTick()}>
                  <Image
                    style={{
                      borderWidth: 1,
                      borderColor: COLOR.LIGHT_TEXT,
                      borderRadius: 5,
                      height: 20,
                      width: 20,
                      marginRight: 10,
                      backgroundColor: this.state.isTick
                        ? COLOR.DARK_BLUE
                        : COLOR.WHITE,
                    }}
                  />
                </TouchableOpacity>
                <Text
                  style={[
                    Styles.body_label,
                    {flex: 1, color: COLOR.LIGHT_TEXT},
                  ]}>
                  Same as Home Address
                </Text>
              </View>
            </View>
            <View
              style={[
                Styles.shadow_view,
                {
                  paddingHorizontal: 15,
                  width: wp(90),
                  paddingVertical: 15,
                  height: 50,
                  marginVertical: 20,
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                },
              ]}>
              <Text style={[Styles.body_label, {color: COLOR.LIGHT_TEXT}]}>
                {' '}
                Select Inspector for this vehicle
              </Text>
              <Image
                style={{height: 15, width: 15}}
                source={IMAGES.down_arrow}
              />
            </View>
            <TouchableOpacity
              onPress={() => {
                this.setState({actionModalVisible: true});
              }}>
              <View
                style={[
                  Styles.shadow_view,
                  {
                    width: wp(90),
                    height: 250,
                    marginVertical: 20,
                    justifyContent: 'center',
                    alignItems: 'center',
                    overflow: 'hidden',
                  },
                ]}>
                <Image
                  style={
                    this.state.inspectionData.customer.userId
                      ? {height: 250, width: wp(90), marginBottom: 20}
                      : {height: 80, marginBottom: 20}
                  }
                  source={
                    this.state.inspectionData.customer.userId
                      ? {uri: this.state.inspectionData.customer.userId}
                      : IMAGES.id_proof
                  }
                  resizeMode={
                    this.state.inspectionData.customer.userId
                      ? 'cover'
                      : 'contain'
                  }
                />
              </View>
            </TouchableOpacity>
            <ActionSheet
              modalVisible={this.state.actionModalVisible}
              handleSheet={this.handleSheet}
            />
          </SafeAreaView>
        </ScrollView>

        <TouchableOpacity
          onPress={this.handleNavigation}
          style={[
            Styles.button_content,
            {
              borderRadius: 0,
              width: widthPercentageToDP(100),
            },
          ]}>
          <Text style={Styles.button_font}>Confirm Details</Text>
        </TouchableOpacity>
        <DateTimePickerModal
          isVisible={this.state.datePickerVisible}
          mode={this.state.pickerMode}
          headerTextIOS={this.state.pickerHeading}
          onConfirm={this.handleDatePickerConfirm}
          onCancel={this.handleDatePickerCancel}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  navigation_bar: {
    height: getStatusBarHeight(true) + 45,
    backgroundColor: COLOR.DARK_BLUE,
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: getStatusBarHeight(true),
    justifyContent: 'center',
  },
  flatlist_view: {
    width: wp(100),
    marginHorizontal: 10,
    flex: 1,
  },
  car_view: {
    width: wp(90),
    height: 110,
    backgroundColor: COLOR.WHITE,
    marginBottom: 10,
    borderRadius: 5,
    paddingHorizontal: 10,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: COLOR.BLACK_GRAD,
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.3,
    elevation: 3,
  },
});

const mapStateToProps = (state, ownProps) => {
  return {
    vehicleInspectionData: state.reducer.inspectionDetail,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    inspectionDetail: (data, type) => {
      dispatch(Actions.inspectionDetail(data, type));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddNewListing);
