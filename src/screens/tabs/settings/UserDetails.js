import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import COLOR from '../../../res/styles/Color';
import Styles from '../../../res/styles/Styles';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {ScrollView, TextInput} from 'react-native-gesture-handler';
import FONTS from '../../../res/styles/Fonts';
import {BackView} from '../../../commonView/BackView';

export default class UserDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      form: {
        name: '',
        email: '',
        role: '',
        previlege: '',
        contact_number: '',
        city: '',
        state: '',
        created_by: '',
        id_proof: '',
      },
    };

    this.handleMultiInput = this.handleMultiInput.bind(this);
  }

  handleMultiInput(name) {
    return (text) => {
      this.setState({form: {[name]: text}});
    };
  }

  commonView = (props) => {
    return (
      <View>
        <View
          style={{
            flexDirection: 'row',
            alignSelf: 'center',
            height: 45,

            alignItems: 'center',
          }}>
          <Text style={[Styles.body_label, {width: 150}]}>{props.name}</Text>
          <TextInput
            onChangeText={this.handleMultiInput(props.fieldName)}
            placeholder={props.placeholderName}
            style={[
              {
                flex: 1,
                fontSize: FONTS.REGULAR,
                fontFamily: FONTS.FAMILY_REGULAR,
                textAlign: 'right',
              },
            ]}
            placeholder={'Name'}
          />
        </View>
        <View style={[Styles.line_view, {marginVertical: 10}]} />
      </View>
    );
  };

  render() {
    const {selectedIndex, dateType} = this.state;
    return (
      <View style={[Styles.container, {backgroundColor: COLOR.BLUE_BG}]}>
        <View style={styles.navigation_bar}>
          <View style={{width: 100, paddingLeft: 10}}>
            <BackView props={this.props} />
          </View>
          <Text
            style={[
              Styles.button_font,
              {alignSelf: 'center', flex: 1, textAlign: 'center'},
            ]}>
            User Details
          </Text>
          <Text
            style={[
              Styles.body_label,
              {
                color: COLOR.WHITE,
                textAlign: 'right',
                paddingRight: 20,
                width: 100,
              },
            ]}></Text>
        </View>
        <ScrollView style={{flex: 1, backgroundColor: COLOR.BLUE_BG}}>
          <SafeAreaView
            style={{
              flex: 1,
              backgroundColor: COLOR.BLUE_BG,
              alignItems: 'center',
              justifyContent: 'flex-start',
            }}>
            <View
              style={[
                Styles.shadow_view,
                {
                  paddingHorizontal: 15,
                  width: wp(90),
                  marginTop: 20,
                  paddingVertical: 15,
                },
              ]}>
              <this.commonView
                name={'Name'}
                fieldName={'name'}
                placeholderName={'Name'}
              />
              <this.commonView
                name={'Email Address'}
                fieldName={'email'}
                placeholderName={'Email Address'}
              />
              <this.commonView
                name={'Role'}
                fieldName={'role'}
                placeholderName={'Role'}
              />
              <this.commonView
                name={'Admin Previliges'}
                fieldName={'previlige'}
                placeholderName={'Admin Previliges'}
              />
              <this.commonView
                name={'Contact Number'}
                fieldName={'contact_number'}
                placeholderName={'Contact Number'}
              />
              <this.commonView
                name={'City'}
                fieldName={'city'}
                placeholderName={'City'}
              />
              <this.commonView
                name={'State'}
                fieldName={'state'}
                placeholderName={'State'}
              />
              <this.commonView
                name={'Created By'}
                fieldName={'created_by'}
                placeholderName={'Created By'}
              />
              <this.commonView
                name={'ID Proof'}
                fieldName={'id_proof'}
                placeholderName={'ID Proof'}
              />
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  height: 30,
                  marginTop: 15,
                }}>
                <Text style={[Styles.body_label, {width: 150}]}>
                  Upload Document
                </Text>

                <TouchableOpacity>
                  <Text
                    style={[
                      Styles.small_label,
                      {
                        textDecorationLine: 'underline',
                        textDecorationColor: COLOR.LIGHT_BLUE,
                        color: COLOR.LIGHT_BLUE,
                        fontFamily: FONTS.FAMILY_SEMIBOLD,
                      },
                    ]}>
                    View
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </SafeAreaView>
        </ScrollView>
        <View
          style={{
            flexDirection: 'row',
            width: widthPercentageToDP(100),
            alignSelf: 'center',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <TouchableOpacity
            style={[
              Styles.button_content,
              {borderRadius: 0, width: null, flex: 1},
            ]}>
            <Text style={Styles.button_font}>Edit</Text>
          </TouchableOpacity>
          <View style={{width: 0.5, backgroundColor: COLOR.LIGHT_TEXT}} />
          <TouchableOpacity
            style={[
              Styles.button_content,
              {borderRadius: 0, width: null, flex: 1},
            ]}>
            <Text style={Styles.button_font}>Delete</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  navigation_bar: {
    height: getStatusBarHeight(true) + 45,
    backgroundColor: COLOR.DARK_BLUE,
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: getStatusBarHeight(true),
    justifyContent: 'center',
  },
  flatlist_view: {
    width: wp(100),
    marginHorizontal: 10,
    flex: 1,
  },
  car_view: {
    width: wp(90),
    height: 110,
    backgroundColor: COLOR.WHITE,
    marginBottom: 10,
    borderRadius: 5,
    paddingHorizontal: 10,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: COLOR.BLACK_GRAD,
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.3,
    elevation: 3,
  },
});
