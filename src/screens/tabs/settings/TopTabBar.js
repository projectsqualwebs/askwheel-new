import * as React from 'react';
import {Dimensions} from 'react-native';
import {TabView, TabBar} from 'react-native-tab-view';

import COLOR from '../../../res/styles/Color';
import FONTS from '../../../res/styles/Fonts';
import CarsCategory from './CarsCategory';


const initialLayout = {width: Dimensions.get('window').width};

export default TopTabBar = (props) => {
  const [index, setIndex] = React.useState(0);

  const setCurrentTab = (val) => {
    switch (val) {
      case 1:
        return 'Added';
      case 2:
        return 'Assigned';
      case 3:
        return 'Inspected';
      case 4:
        return 'On Auction';
      case 5:
        return 'Deals';
        break;
    }
  };

  const [routes] = React.useState([
    {
      key: 'first',
      title: setCurrentTab(1),
    },
    {
      key: 'second',
      title: setCurrentTab(2),
    },
    {
      key: 'third',
      title: setCurrentTab(3),
    },
    {
      key: 'fourth',
      title: setCurrentTab(4),
    },
    {
      key: 'fifth',
      title: setCurrentTab(5),
    },
  ]);

  renderScene = ({route}) => {
    switch (route.key) {
      case 'first':
        return <CarsCategory navigation={props.props.navigation} />;
      case 'second':
        return <CarsCategory navigation={props.props.navigation} />;
      case 'third':
        return <CarsCategory navigation={props.props.navigation} />;
      case 'fourth':
        return <CarsCategory navigation={props.props.navigation} />;
      case 'fifth':
        return <CarsCategory navigation={props.props.navigation} />;
      default:
        return null;
    }
  };

  const renderTabBar = (props) => (
    <TabBar
      {...props}
      indicatorStyle={{backgroundColor: COLOR.LIGHT_BLUE}}
      style={{backgroundColor: COLOR.WHITE}}
      activeColor={COLOR.BLACK}
      labelStyle={{
        textTransform: 'capitalize',
        fontFamily: FONTS.FAMILY_REGULAR,
		fontSize: 14,
		  textAlign: 'center',
		marginHorizontal:-5
      }}
      inactiveColor={COLOR.BLACK}
    />
  );

  return (
    <TabView
      {...props}
      navigationState={{index, routes}}
      renderScene={renderScene}
      renderTabBar={renderTabBar}
      onIndexChange={setIndex}
      initialLayout={initialLayout}
    />
  );
};
