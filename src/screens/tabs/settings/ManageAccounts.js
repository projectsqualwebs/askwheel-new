import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  SafeAreaView,
  Image,
} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import COLOR from '../../../res/styles/Color';
import Styles from '../../../res/styles/Styles';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {ScrollView, TextInput} from 'react-native-gesture-handler';
import FONTS from '../../../res/styles/Fonts';
import {BackView} from '../../../commonView/BackView';
import IMAGES from '../../../res/styles/Images';

export default class ManageAccounts extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.handleDateType = this.handleDateType.bind(this);
  }

  handleDateType = (val) => {
    this.setState({dateType: val});
  };

  render() {
    const {selectedIndex, dateType} = this.state;
    return (
      <View style={Styles.container}>
        <View style={styles.navigation_bar}>
          <View style={{width: 100, paddingLeft: 10}}>
            <BackView props={this.props} />
          </View>
          <Text
            style={[
              Styles.button_font,
              {alignSelf: 'center', flex: 1, textAlign: 'center'},
            ]}>
            Manage Accounts
          </Text>
          <Text
            style={[
              Styles.body_label,
              {
                color: COLOR.WHITE,
                textAlign: 'right',
                paddingRight: 20,
                width: 100,
              },
            ]}></Text>
        </View>
        <ScrollView style={{flex: 1}}>
          <SafeAreaView
            style={{
              flex: 1,
              backgroundColor: COLOR.BLUE_BG,
              alignItems: 'center',
              justifyContent: 'flex-start',
            }}>
            <Text
              style={[
                Styles.button_font,
                {
                  alignSelf: 'center',
                  textAlign: 'left',
                  color: COLOR.BLACK,
                  width: widthPercentageToDP(90),
                  marginVertical: 15,
                },
              ]}>
              Admin
            </Text>
            <View
              style={[
                Styles.shadow_view,
                {
                  paddingHorizontal: 15,
                  width: widthPercentageToDP(90),
                  alignSelf: 'center',
                },
              ]}>
              <View
                style={{
                  flexDirection: 'row',
                  alignSelf: 'center',
                  height: 45,
                  marginTop: 15,
                  alignItems: 'center',
                }}>
                <Image
                  style={{height: 25, width: 25}}
                  resizeMode={'contain'}
                  source={IMAGES.badge}
                />
                <Text style={[Styles.medium_label, {marginLeft: 20, flex: 1}]}>
                  Deepka Gill
                </Text>
                <Text style={[Styles.body_label, {color: COLOR.LIGHT_TEXT}]}>
                  Admin
                </Text>
              </View>
              <View style={[Styles.line_view, {marginVertical: 10}]} />

              <View
                style={{
                  flexDirection: 'row',
                  alignSelf: 'center',
                  height: 45,
                  marginBottom: 15,
                  alignItems: 'center',
                }}>
                <Image
                  style={{height: 25, width: 25}}
                  resizeMode={'contain'}
                  source={IMAGES.badge}
                />
                <Text style={[Styles.medium_label, {marginLeft: 20, flex: 1}]}>
                  Rahul Vyas
                </Text>
                <Text style={[Styles.body_label, {color: COLOR.LIGHT_TEXT}]}>
                  Admin
                </Text>
              </View>
            </View>
            <Text
              style={[
                Styles.button_font,
                {
                  alignSelf: 'center',
                  textAlign: 'left',
                  color: COLOR.BLACK,
                  width: widthPercentageToDP(90),
                  marginVertical: 15,
                },
              ]}>
              Admin
            </Text>
            <View
              style={[
                Styles.shadow_view,
                {
                  paddingHorizontal: 15,
                  width: widthPercentageToDP(90),
                  alignSelf: 'center',
                },
              ]}>
              <View
                style={{
                  flexDirection: 'row',
                  alignSelf: 'center',
                  height: 45,
                  marginTop: 15,
                  alignItems: 'center',
                }}>
                <Image
                  style={{height: 25, width: 25}}
                  resizeMode={'contain'}
                  source={IMAGES.profile}
                />
                <Text style={[Styles.medium_label, {marginLeft: 20, flex: 1}]}>
                  Rajiv Verma
                </Text>
                <Text style={[Styles.body_label, {color: COLOR.LIGHT_TEXT}]}>
                  Manager
                </Text>
              </View>
              <View style={[Styles.line_view, {marginVertical: 10}]} />
              <View
                style={{
                  flexDirection: 'row',
                  alignSelf: 'center',
                  height: 45,

                  alignItems: 'center',
                }}>
                <Image
                  style={{height: 25, width: 25}}
                  resizeMode={'contain'}
                  source={IMAGES.profile}
                />
                <Text style={[Styles.medium_label, {marginLeft: 20, flex: 1}]}>
                  Gaurav Jain
                </Text>
                <Text style={[Styles.body_label, {color: COLOR.LIGHT_TEXT}]}>
                  Manager
                </Text>
              </View>
              <View style={[Styles.line_view, {marginVertical: 10}]} />
              <View
                style={{
                  flexDirection: 'row',
                  alignSelf: 'center',
                  height: 45,
                  marginBottom: 15,
                  alignItems: 'center',
                }}>
                <Image
                  style={{height: 25, width: 25}}
                  resizeMode={'contain'}
                  source={IMAGES.profile}
                />
                <Text style={[Styles.medium_label, {marginLeft: 20, flex: 1}]}>
                  Rohit Sharma
                </Text>
                <Text style={[Styles.body_label, {color: COLOR.LIGHT_TEXT}]}>
                  Manager
                </Text>
              </View>
            </View>
            <Text
              style={[
                Styles.button_font,
                {
                  alignSelf: 'center',
                  textAlign: 'left',
                  color: COLOR.BLACK,
                  width: widthPercentageToDP(90),
                  marginVertical: 15,
                },
              ]}>
              Inspector
            </Text>
            <View
              style={[
                Styles.shadow_view,
                {
                  paddingHorizontal: 15,
                  width: widthPercentageToDP(90),
                  alignSelf: 'center',
                  marginBottom: 20,
                },
              ]}>
              <View
                style={{
                  flexDirection: 'row',
                  alignSelf: 'center',
                  height: 45,
                  marginTop: 15,
                  alignItems: 'center',
                }}>
                <Image
                  style={{height: 25, width: 25}}
                  resizeMode={'contain'}
                  source={IMAGES.wrench}
                />
                <Text style={[Styles.medium_label, {marginLeft: 20, flex: 1}]}>
                  Rahul Roy
                </Text>
                <Text style={[Styles.body_label, {color: COLOR.LIGHT_TEXT}]}>
                  Inspector
                </Text>
              </View>
              <View style={[Styles.line_view, {marginVertical: 10}]} />
              <View
                style={{
                  flexDirection: 'row',
                  alignSelf: 'center',
                  height: 45,

                  alignItems: 'center',
                }}>
                <Image
                  style={{height: 25, width: 25}}
                  resizeMode={'contain'}
                  source={IMAGES.wrench}
                />
                <Text style={[Styles.medium_label, {marginLeft: 20, flex: 1}]}>
                  Mohan Rai
                </Text>
                <Text style={[Styles.body_label, {color: COLOR.LIGHT_TEXT}]}>
                  Inspector
                </Text>
              </View>
              <View style={[Styles.line_view, {marginVertical: 10}]} />
              <View
                style={{
                  flexDirection: 'row',
                  alignSelf: 'center',
                  height: 45,

                  alignItems: 'center',
                }}>
                <Image
                  style={{height: 25, width: 25}}
                  resizeMode={'contain'}
                  source={IMAGES.wrench}
                />
                <Text style={[Styles.medium_label, {marginLeft: 20, flex: 1}]}>
                  Kailash Mathur
                </Text>
                <Text style={[Styles.body_label, {color: COLOR.LIGHT_TEXT}]}>
                  Inspector
                </Text>
              </View>
              <View style={[Styles.line_view, {marginVertical: 10}]} />
              <View
                style={{
                  flexDirection: 'row',
                  alignSelf: 'center',
                  height: 45,
                  marginBottom: 15,
                  alignItems: 'center',
                }}>
                <Image
                  style={{height: 25, width: 25}}
                  resizeMode={'contain'}
                  source={IMAGES.wrench}
                />
                <Text style={[Styles.medium_label, {marginLeft: 20, flex: 1}]}>
                  Rajendra Jaat
                </Text>
                <Text style={[Styles.body_label, {color: COLOR.LIGHT_TEXT}]}>
                  Inspector
                </Text>
              </View>
            </View>
          </SafeAreaView>
        </ScrollView>
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate('AddNewUser')}
          style={[
            Styles.button_content,
            {width: widthPercentageToDP(100), borderRadius: 0},
          ]}>
          <Text style={Styles.button_font}>Add New User</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  navigation_bar: {
    height: getStatusBarHeight(true) + 45,
    backgroundColor: COLOR.DARK_BLUE,
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: getStatusBarHeight(true),
    justifyContent: 'center',
  },
  flatlist_view: {
    width: wp(100),
    marginHorizontal: 10,
    flex: 1,
  },
  car_view: {
    width: wp(90),
    height: 110,
    backgroundColor: COLOR.WHITE,
    marginBottom: 10,
    borderRadius: 5,
    paddingHorizontal: 10,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: COLOR.BLACK_GRAD,
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.3,
    elevation: 3,
  },
});
