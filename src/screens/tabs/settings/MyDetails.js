import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import COLOR from '../../../res/styles/Color';
import Styles from '../../../res/styles/Styles';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {TextInput} from 'react-native-gesture-handler';
import FONTS from '../../../res/styles/Fonts';
import { BackView } from '../../../commonView/BackView';

export default class MyDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.handleDateType = this.handleDateType.bind(this);
  }

  handleDateType = (val) => {
    this.setState({dateType: val});
  };

  render() {
    const {selectedIndex, dateType} = this.state;
    return (
      <View style={Styles.container}>
        <View style={styles.navigation_bar}>
          <View style={{width: 100,paddingLeft:10}}>
            <BackView props={this.props} />
          </View>
          <Text
            style={[
              Styles.button_font,
              {alignSelf: 'center', flex: 1, textAlign: 'center'},
            ]}>
            My Details
          </Text>
          <Text
            style={[
              Styles.body_label,
              {
                color: COLOR.WHITE,
                textAlign: 'right',
                paddingRight: 20,
                width: 100,
              },
            ]}></Text>
        </View>
        <SafeAreaView
          style={{
            flex: 1,
            backgroundColor: COLOR.BLUE_BG,
            alignItems: 'center',
            justifyContent: 'flex-start',
          }}>
          <Text
            style={[
              Styles.button_font,
              {
                alignSelf: 'center',
                textAlign: 'left',
                color: COLOR.BLACK,
                width: widthPercentageToDP(90),
                marginVertical: 15,
              },
            ]}>
            Personal Details
          </Text>
          <View
            style={[
              Styles.shadow_view,
              {paddingHorizontal: 15, width: wp(90)},
            ]}>
            <View
              style={{
                flexDirection: 'row',
                alignSelf: 'center',
                height: 45,
                marginTop: 15,
                alignItems: 'center',
              }}>
              <Text style={[Styles.body_label, {width: 100}]}>Name</Text>
              <TextInput
                style={[
                  {
                    flex: 1,
                    fontSize: FONTS.REGULAR,
                    fontFamily: FONTS.FAMILY_REGULAR,
                  },
                ]}
                placeholder={'Name'}
              />
            </View>
            <View
              style={{
                flexDirection: 'row',
                alignSelf: 'center',
                height: 45,
                alignItems: 'center',
              }}>
              <Text style={[Styles.body_label, {width: 100}]}>Contact</Text>
              <TextInput
                style={[
                  {
                    flex: 1,
                    fontSize: FONTS.REGULAR,
                    fontFamily: FONTS.FAMILY_REGULAR,
                  },
                ]}
                placeholder={'Contact'}
              />
            </View>
            <View
              style={{
                flexDirection: 'row',
                alignSelf: 'center',
                height: 45,
                alignItems: 'center',
              }}>
              <Text style={[Styles.body_label, {width: 100}]}>Role</Text>
              <TextInput
                style={[
                  {
                    flex: 1,
                    fontSize: FONTS.REGULAR,
                    fontFamily: FONTS.FAMILY_REGULAR,
                  },
                ]}
                placeholder={'Role'}
              />
            </View>
            <View
              style={{
                flexDirection: 'row',
                alignSelf: 'center',

                alignItems: 'center',
                marginBottom: 15,
              }}>
              <Text style={[Styles.body_label, {width: 100}]}>Address</Text>
              <TextInput
                multiline={true}
                style={[
                  {
                    flex: 1,
                    fontSize: FONTS.REGULAR,
                    fontFamily: FONTS.FAMILY_REGULAR,
                  },
                ]}
                placeholder={'Address'}
              />
            </View>
          </View>
          <Text
            style={[
              Styles.button_font,
              {
                alignSelf: 'center',
                textAlign: 'left',
                color: COLOR.BLACK,
                width: widthPercentageToDP(90),
                marginVertical: 15,
              },
            ]}>
            Dealership Details
          </Text>
          <View
            style={[
              Styles.shadow_view,
              {paddingHorizontal: 15, width: wp(90)},
            ]}>
            <View
              style={{
                flexDirection: 'row',
                alignSelf: 'center',
                height: 45,
                marginTop: 10,
                alignItems: 'center',
              }}>
              <Text style={[Styles.body_label, {width: 100}]}>Name</Text>
              <TextInput
                style={[
                  {
                    flex: 1,
                    fontSize: FONTS.REGULAR,
                    fontFamily: FONTS.FAMILY_REGULAR,
                  },
                ]}
                placeholder={'Name'}
              />
            </View>
            <View
              style={{
                flexDirection: 'row',
                alignSelf: 'center',
                height: 45,
                alignItems: 'center',
              }}>
              <Text style={[Styles.body_label, {width: 100}]}>Contact</Text>
              <TextInput
                style={[
                  {
                    flex: 1,
                    fontSize: FONTS.REGULAR,
                    fontFamily: FONTS.FAMILY_REGULAR,
                  },
                ]}
                placeholder={'Contact'}
              />
            </View>
            <View
              style={{
                flexDirection: 'row',
                alignSelf: 'center',
                height: 45,
                alignItems: 'center',
              }}>
              <Text style={[Styles.body_label, {width: 100}]}>Landline</Text>
              <TextInput
                style={[
                  {
                    flex: 1,
                    fontSize: FONTS.REGULAR,
                    fontFamily: FONTS.FAMILY_REGULAR,
                  },
                ]}
                placeholder={'Landline'}
              />
            </View>

            <View
              style={{
                flexDirection: 'row',
                alignSelf: 'center',

                alignItems: 'center',
                marginBottom: 20,
              }}>
              <Text style={[Styles.body_label, {width: 100}]}>Address</Text>
              <TextInput
                multiline={true}
                style={[
                  {
                    flex: 1,
                    fontSize: FONTS.REGULAR,
                    fontFamily: FONTS.FAMILY_REGULAR,
                  },
                ]}
                placeholder={'Address'}
              />
            </View>
          </View>
        </SafeAreaView>
        <TouchableOpacity
          style={[
            Styles.button_content,
            {width: widthPercentageToDP(100), borderRadius: 0},
          ]}>
          <Text style={Styles.button_font}>Change Account Password</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  navigation_bar: {
    height: getStatusBarHeight(true) + 45,
    backgroundColor: COLOR.DARK_BLUE,
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: getStatusBarHeight(true),
    justifyContent: 'center',
  },
  flatlist_view: {
    width: wp(100),
    marginHorizontal: 10,
    flex: 1,
  },
  car_view: {
    width: wp(90),
    height: 110,
    backgroundColor: COLOR.WHITE,
    marginBottom: 10,
    borderRadius: 5,
    paddingHorizontal: 10,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: COLOR.BLACK_GRAD,
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.3,
    elevation: 3,
  },
});
