import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  Dimensions
} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import COLOR from '../../../res/styles/Color';
import IMAGES from '../../../res/styles/Images';
import Styles from '../../../res/styles/Styles';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import {TabView, SceneMap, TabBar} from 'react-native-tab-view';
import CarsCategory from './CarsCategory';
import TopTabBar from './TopTabBar';
import {BackView} from '../../../commonView/BackView'

const initialLayout = {width: Dimensions.get('window').width};



export default class MyCars extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedIndex: 0,
      dateType: 2,
      carData: [],
    };
    this.handleDateType = this.handleDateType.bind(this);
  }

  handleDateType = (val) => {
    this.setState({dateType: val});
  };

  render() {

    return (
      <View style={Styles.container}>
        <View style={styles.navigation_bar}>
          <View style={{width: 50}}>
            <BackView props={this.props}/>
          </View>
          <Text
            style={[
              Styles.button_font,
              {alignSelf: 'center', flex: 1, textAlign: 'center'},
            ]}>
            My Cars
          </Text>
          <Text
            style={[
              Styles.body_label,
              {
                color: COLOR.WHITE,
                textAlign: 'right',
                paddingRight: 20,
                width: 50,
              },
            ]}></Text>
        </View>
        <SafeAreaView
          style={[Styles.container,{backgroundColor:COLOR.BLUE_BG}]}>
          <TopTabBar props={this.props}/>
        </SafeAreaView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  navigation_bar: {
    height: getStatusBarHeight(true) + 45,
    backgroundColor: COLOR.DARK_BLUE,
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: getStatusBarHeight(true),
    justifyContent: 'center',
  },
  flatlist_view: {
    width: wp(100),
    marginHorizontal: 10,
    flex: 1,
  },
  
});
