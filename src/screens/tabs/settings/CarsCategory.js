import React from 'react'
import { View,FlatList,Text, Image, StyleSheet, TouchableOpacity, } from "react-native";
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import { widthPercentageToDP } from 'react-native-responsive-screen';
import COLOR from '../../../res/styles/Color';
import FONTS from '../../../res/styles/Fonts';
import IMAGES from '../../../res/styles/Images';
import Styles from '../../../res/styles/Styles';


export default class CarsCategory extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      carData: [],
    };
  }

  render() {
    return (
      <View style={[Styles.container, {backgroundColor: COLOR.BLUE_BG}]}>
        <FlatList
          style={[
            styles.flatlist_view,
            {alignSelf: 'center', flex: 1, paddingTop: 15},
          ]}
          data={this.state.carData}
          renderItem={({item}) => (
            <TouchableOpacity
              // onPress={() =>
              //   this.props.navigation.navigate('VehicleInspection')
              // }
              style={styles.car_view}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'flex-start',
                }}>
                <Image
                  source={item.image}
                  resizeMode={'cover'}
                  style={{
                    width: 110,
                    marginRight: 10,
                    height: 80,
                    borderRadius: 10,
                  }}
                />
                <View style={{flex: 1, marginVertical: 0}}>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      alignItems: 'flex-start',
                      flex: 1,
                    }}>
                    <View>
                      <Text
                        style={[
                          Styles.body_label,
                          {
                            width: null,
                            alignSelf: 'flex-start',
                          },
                        ]}>
                        BMW 3 Series
                      </Text>
                      <Text
                        style={[
                          Styles.body_label,
                          {
                            fontFamily: FONTS.FAMILY_SEMIBOLD,
                            width: null,
                            alignSelf: 'flex-start',
                          },
                        ]}>
                        $11,404.16
                      </Text>
                    </View>
                    <AnimatedCircularProgress
                      style={{backgroundColor: COLOR.WHITE}}
                      size={35}
                      width={3}
                      fill={parseInt(item.avgRating ? item.avgRating : 0)}
                      rotation={0}
                      minva
                      tintColor={COLOR.LIGHT_BLUE}
                      backgroundColor={COLOR.LIGHT_GRAY}>
                      {(fill) => (
                        <Text
                          style={[
                            Styles.small_label,
                            {
                              color: COLOR.LIGHT_BLUE,
                              fontFamily: FONTS.FAMILY_SEMIBOLD,
                            },
                          ]}>
                          {parseInt(item.avgRating ? item.avgRating : 0)}
                        </Text>
                      )}
                    </AnimatedCircularProgress>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      marginBottom: 0,
                    }}>
                    <Text
                      style={[
                        Styles.small_label,
                        {
                          color: COLOR.BLACK,
                          width: null,
                          fontFamily: FONTS.FAMILY_MEDIUM,
                        },
                      ]}>
                      65,000 kms
                    </Text>
                    <Text
                      style={[
                        Styles.small_label,
                        {
                          color: COLOR.LIGHT_BLUE,
                          width: null,
                          fontFamily: FONTS.FAMILY_MEDIUM,
                        },
                      ]}>
                      |
                    </Text>
                    <Text
                      style={[
                        Styles.small_label,
                        {
                          color: COLOR.BLACK,
                          width: null,
                          fontFamily: FONTS.FAMILY_MEDIUM,
                        },
                      ]}>
                      Diesel
                    </Text>
                    <Text
                      style={[
                        Styles.small_label,
                        {
                          color: COLOR.LIGHT_BLUE,
                          width: null,
                          fontFamily: FONTS.FAMILY_MEDIUM,
                        },
                      ]}>
                      |
                    </Text>
                    <Text
                      style={[
                        Styles.small_label,
                        {
                          color: COLOR.BLACK,
                          width: null,
                          fontFamily: FONTS.FAMILY_MEDIUM,
                        },
                      ]}>
                      1st owner
                    </Text>
                  </View>
                </View>
              </View>
            </TouchableOpacity>
          )}
          numColumns={1}
          showsVerticalScrollIndicator={false}
          showsHorizontalScrollIndicator={false}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  flatlist_view: {
    width: widthPercentageToDP(100),
    marginHorizontal: 10,
    flex: 1,
  },
  car_view: {
    width: widthPercentageToDP(90),
    height: 110,
    backgroundColor: COLOR.WHITE,
    marginBottom: 15,
    borderRadius: 5,
    paddingHorizontal: 10,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: COLOR.BLACK_GRAD,
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.3,
    elevation: 3,
  },
});