import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  FlatList,
  TextInput,
  TouchableOpacity,
  ImageBackground,
  ScrollView,
  TouchableWithoutFeedback,
} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import COLOR from '../../../res/styles/Color';
import FONTS from '../../../res/styles/Fonts';
import IMAGES from '../../../res/styles/Images';
import Styles from '../../../res/styles/Styles';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {AnimatedCircularProgress} from 'react-native-circular-progress';
import BezireLineChart from './LineChart';
import { connect } from 'react-redux';
import Actions from '../../../redux/actions/Actions';
import {SafeAreaView} from "react-native-safe-area-context";

class StatisticsScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedIndex: 0,
      dateType: 2,
      carData: [],
      vehicleTypes: [],
      statisticData: null
    };
     
    this.handleDateType = this.handleDateType.bind(this);
  }

  handleDateType = (val) => {
    this.setState({dateType: val});
  };

  componentDidMount() {
    this.props.getStatisticsData()
  }

  componentWillReceiveProps(props) {
    if (props.statisticData != this.props.statisticData) {
      this.setState({
        statisticData: props.statisticData
      })
    }
  }

  commonView = (props) => {
    return (
      <TouchableOpacity
        onPress={() => {
         // this.setState({selectedIndex: item.id});
        }}>
        <View
          style={[
            {
              borderRightColor: '#eaeaea',

              borderRightWidth: 1,
              justifyContent: 'center',
              alignItems: 'center',
              width: wp(90) / 4,
              height: 110,
              alignSelf: 'center',
            },
          ]}>
          <View
            style={{
              height: 50,
              width: 50,
              borderRadius: 25,
              borderWidth: 3,
              borderColor: COLOR.BLACK,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={[Styles.medium_label, {color: COLOR.LIGHT_BLUE}]}>
              {props.value}
            </Text>
          </View>
          <Text
            style={[
              Styles.small_label,
              {
                marginTop: 10,
                fontFamily: FONTS.FAMILY_MEDIUM,
                color: COLOR.BLACK,
              },
            ]}>
            {props.name}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }

  render() {
    const {selectedIndex, dateType, statisticData} = this.state;
    return (
      <SafeAreaView style={Styles.container}>
          <View style={{height:60, top: 0, position: 'absolute', backgroundColor: COLOR.DARK_BLUE, width: wp(100)}}/>
        <View style={styles.navigation_bar}>
          <View style={{width: 100}} />
          <Text
            style={[
              Styles.button_font,
              {alignSelf: 'center', flex: 1, textAlign: 'center', fontSize: 18},
            ]}>
            Statistics
          </Text>
          <Text
            style={[
              Styles.body_label,
              {
                color: COLOR.WHITE,
                textAlign: 'right',
                paddingRight: 20,
                width: 100,
              },
            ]}>
            {statisticData ? statisticData.totalVehicle : 0} Car(s)
          </Text>
        </View>
        <ScrollView
          showsVerticalScrollIndicator={false}
          style={{flex: 1, backgroundColor: COLOR.BLUE_BG}}>

          <View
            style={[
              styles.vehicle_type_view,
              {marginTop: 20, flexDirection: 'row'},
            ]}>
            <this.commonView
              name={'Cars Added'}
              value={statisticData ? statisticData.totalVehicle : 0}
            />
            <this.commonView
              name={'Cars Sold'}
              value={statisticData ? statisticData.sold : 0}
            />
            <this.commonView
              name={'Cars on Bid'}
              value={statisticData ? statisticData.onBid : 0}
            />
            <this.commonView
              name={'Direct Buy'}
              value={statisticData ? statisticData.directBuy : 0}
            />

          </View>
          <View
            style={{
              width: wp(90),

              alignSelf: 'center',
              padding: 15,
              backgroundColor: COLOR.WHITE,
              borderRadius: 10,
              shadowOffset: {width: 0.2, height: 0.2},
              shadowOpacity: 0.2,
              shadowColor: COLOR.GRAY,
              elevation: 3,
              paddingBottom: 0,
            }}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginVertical: 10,
              }}>
              <View>
                <Text style={Styles.medium_label}>
                  Total number of car registered and sold
                </Text>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginTop: 10,
                  }}>
                  <Image
                    style={{
                      height: 10,
                      width: 10,
                      marginRight: 5,
                      tintColor: 'FF6C02',
                    }}
                    resizeMode={'contain'}
                    source={IMAGES.blue_dot}
                  />
                  <Text
                    style={[
                      Styles.body_label,
                      {color: COLOR.GRAY, marginRight: 20},
                    ]}>
                    Registered
                  </Text>
                  <Image
                    style={{
                      height: 10,
                      width: 10,
                      marginHorizontal: 5,
                      tintColor: COLOR.BLACK,
                    }}
                    resizeMode={'contain'}
                    source={IMAGES.blue_dot}
                  />
                  <Text style={[Styles.body_label, {color: COLOR.GRAY}]}>
                    Sold
                  </Text>
                </View>
              </View>

              <TouchableOpacity>
                <Image
                  style={{height: 40, width: 40, marginHorizontal: 5}}
                  source={IMAGES.refresh_icon}
                  resizeMode={'contain'}
                 />
              </TouchableOpacity>
            </View>
            {/* <BezireLineChart /> */}
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  navigation_bar: {
    height: getStatusBarHeight(true) + 30,
    backgroundColor: COLOR.DARK_BLUE,
    flexDirection: 'row',
    alignItems: 'center',

    justifyContent: 'center',
  },
  vehicle_type_view: {
    width: wp(90),
    borderRadius: 10,
    backgroundColor: COLOR.WHITE,
    alignSelf: 'center',
    marginVertical: 20,
    shadowColor: COLOR.GRAY,
    shadowOpacity: 0.2,
    elevation: 3,
    shadowOffset: {width: 2, height: 2},
  },
  flatlist_view: {
    width: wp(100),
    marginHorizontal: 10,
    flex: 1,
  },
  car_on_bid: {
    backgroundColor: COLOR.WHITE,
    borderRadius: 10,
    padding: 0,
    width: wp(90),
    alignSelf: 'center',
    marginBottom: 20,
    shadowColor: COLOR.GRAY,
    shadowOpacity: 0.2,
    elevation: 3,
    shadowOffset: {width: 2, height: 2},
  },
  image_view: {
    height: 250,
    alignSelf: 'center',
    width: wp(90),
    marginBottom: 10,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    overflow: 'hidden',
  },
  search_view: {
    backgroundColor: COLOR.WHITE,
    borderRadius: 10,
    paddingHorizontal: 20,
    width: wp(90),
    alignSelf: 'center',
    height: 50,
    marginBottom: 20,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    shadowColor: COLOR.GRAY,
    shadowOpacity: 0.2,
    elevation: 3,
    shadowOffset: {width: 2, height: 2},
  },
  car_view: {
    width: wp(90),
    height: 110,
    backgroundColor: COLOR.WHITE,
    marginBottom: 10,
    borderRadius: 5,
    paddingHorizontal: 10,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: COLOR.BLACK_GRAD,
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.3,
    elevation: 3,
  },
});

const mapStateToProps = (state, ownProps) => {
  return {
     statisticData: state.reducer.statisticData
  }
}

const mapDispatchToProps = (Dispatch) => {
  return {
    getStatisticsData: (val) => {
      Dispatch(Actions.getStatisticData(val))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(StatisticsScreen);