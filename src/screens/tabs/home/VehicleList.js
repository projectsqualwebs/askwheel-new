import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  FlatList,
  TextInput,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import COLOR from '../../../res/styles/Color';
import FONTS from '../../../res/styles/Fonts';
import IMAGES from '../../../res/styles/Images';
import Styles from '../../../res/styles/Styles';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {AnimatedCircularProgress} from 'react-native-circular-progress';
import {BackView} from '../../../commonView/BackView';
import {DotView} from '../../../commonView/BackView';
import {color} from 'react-native-reanimated';

export default class VehicleList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedIndex: 1,
      carData: [
        {
          id: 1,
          name: 'BMW 3 series',
          image: IMAGES.white_car,
          price: '$11404.16',
          km: '65,000 kms',
          engine: 'Diesel',
          owner: '1st owner',
          percent: 7,
        },
        {
          id: 2,
          name: 'BMW 3 series',
          image: IMAGES.white_car,
          price: '$11404.16',
          km: '65,000 kms',
          engine: 'Diesel',
          owner: '1st owner',
          percent: 7,
        },
        {
          id: 3,
          name: 'BMW 3 series',
          image: IMAGES.white_car,
          price: '$11404.16',
          km: '65,000 kms',
          engine: 'Diesel',
          owner: '1st owner',
          percent: 7,
        },
        {
          id: 4,
          name: 'BMW 3 series',
          image: IMAGES.white_car,
          price: '$11404.16',
          km: '65,000 kms',
          engine: 'Diesel',
          owner: '1st owner',
          percent: 7,
        },
        {
          id: 5,
          name: 'BMW 3 series',
          image: IMAGES.white_car,
          price: '$11404.16',
          km: '65,000 kms',
          engine: 'Diesel',
          owner: '1st owner',
          percent: 7,
        },
        {
          id: 6,
          name: 'BMW 3 series',
          image: IMAGES.white_car,
          price: '$11404.16',
          km: '65,000 kms',
          engine: 'Diesel',
          owner: '1st owner',
          percent: 7,
        },
        {
          id: 7,
          name: 'BMW 3 series',
          image: IMAGES.white_car,
          price: '$11404.16',
          km: '65,000 kms',
          engine: 'Diesel',
          owner: '1st owner',
          percent: 7,
        },
        {
          id: 8,
          name: 'BMW 3 series',
          image: IMAGES.white_car,
          price: '$11404.16',
          km: '65,000 kms',
          engine: 'Diesel',
          owner: '1st owner',
          percent: 7,
        },
        {
          id: 9,
          name: 'BMW 3 series',
          image: IMAGES.white_car,
          price: '$11404.16',
          km: '65,000 kms',
          engine: 'Diesel',
          owner: '1st owner',
          percent: 7,
        },
        {
          id: 10,
          name: 'BMW 3 series',
          image: IMAGES.white_car,
          price: '$11404.16',
          km: '65,000 kms',
          engine: 'Diesel',
          owner: '1st owner',
          percent: 7,
        },
      ],
      vehicleTypes: [
        {
          id: 1,
          name: 'Hatchback',
          image: IMAGES.hatchback,
        },
        {
          id: 2,
          name: 'Sedan',
          image: IMAGES.sedan,
        },
        {
          id: 3,
          name: 'Coupe',
          image: IMAGES.coupe,
        },
        {
          id: 4,
          name: 'SUV/MUV',
          image: IMAGES.suv,
        },
        {
          id: 5,
          name: 'Minivan/Van',
          image: IMAGES.van,
        },
        {
          id: 6,
          name: 'Station Wagon',
          image: IMAGES.wagon,
        },
        {
          id: 7,
          name: 'Convertible',
          image: IMAGES.convertable,
        },
        {
          id: 8,
          name: 'Truck',
          image: IMAGES.truck,
        },
      ],
    };
  }

  componentDidMount() {
    if (this.props.route.params) {
      this.setState({
        selectedIndex: this.props.route.params.selectedIndex
          ? parseInt(this.props.route.params.selectedIndex)
          : 1,
      });
    }
  }

  render() {
    const {selectedIndex} = this.state;
    return (
      <View style={Styles.container}>
        <View style={styles.navigation_bar}>
          <View style={{flexDirection: 'row'}}>
            <BackView props={this.props} />
            <Text style={[Styles.button_font]}>Vehicle List</Text>
          </View>
          <TouchableOpacity style={[Styles.back_arrow]}>
            <Image
              style={{height: 25, width: 24, tintColor: COLOR.WHITE}}
              resizeMode={'contain'}
              source={IMAGES.search}
            />
          </TouchableOpacity>
        </View>
        <View style={{flex: 1, backgroundColor: COLOR.BLUE_BG}}>
          <View style={styles.vehicle_type_view}>
            <FlatList
              data={this.state.vehicleTypes}
              renderItem={({item}) => (
                <TouchableOpacity
                  onPress={() => {
                    this.setState({selectedIndex: item.id});
                  }}>
                  <View
                    style={[
                      {
                        borderRightColor: '#eaeaea',
                        borderRightWidth: 1,
                        justifyContent: 'center',
                        alignItems: 'center',
                        width: wp(90) / 4,
                        paddingVertical: 10,
                        alignSelf: 'center',
                      },
                    ]}>
                    <Image
                      style={{
                        width: 40,
                        height: 40,
                      }}
                      resizeMode={'contain'}
                      source={IMAGES.car_blue_bg}
                    />
                    <Text
                      style={[
                        Styles.small_label,
                        {
                          marginTop: 10,
                          fontFamily: FONTS.FAMILY_SEMIBOLD,
                          color:
                            selectedIndex == item.id
                              ? COLOR.LIGHT_BLUE
                              : COLOR.BLACK,
                        },
                      ]}>
                      {item.name}
                    </Text>
                  </View>
                </TouchableOpacity>
              )}
              horizontal={true}
              showsVerticalScrollIndicator={false}
              showsHorizontalScrollIndicator={false}
              keyExtractor={(item, index) => index.toString()}
            />
          </View>
          <FlatList
            style={[styles.flatlist_view, {alignSelf: 'center', flex: 1}]}
            data={this.state.carData}
            renderItem={({item}) => (
              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate('VehicleInspection')
                }>
                <View style={styles.car_on_bid}>
                  <View>
                    <ImageBackground
                      style={styles.image_view}
                      resizeMode={'cover'}
                      source={IMAGES.white_car}>
                      <AnimatedCircularProgress
                        style={{
                          top: 20,
                          right: 10,
                          position: 'absolute',
                          backgroundColor: COLOR.WHITE,
                          borderRadius: 25,
                        }}
                        size={50}
                        width={3}
                        fill={parseInt(item.avgRating ? item.avgRating : 0)}
                        rotation={0}
                        minva
                        tintColor={COLOR.LIGHT_BLUE}
                        backgroundColor={COLOR.LIGHT_GRAY}>
                        {(fill) => (
                          <Text
                            style={[
                              Styles.subheading_label,
                              {
                                color: COLOR.LIGHT_BLUE,
                                fontFamily: FONTS.FAMILY_SEMIBOLD,
                              },
                            ]}>
                            {parseInt(item.avgRating ? item.avgRating : 0)}
                          </Text>
                        )}
                      </AnimatedCircularProgress>
                      <View
                        style={{
                          height: 30,
                          bottom: 0,
                          position: 'absolute',
                          backgroundColor: COLOR.BLACK_GRAD,
                          width: wp(90),
                          paddingHorizontal: 10,
                          justifyContent: 'center',
                        }}>
                        <Text
                          style={[
                            Styles.subheading_label,
                            {
                              fontFamily: FONTS.FAMILY_MEDIUM,
                              color: COLOR.WHITE,
                            },
                          ]}>
                          BMW 3 Series 2018
                        </Text>
                      </View>
                    </ImageBackground>
                  </View>
                  <Text
                    style={[
                      {
                        width: wp(90),
                        alignSelf: 'center',
                        paddingHorizontal: 15,
                        marginBottom: 0,
                        fontSize: FONTS.SMALL,
                        fontFamily: FONTS.FAMILY_REGULAR,
                        marginBottom: 10,
                      },
                    ]}>
                    <DotView />
                    65,000kms{'  '}
                    <DotView />
                    Automatic{'  '}
                    <DotView />
                    1st owner{'  '}
                    <DotView />
                    2016-2017{'  \n'}
                    <DotView />
                    MP-04{'  '}
                    <DotView />
                    Dwarka, Delhi
                  </Text>
                </View>
              </TouchableOpacity>
            )}
            numColumns={1}
            ListHeaderComponent={() =>
              !this.state.carData.length ? (
                <Text style={{textAlign: 'center', color: COLOR.LIGHT_TEXT}}>
                  No Vehicle(s) Found
                </Text>
              ) : null
            }
            showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  navigation_bar: {
    height: getStatusBarHeight(true) + 45,
    backgroundColor: COLOR.DARK_BLUE,
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: getStatusBarHeight(true),
    justifyContent: 'space-between',
  },
  vehicle_type_view: {
    width: wp(90),
    borderRadius: 10,
    backgroundColor: COLOR.WHITE,
    alignSelf: 'center',
    marginVertical: 20,
    shadowColor: COLOR.GRAY,
    shadowOpacity: 0.2,
    elevation: 3,
    shadowOffset: {width: 2, height: 2},
  },
  flatlist_view: {
    width: wp(100),
    marginHorizontal: 10,
    flex: 1,
  },
  car_on_bid: {
    backgroundColor: COLOR.WHITE,
    borderRadius: 10,
    padding: 0,
    width: wp(90),
    alignSelf: 'center',
    marginBottom: 20,
    shadowColor: COLOR.GRAY,
    shadowOpacity: 0.2,
    elevation: 3,
    shadowOffset: {width: 2, height: 2},
  },
  image_view: {
    height: 250,
    alignSelf: 'center',
    width: wp(90),
    marginBottom: 10,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    overflow: 'hidden',
  },
});
