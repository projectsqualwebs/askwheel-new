import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  FlatList,
  TextInput,
  TouchableOpacity,
  ImageBackground,
    Alert
} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';

import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import COLOR from '../../../res/styles/Color';
import FONTS from '../../../res/styles/Fonts';
import IMAGES from '../../../res/styles/Images';
import Styles from '../../../res/styles/Styles';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {AnimatedCircularProgress} from 'react-native-circular-progress';
import {DotView} from '../../../commonView/BackView';
import {connect} from 'react-redux';
import Actions from '../../../redux/actions/Actions';
import {BODY_TYPE} from '../../../appData';
import Indicator from '../../../commonView/ActivityIndicator';
import * as Animatable from 'react-native-animatable';
import CountDown from 'react-native-countdown-component';
import {insertComma, getRemainingTime} from '../../../commonView/Helpers';
import FastImage from 'react-native-fast-image';
import database from '@react-native-firebase/database';
import {SafeAreaView} from "react-native-safe-area-context";

class AuctionScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedIndex: 0,
      carData: [],
      vehicleTypes: BODY_TYPE,
      isLoading: false,
      pageLoading:false,
    };
    this.props.getVehicleBasedonType(1,0,this.state.vehicleTypes[0].value,1);
    this.handleSelection = this.handleSelection.bind(this);
    this.handleAnimationEnd = this.handleAnimationEnd.bind(this);
  }

  componentDidMount() {
    this.handleRealtimeDB();
  }

  componentWillReceiveProps(props) {
    if (props.vehicleDataBasedonType != this.props.vehicleDataBasedonType) {
      this.setState({
        carData: props.vehicleDataBasedonType,
        isLoading: false,
        pageLoading:false,
      },()=>{
          // if(this.state.carData?.length > 0){
          //    this.handleRealtimeDB();
          // }
      });
    }


    if(props.typeCurrentPage != this.props.typeCurrentPage){
      this.setState({currentPage:this.props.typeCurrentPage})
    }
  }

  handleAnimationEnd = (index) => {
    let data = this.state.carData;
    data[index].isAnimating = false;
    this.setState({
      carData: data,
    });
  };

    getCommisionPrice = (percent, value) => {
        percent = percent / 100;
        value = value * percent;
        return value;
    };

  handleRealtimeDB = () => {
    database()
      .ref('highBid')
      .on('value', (snapshot) => {
            let carData = this.state.carData;
            snapshot.forEach((childSnapshot, i) => {
                    carData.forEach((val, index) => {
                        if (val.id == childSnapshot.key) {
                            console.log('called', val.id)
                            carData[index].highBid = childSnapshot.val().amount;
                            carData[index].isAnimating = true;
                        }
                    });
                    this.setState({
                        carData: carData,
                    });
            });
      });

    database()
      .ref('closedBids')
      .on('value', (snapshot) => {
        let carData = this.state.carData;
        snapshot.forEach((childSnapshot, i) => {
          carData.forEach((val, index) => {
            if (val.id == childSnapshot.key) {
              carData[index].closeForBid = childSnapshot.val().closeForBid;
              carData[index].isAnimating = true;
              this.props.getVehicleOnBid(1,0);
            }
          });
          this.setState({
            carData: carData,
          });
        });
      });

    database()
      .ref('vehicleBidTime')
      .on('value', (snapshot) => {
        let carData = this.state.carData;
        snapshot.forEach((childSnapshot, i) => {
          carData.forEach((val, index) => {
            if (val.id == childSnapshot.key) {
              carData[index].bidCloseTime = childSnapshot.val().bidCloseTime;
            }
          });
          this.setState({
            carData: carData,
          });
        });
      });

    database()
      .ref('bidVehicles')
      .on('value', (snapshot) => {
        if (this.state.handlerSet == false) {
          this.setState({
            handlerSet: true,
          });
        } else {
          this.props.getVehicleOnBid(1,0);
        }
      });

    database()
      .ref('pausedBids')
      .on('value', (snapshot) => {
        if (this.state.handlerSet == false) {
          this.setState({
            handlerSet: true,
          });
        } else {
          this.props.getVehicleOnBid(1,0);
        }
      });
  };

  handleSelection(item) {
    if (this.state.selectedIndex != item.id) {
      this.props.getVehicleBasedonType(1,0,item.value,1);
      this.setState({selectedIndex: item.id, carData: [], isLoading: true});
    }
  }

  render() {
    const {selectedIndex, carData, isLoading,currentPage,pageLoading} = this.state;
    return (
      <SafeAreaView style={Styles.container}>
          <View style={{height:60, top: 0, position: 'absolute', backgroundColor: COLOR.DARK_BLUE, width: wp(100)}}/>
        <Indicator loading={isLoading} />
        <View style={styles.navigation_bar}>
          <View style={{flex: 1}} />
          <Text style={[Styles.button_font, {alignSelf: 'center', flex: 1,fontSize: 18}]}>
            Auction Board
          </Text>
          <Text
            style={[
              Styles.body_label,
              {
                color: COLOR.WHITE,
                flex: 1,
                textAlign: 'right',
                paddingRight: 20,
              },
            ]}>
            {carData.length} Car(s)
          </Text>
        </View>
        <View style={{flex: 1, backgroundColor: COLOR.BLUE_BG}}>
          <View style={styles.vehicle_type_view}>
            <FlatList
              data={this.state.vehicleTypes}
              renderItem={({item}) => (
                <TouchableOpacity onPress={() => this.handleSelection(item)}>
                  <View
                    style={[
                      {
                        borderRightColor: '#eaeaea',
                        borderRightWidth: 1,
                        justifyContent: 'center',
                        alignItems: 'center',
                        width: wp(90) / 4,
                        height: 80,
                        alignSelf: 'center',
                      },
                    ]}>
                    <View
                      style={{
                        height: 40,
                        width: 40,
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor:
                          selectedIndex == item.id
                            ? COLOR.DARK_BLUE
                            : COLOR.GRAY,
                        borderRadius: 20,
                      }}>
                      <Image
                        style={{
                          width: 30,
                          height: 30,
                        }}
                        resizeMode={'contain'}
                        source={item.image}
                      />
                    </View>
                    <Text
                      style={[
                        Styles.small_label,
                        {
                          marginTop: 10,
                            height: 15,
                          fontFamily: FONTS.FAMILY_SEMIBOLD,
                          color:
                            selectedIndex == item.id
                              ? COLOR.DARK_BLUE
                              : COLOR.GRAY,
                        },
                      ]}>
                      {item.value}
                    </Text>
                  </View>
                </TouchableOpacity>
              )}
              horizontal={true}
              showsVerticalScrollIndicator={false}
              showsHorizontalScrollIndicator={false}
              keyExtractor={(item, index) => index.toString()}
            />
          </View>
          <FlatList
          onScroll={e => {
            let paddingToBottom = 10;
            paddingToBottom += e.nativeEvent.layoutMeasurement.height;
            var currentOffset = e.nativeEvent.contentOffset.y;
            var direction = currentOffset > this.offset ? 'down' : 'up';
            if (direction === 'up') {
              if (
                e.nativeEvent.contentOffset.y >=
                e.nativeEvent.contentSize.height - paddingToBottom
              ) {
                if(!this.state.pageLoading){
                  this.setState(
                    {
                      pageLoading:true,
                    },
                    () => {
                      setTimeout(() => {
                        this.props.getVehicleBasedonType(1,this.state.carData?.length ?? 0,this.state.vehicleTypes[this.state.selectedIndex].value,0);
                      }, 1000);
                    },
                  );
                }
              }
            }
          }}
            style={[
              styles.flatlist_view,
              {alignSelf: 'center', flex: 1, paddingTop: 0},
            ]}
            data={this.state.carData}
            renderItem={({item, index}) => (
              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate('VehicleInspection', {
                    data: item,
                  })
                }>
                <Animatable.View
                  animation={item.isAnimating ? 'shake' : ''}
                  iterationCount={1}
                  duration={500}
                  onAnimationEnd={() => this.handleAnimationEnd(index)}>
                  <View
                    style={[
                      styles.car_on_bid,
                      {width: wp(90), marginBottom: 20},
                    ]}>
                    {item.closeForBid == false && (
                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                          alignItems: 'center',
                          width: wp(90),
                          alignSelf: 'center',
                          paddingHorizontal: 10,
                        }}>
                        <View
                          style={{
                            borderRadius: 10,
                            marginTop: 10,
                            flex: 1,
                            paddingHorizontal: 0,
                            alignItems: 'flex-start',
                          }}>
                          <Text
                            style={[
                              Styles.body_label,
                              {
                                color: COLOR.BLACK,
                                fontFamily: FONTS.FAMILY_SEMIBOLD,
                                textAlign: 'left',
                              },
                            ]}>
                            Highest Bid: ₹
                            {item.highBid
                              ? insertComma(
                                  item.highBid -
                                    parseFloat(
                                      this.getCommisionPrice(
                                        item.commissionPercent,
                                        item.highBid,
                                      ),
                                    ).toFixed(2),
                                )
                              : insertComma(item.expectedPrice)}
                          </Text>
                          {(item.highBid != null ||
                            item.closeForBid == false) && (
                            <Text
                              style={[
                                Styles.small_label,
                                {
                                  color: COLOR.GREEN,
                                  textAlign: 'left',
                                  marginVertical: 5,
                                  fontFamily: FONTS.FAMILY_SEMIBOLD,
                                },
                              ]}>
                              Scheduled for Bidding
                            </Text>
                          )}
                        </View>
                        <View
                          style={{
                            borderRadius: 10,
                            flexDirection: 'row',
                            paddingHorizontal: 0,
                            alignItems: 'center',
                          }}>
                          <Image
                            style={{
                              height: 22,
                              width: 22,
                              marginRight: 5,
                              tintColor: COLOR.BLACK,
                            }}
                            source={IMAGES.clock}
                            resizeMode={'contain'}
                          />

                          <CountDown
                            style={{
                              marginRight: 10,
                              alignItems: 'center',
                              justifyContent: 'center',
                            }}
                            until={
                              item.closeForBid
                                ? 0
                                : getRemainingTime(item.bidCloseTime)
                            }
                            digitStyle={{
                              backgroundColor: 'transparent',
                              width: 20,
                            }}
                            digitTxtStyle={{
                              color: COLOR.BLACK,
                              width: 20,
                            }}
                            timeLabels={{m: null, s: null}}
                            timeToShow={['H', 'M', 'S']}
                            size={15}
                            separatorStyle={{
                              color: COLOR.BLACK,
                              marginBottom: 5,
                            }}
                            onFinish={() => {
                              let data = carData;
                              data[index].closeForBid = true;
                              this.setState({
                                vehicleData: data,
                              });
                            }}
                            showSeparator={true}
                          />
                          <Text style={{color: COLOR.LIGHT_TEXT, fontSize: 12}}>
                            left
                          </Text>
                        </View>
                      </View>
                    )}
                    <View
                      style={{
                        height: 250,
                        alignSelf: 'center',
                        width: wp(90),
                        marginTop: 0,
                      }}>
                      <FastImage
                        style={{
                          flex: 1,
                          height: 250,
                          overflow: 'hidden',
                        }}
                        resizeMode={FastImage.resizeMode.cover}
                        source={
                          item.files.length > 0
                            ? {
                                uri: item.files[0],
                                priority: FastImage.priority.normal,
                              }
                            : IMAGES.white_car
                        }>
                        <View
                          style={{
                            bottom: 0,
                            position: 'absolute',
                            backgroundColor: COLOR.BLACK_GRAD,
                            width: wp(90),
                            paddingHorizontal: 10,
                            justifyContent: 'center',
                            alignSelf: 'center',
                          }}>
                          <Text
                            style={[
                              Styles.subheading_label,
                              {
                                fontFamily: FONTS.FAMILY_MEDIUM,
                                color: COLOR.WHITE,
                                fontSize: 17,
                              },
                            ]}>
                            {item.selectedMake
                              ? item.selectedMake.label
                              : item.make}{' '}
                            {item.selectedModel
                              ? item.selectedModel.label
                              : item.model}
                            (
                            {item.selectedVarient
                              ? item.selectedVarient.label
                              : item.variant}
                            )
                          </Text>
                        </View>
                      </FastImage>
                    </View>
                    <Text
                      style={[
                        {
                          width: wp(90),
                          alignSelf: 'center',
                          paddingHorizontal: 15,
                          marginVertical: 10,
                          fontSize: FONTS.SMALL,
                          fontFamily: FONTS.FAMILY_REGULAR,
                        },
                      ]}>
                      <DotView />
                      {insertComma(item.odometerReading)}kms{'  '}
                      <DotView />
                      {item.transmission}
                      {'  '}
                      <DotView />
                      {item.numberOfOwners == 1
                        ? `${item.numberOfOwners}st`
                        : item.numberOfOwners == 2
                        ? `${item.numberOfOwners}nd`
                        : item.numberOfOwners == 3
                        ? `${item.numberOfOwners}rd`
                        : `${item.numberOfOwners}th`}{' '}
                      owner{'  '}
                      <DotView />
                      {item.manufacturingYear}
                      {'  \n'}
                      <DotView />
                      {item.rtoState ? item.rtoState : ''}
                      {'  '}
                      <DotView />
                      {item.pickupAddress ? item.pickupAddress : ''}
                    </Text>
                  </View>
                </Animatable.View>
              </TouchableOpacity>
            )}
            numColumns={1}
            ListHeaderComponent={() =>
              !this.state.carData.length ? (
                <Text style={{textAlign: 'center', color: COLOR.LIGHT_TEXT}}>
                  No Vehicle in Auction
                </Text>
              ) : null
            }
            showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  navigation_bar: {
    height: getStatusBarHeight(true) + 30,
    backgroundColor: COLOR.DARK_BLUE,
    flexDirection: 'row',
    alignItems: 'center',

    justifyContent: 'center',
  },
  vehicle_type_view: {
    width: wp(90),
    borderRadius: 10,
    backgroundColor: COLOR.WHITE,
    alignSelf: 'center',
    marginVertical: 20,
    shadowColor: COLOR.GRAY,
    shadowOpacity: 0.2,
    elevation: 3,
    shadowOffset: {width: 2, height: 2},
  },
  flatlist_view: {
    width: wp(100),
    marginHorizontal: 10,
    flex: 1,
  },
  car_on_bid: {
    backgroundColor: COLOR.WHITE,
    borderRadius: 10,
    padding: 0,
    width: wp(90),
    alignSelf: 'center',
    marginBottom: 20,
    shadowColor: COLOR.GRAY,
    shadowOpacity: 0.2,
    elevation: 3,
    shadowOffset: {width: 2, height: 2},
  },
  image_view: {
    height: 250,
    alignSelf: 'center',
    width: wp(90),
    marginBottom: 10,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    overflow: 'hidden',
  },
  search_view: {
    backgroundColor: COLOR.WHITE,
    borderRadius: 10,
    paddingHorizontal: 20,
    width: wp(90),
    alignSelf: 'center',
    height: 50,
    marginBottom: 20,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    shadowColor: COLOR.GRAY,
    shadowOpacity: 0.2,
    elevation: 3,
    shadowOffset: {width: 2, height: 2},
  },
});

const mapStateToProps = (state, ownProps) => {
  return {
    vehicleDataBasedonType: state.reducer.vehicleDataBasedonType,
    typeCurrentPage: state.reducer.typeCurrentPage,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getVehicleBasedonType: (val,page,type,tabChanged) => {
      dispatch(Actions.getVehicleBasedonType(val,page,type,tabChanged));
    },
    getVehicleOnBid: (type,page) => {
      dispatch(Actions.getVehicleOnBid(type,page));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AuctionScreen);
