import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  FlatList,
  TextInput,
  TouchableOpacity,
  ImageBackground,
  SafeAreaView,
  Alert,
} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';

import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import COLOR from '../../res/styles/Color';
import FONTS from '../../res/styles/Fonts';
import IMAGES from '../../res/styles/Images';
import Styles from '../../res/styles/Styles';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {AnimatedCircularProgress} from 'react-native-circular-progress';
import {BackView} from '../../commonView/BackView';
import Actions from '../../redux/actions/Actions';
import {connect} from 'react-redux';
import store from '../../../src/redux/Store';
import API from '../../api/Api';
import Toast from 'react-native-simple-toast';
import Indicator from '../../commonView/ActivityIndicator';

const api = new API();

class CarInspection extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      selectedIndex: 0,
      vehicleData: null,
    };
    this.props.getInspectionDetail(null, 1);
    this.handleNavigation = this.handleNavigation.bind(this);
  }

  componentDidMount() {
    this.setState({
      vehicleData: this.props.inspectionData,
    });

    this._unsubscribe = this.props.navigation.addListener('focus', () => {
      this.setState({
        vehicleData: this.props.inspectionData,
      });
    });
  }

  componentWillUnmount() {
    this._unsubscribe();
  }

  handleNavigation = (index) => {
    this.props.navigation.navigate('Exterior', {inspectionIndex: index});
  };

  callInspectionApi = () => {
    let that = this;
    let vehicle = this.state.vehicleData
    vehicle.rtoState = vehicle.state
    let data = JSON.stringify(vehicle);
    this.setState({ isLoading: true });
    api
      .saveInspectionData(data)
      .then((json) => {
        console.log('json data is', json);
        if (json.status == 200) {
          that.setState({ isLoading: false });
          Toast.show('Successfully listed Vehicle');
          this.props.navigation.navigate('HomeStack');
          this.props.getInspectionDetail(null, 3);
          this.props.getListedVehicles(1,0);
          
        } else {
          that.setState({isLoading: false});
          Toast.show(json.data.message);
        }
      })
      .catch(function (error) {
        Alert.alert(error.response.data.details);
        console.log('json error is:-', error.response);
        that.setState({isLoading: false});
        Toast.show(error.response.data.details);
      });
  };

  render() {
    const {selectedIndex, vehicleData, isLoading} = this.state;
    return (
      <View style={[Styles.container, {backgroundColor: COLOR.BLUE_BG}]}>
        <Indicator loading={isLoading} />
        <View style={styles.navigation_bar}>
          <View style={{flexDirection: 'row'}}>
            <BackView props={this.props} />
            <Text style={[Styles.button_font]}>Car Inspection</Text>
          </View>
        </View>
        <SafeAreaView style={Styles.container}>
          <View
            style={{
              flex: 1,
              backgroundColor: COLOR.BLUE_BG,
              paddingVertical: 10,
            }}>
            <FlatList
              style={{marginHorizontal: 5, alignSelf: 'center'}}
              data={vehicleData ? vehicleData.bodyFrame : []}
              renderItem={({item, index}) => (
                <TouchableOpacity onPress={() => this.handleNavigation(index)}>
                  <View
                    style={[
                      Styles.shadow_view,
                      {
                        alignItems: 'center',
                        justifyContent: 'center',
                        alignSelf: 'center',
                        aspectRatio: 1,
                        margin: 5,
                        width: wp(45),
                      },
                    ]}>
                    <AnimatedCircularProgress
                      style={{
                        backgroundColor: COLOR.WHITE,
                        borderRadius: 25,
                        marginBottom: 10,
                      }}
                      size={wp(18)}
                      width={5}
                      fill={parseInt(item.rating ? item.rating : 0)}
                      rotation={0}
                      minva
                      tintColor={COLOR.LIGHT_BLUE}
                      backgroundColor={COLOR.LIGHT_GRAY}>
                      {(fill) => (
                        <Image
                          style={{height: 40, width: 40}}
                          source={item.image}
                          resizeMode={'contain'}
                        />
                      )}
                    </AnimatedCircularProgress>
                    <Text
                      style={[
                        Styles.body_label,
                        {
                          margin: 0,
                          fontFamily: FONTS.FAMILY_MEDIUM,
                          marginHorizontal: 10,
                          textAlign: 'center',
                        },
                      ]}>
                      {item.name}
                      
                    </Text>
                    <Text
                      style={[
                        Styles.small_label,
                        {
                          margin: 0,
                          color: COLOR.LIGHT_TEXT,
                        },
                      ]}>
                      {item.issueListed ? item.issueListed : 0} issue(s) listed
                    </Text>
                  </View>
                </TouchableOpacity>
              )}
              numColumns={2}
              showsVerticalScrollIndicator={false}
              showsHorizontalScrollIndicator={false}
              bounces={false}
              keyExtractor={(item, index) => index.toString()}
            />
          </View>
          <TouchableOpacity
            onPress={() => this.callInspectionApi()}
            style={[
              Styles.button_content,
              {marginVertical: 0, width: wp(100), borderRadius: 0},
            ]}>
            <Text style={Styles.button_font}>Confirm Details</Text>
          </TouchableOpacity>
        </SafeAreaView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  navigation_bar: {
    height: getStatusBarHeight(true) + 45,
    backgroundColor: COLOR.DARK_BLUE,
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: getStatusBarHeight(true),
    justifyContent: 'space-between',
  },
});

const mapStateToProps = (state, ownProps) => {
  return {
    inspectionData: state.reducer.inspectionDetail,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getInspectionDetail: (data, val) => {
      dispatch(Actions.inspectionDetail(data, val));
    },
    getListedVehicles: (val,page) => {
      dispatch(Actions.getListedVehicles(val,page));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CarInspection);
