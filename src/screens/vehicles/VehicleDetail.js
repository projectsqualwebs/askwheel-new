import React from 'react';
import {
  View,
  Image,
  Text,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  Platform,
} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import IMAGES from '../../res/styles/Images';
import Styles from '../../res/styles/Styles';
import FONTS from '../../res/styles/Fonts';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import COLOR from '../../res/styles/Color';
import {BackView} from '../../commonView/BackView';
import {Pages} from 'react-native-pages';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {TextInput} from 'react-native-gesture-handler';
import DropDownPicker from 'react-native-dropdown-picker';
import actions from '../../redux/actions/Actions';
import {connect} from 'react-redux';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import {getCountries, getYears} from '../../commonView/Helpers';
import database from '@react-native-firebase/database';

import {
  TRANSMISSION_TYPE,
  getVehicleInspectionCategory,
  BODY_TYPE,
  BODY_COLOR,
  REGISTRATION_STATE,
  COUNTRY_DATA,
  ACCIDENTAL_DATA,
  RC_AVAILABLE_DATA,
  POLLUTION_DATA,
  OWNER_DATA,
  FUEL_TYPE_DATA,
  INSURANCE_TYPE_DATA,
  UNDER_HYPOTHYCATION,
  FINANCER_DATA,
  GAS_FITMENT_DATA,
  FITMENT_ENDORSEMENT_DATA,
  CNG_REMOVAL_DATA,
  CNG_CERTIFIED_PLATE,
  CHASSIS_EMBOSSING,
  RC_MISMATCH,
  NO_OF_KEYS,
  LOAN_REMOVAL,
  RTO_NOC_ISSUED,
  BANKER_NOC_ISSUED_DATA,
} from '../../appData';
import Moment from 'moment';
import ImagePicker from 'react-native-image-crop-picker';
import Toast from 'react-native-simple-toast';
import {U_BASE, U_FILE_UPLOAD, U_IMAGE_BASE} from '../../res/Constants';
import Indicator from '../../commonView/ActivityIndicator';
import ActionSheet from '../../commonView/ActionSheet';
import FastImage from 'react-native-fast-image';

class VehicleDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      actionModalVisible: false,
      isGalleryViewVisible: false,
      language: '',
      currentPicker: 1,
      currentDatePicker: '',
      makeData: [],
      variantData: [],
      modalData: [],
      stateData: [],
      countryData: COUNTRY_DATA,
      manufacturingYear: [],
      accidental: ACCIDENTAL_DATA,
      rcAvailableData: RC_AVAILABLE_DATA,
      pollutionData: POLLUTION_DATA,
      ownerData: OWNER_DATA,
      fuelTypeData: FUEL_TYPE_DATA,
      financerData: FINANCER_DATA,
      hypothycationData: UNDER_HYPOTHYCATION,
      insuranceTypeData: INSURANCE_TYPE_DATA,
      colorData: [],
      bodyTypeData: BODY_TYPE,
      gasFitmentData: GAS_FITMENT_DATA,
      fitmentEndorsementData: FITMENT_ENDORSEMENT_DATA,
      cngRemovalData: CNG_REMOVAL_DATA,
      cngCertifiedPlate: CNG_CERTIFIED_PLATE,
      nocIssuedData: RTO_NOC_ISSUED,
      embossingData: CHASSIS_EMBOSSING,
      rcMismatchData: RC_MISMATCH,
      keysData: NO_OF_KEYS,
      loanRemovalData: LOAN_REMOVAL,
      bankerNocIssuedData: BANKER_NOC_ISSUED_DATA,
      currentImageField: '',
      pickerMode: null,
      setPickerMode: null,
      pickerHeading: '',
      inspectionData: {
        selectedMake: null,
        selectedVarient: null,
        selectedModel: null,
        FitmentEndorsementRC: '',
        make: '',
        variant: '',
        model: '',
        color: '',
        bodyType: '',
        engineNumber: '',
        chassisNumber: '',
        loanNumber: '',
        typeOfInsurance: '',
        insuranceValidityDate: '',
        underHypothecation: '',
        financer: '',
        gasFitment: '',
        gasRemoval: '',
        remark: '',
        manufacturingYear: '',
        transmission: '',
        manufacturingCountry: '',
        rtoState: '',
        state: '',
        accidental: '',
        manufacturingDate: '',
        registrationDate: '',
        files: [],
        registrationImage: null,
        registrationNumber: '',
        numberOfOwners: '',
        odometerReading: '',
        fuelType: '',
        expectedPrice: '',
        bodyFrame: getVehicleInspectionCategory(),
        pollutionCard: '',
        odometerReadingImage: null,
        isFinancer: false,
        insuranceTypeImage: '',
        cngRemoval: '',
        fitmentUserName: '',
        fitmentValidity: '',
        certificatePlate: '',
        nocIssued: '',
        chassisEmbossing: '',
        rcMismatch: '',
        rcAvailable: '',
        noOfKeys: '',
        loanNoRemoval: '',
        chassisEmbossingImage: '',
        engineNumberImage: '',
        isAnimating: false,
        bankerNocIssued: '',
        pickupAddress: '',
      },
      galleryData: [],
    };

    this.handleDropdownValue = this.handleDropdownValue.bind(this);
    this.handleSelectedDropdownVal = this.handleSelectedDropdownVal.bind(this);
    this.dropdown = this.dropdown.bind(this);
    this.chooseImage = this.chooseImage.bind(this);
    this.callUploadImage = this.callUploadImage.bind(this);
    this.updateMasterState = this.updateMasterState.bind(this);
    this.handleNavigation = this.handleNavigation.bind(this);
    this.handleGallery = this.handleGallery.bind(this);
  }

  async componentDidMount() {
    this.props.getMakeData();
    this.props.inspectionDetail(null, 1);
    let years = getYears('manufacturingYear');
    this.setState(
      {
        manufacturingYear: years,
        isLoading: true,
      },
      (val) => {
        console.log(
          'this.props.vehicleInspectionData',
          this.props.vehicleInspectionData,
        );
        if (this.props.vehicleInspectionData) {
          if (
            this.props.vehicleInspectionData.id != null ||
            this.props.vehicleInspectionData.id != undefined
          ) {
            this.props.getModalData(this.props.vehicleInspectionData.make);
            this.props.getVarientData(this.props.vehicleInspectionData.model);
            this.props.getColorData(this.props.vehicleInspectionData.model);
            this.setState({
              inspectionData: this.props.vehicleInspectionData,
            });
          }
        }
      },
    );
  }

  componentWillUnmount() {
    this.props.inspectionDetail(null, 3);
  }

  componentWillReceiveProps(props) {
    if (props.makeData != this.props.makeData) {
      this.handleDropdownValue(props.makeData, 'makeData', 'make');
    }

    if (props.variantData != this.props.variantData) {
      this.handleDropdownValue(props.variantData, 'variantData', 'variant');
    }

    if (props.modelData != this.props.modalData) {
      this.handleDropdownValue(props.modalData, 'modalData', 'model');
    }

    if (props.colorData != this.props.colorData) {
      console.log('color is:=-', props.colorData);
      this.handleDropdownValue(props.colorData, 'colorData', 'color');
    }
  }

  handleGallery = (val, data) => {
    let that = this;
    let array = [];
    if (data) {
      for (let i = 0; i < data.length; i++) {
        array.push(data[i]);
      }
    }
    that.setState({
      isGalleryViewVisible: !that.state.isGalleryViewVisible,
      galleryData: val == 1 ? this.state.inspectionData.files : array,
    });
  };

  updateMasterState = (attrName, value) => {
    if (value == 'state') {
      this.setState({
        inspectionData: {
          ...this.state.inspectionData,
          state: attrName,
          registrationNumber: attrName,
        },
      });
    } else {
      this.setState({
        inspectionData: {
          ...this.state.inspectionData,
          [value]: attrName,
        },
      });
    }
  };

  getFormattedPhoneNum(input, value) {
    let output = '';
    input.replace(
      /^[A-Z]{2}[ -][0-9]{1,2}(?: [A-Z])?(?: [A-Z]*)? [0-9]{4}$/,
      function (match, g1, g2, g3) {
        if (g1.length) {
          output += g1;
          if (g1.length == 3) {
            output += ')';
            if (g2.length) {
              output += ' ' + g2;
              if (g2.length == 3) {
                output += ' - ';
                if (g3.length) {
                  output += g3;
                }
              }
            }
          }
        }
      },
    );
    return output;
  }

  showDatePicker = (val) => {
    this.setState({
      pickerMode: true,
      currentDatePicker: val,
    });
  };

  hidePicker = () => {
    this.setState({
      pickerMode: false,
      currentDatePicker: '',
    });
  };

  handleConfirm = (date) => {
    if (this.state.currentDatePicker == 'manufacturingDate') {
      this.setState({
        inspectionData: {
          ...this.state.inspectionData,
          manufacturingDate: Moment(date).format('DD/MM/yyyy'),
        },
      });
    } else if (this.state.currentDatePicker == 'registrationDate') {
      this.setState({
        inspectionData: {
          ...this.state.inspectionData,
          registrationDate: Moment(date).format('DD/MM/yyyy'),
        },
      });
    } else if (this.state.currentDatePicker == 'insuranceValidityDate') {
      this.setState({
        inspectionData: {
          ...this.state.inspectionData,
          insuranceValidityDate: Moment(date).format('DD/MM/yyyy'),
        },
      });
    } else if (this.state.currentDatePicker == 'fitmentValidity') {
      this.setState({
        inspectionData: {
          ...this.state.inspectionData,
          fitmentValidity: Moment(date).format('MM-yyyy'),
        },
      });
    }

    this.setState({
      pickerMode: null,
      currentDatePicker: '',
    });
    // this.hidePicker();
  };

  handleCancel = () => {
    this.setState({
      pickerMode: null,
      currentDatePicker: '',
    });
  };

  dropdown = (props) => {
    return (
      <View
        style={{
          ...(Platform.OS !== 'android' && {
            zIndex: props.zIndex,
          }),
        }}>
        <Text
          style={{
            paddingTop: 10,
            marginBottom: 0,
            fontSize: 10,
            paddingLeft: 15,
            color: COLOR.GRAY,
            backgroundColor: COLOR.WHITE,
          }}>
          {props.placeholder}
        </Text>
        <View
          style={{
            height: 50,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <DropDownPicker
            items={props.data ? props.data : []}
            searchable={false}
            searchableError={() => <Text>Not Found</Text>}
            searchablePlaceholderTextColor={COLOR.GRAY}
            defaultValue={
              props.data.length > 0
                ? props.name
                  ? props.name == '1'
                    ? props.name
                    : props.name == true
                    ? 'YES'
                    : props.name == false
                    ? 'NO'
                    : String(props.name)
                  : null
                : null
            }
            placeholder={''}
            zIndex={props.zIndex}
            placeholderStyle={{color: COLOR.LIGHT_TEXT, fontSize: 14}}
            labelStyle={{
              fontSize: FONTS.MEDIUM,
              fontFamily: FONTS.FAMILY_MEDIUM,
              color: COLOR.BLACK,
            }}
            dropDownMaxHeight={300}
            style={{
              backgroundColor: '#ffffff',
              shadowOpacity: 0,
              borderWidth: 0,
              height: 50,
              width: wp(90),
              alignSelf: 'center',
              borderTopLeftRadius: 0,
              borderTopRightRadius: 0,
              borderBottomLeftRadius: 0,
              borderBottomRightRadius: 0,
            }}
            dropDownStyle={{
              backgroundColor: '#fff',
            }}
            containerStyle={{
              borderRadius: 0,
            }}
            onChangeItem={(item) => {
              this.handleSelectedDropdownVal(item);
              console.log('props.name', item);
            }}
          />
        </View>
      </View>
    );
  };

  imageView = () => {
    return;
  };

  handleDropdownValue(val, name, key) {
    var dropdownData = [];
    for (let i = 0; i < val.length; i++) {
      if (val[i].name !== '') {
        let text =
          key == 'color'
            ? val[i].color.toUpperCase()
            : val[i].name.toUpperCase();

        dropdownData.push({
          label: text,
          value: key == 'color' ? val[i].color : val[i].id,
          id: val[i].id,
          key: key,
        });
      }
    }
    this.setState({
      isLoading: false,
    });

    this.setState({[name]: dropdownData});
  }

  handleSelectedDropdownVal(item, id) {
    let that = this;
    let value = item.value;
    that.setState(
      {
        inspectionData: {
          ...that.state.inspectionData,
          [item.key]: value,
        },
      },
      (val) => {
        if (item.key == 'make') {
          that.setState(
            {
              isLoading: true,
              inspectionData: {
                ...that.state.inspectionData,
                modelData: [],
                variantData: [],
                selectedMake: item,
                selectedModel: null,
                selectedVarient: null,
                model: '',
                variant: '',
              },
            },
            (cal) => {
              console.log('that.inspectionData', that.state.inspectionData);
              that.props.getModalData(item.id);
            },
          );
        } else if (item.key == 'variant') {
          that.setState({
            inspectionData: {
              ...that.state.inspectionData,
              selectedVarient: item,
            },
          });
        } else if (item.key == 'model') {
          that.props.getVarientData(item.id);
          that.props.getColorData(item.id);
          that.setState({
            isLoading: true,
            inspectionData: {
              ...that.state.inspectionData,
              selectedModel: item,
              variantData: [],
              selectedVarient: null,
              variant: '',
            },
          });
        } else if (item.key == 'rtoState') {
          that.setState({
            inspectionData: {
              ...that.state.inspectionData,
              state: item.value,
            },
          });
        }
      },
    );
  }

  chooseImage = (val) => {
    let options = {
      title: 'Select Image',
      width: 1630,
      height: 750,
      compressImageMaxWidth: 1630,
      compressImageMaxHeight: 750,
      multiple: true,
    };

    if (val == 1) {
      ImagePicker.openCamera({
        title: 'Select Image',
        width: 1024,
        height: 472,
        compressImageMaxWidth: 1024,
        compressImageMaxHeight: 472,
      }).then((response) => {
        database()
          .ref('error')
          .set({
            name: 'response is',
            error: JSON.stringify(response),
          })
          .then(() => console.log('Data set.'));
        if (response.didCancel) {
          console.log('User cancelled  Camera');
        } else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        } else if (response.customButton) {
          console.log('User tapped custom button: ', response.customButton);
          alert(response.customButton);
        } else {
          this.setState({
            isLoading: true,
            actionModalVisible: false,
          });
          this.callUploadImage(response, val);
        }
      });
    } else if (val == 2) {
      ImagePicker.openPicker(options).then((response) => {
        console.log('image local reposnse', response);
        database()
          .ref('error')
          .set({
            name: 'response is',
            error: JSON.stringify(response),
          })
          .then(() => console.log('Data set.'));
        if (response.didCancel) {
          console.log('User cancelled image picker');
        } else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        } else if (response.customButton) {
          console.log('User tapped custom button: ', response.customButton);
          alert(response.customButton);
        } else {
          this.setState({
            isLoading: true,
            actionModalVisible: false,
          });
          for (let i = 0; i < response.length; i++) {
            this.callUploadImage(response[i], val);
          }
        }
      });
    }
  };

  callUploadImage(res, val) {
    var data = new FormData();
    var photo = {
      uri: res.path,
      type: res.type ? res.type : res.mime,
      name: 'image.jpg',
    };
    data.append(
      'directory',
      this.state.currentPicker == 1 ? 'vehicle/' : 'user/documents/',
    );
    console.log('image uploading data is:-', photo);
    data.append('file', photo);
    fetch(U_BASE + U_FILE_UPLOAD, {
      body: data,
      method: 'POST',
      headers: {
        Accept: 'application/json',
        Authorization: 'Bearer ' + this.state.token,
      },
    })
      .then((response) => response.json())
      .catch((error) => {
        console.log('error data is:-', error);
        this.setState({isLoading: false});
      })
      .then((responseData) => {
        console.log('response data is:-', responseData);
        if (responseData !== undefined) {
          if (this.state.currentImageField == 'field') {
            this.setState(
              {
                isLoading: false,
                inspectionData: {
                  ...this.state.inspectionData,
                  files: [
                    ...this.state.inspectionData.files,
                    responseData.response.imageUrl,
                  ],
                },
              },
              (val) => {
                console.log('inspection data is:-', this.state.inspectionData);
                Toast.show('Successfully uploaded image');
              },
            );
          } else {
            this.setState(
              {
                isLoading: false,
                inspectionData: {
                  ...this.state.inspectionData,
                  [this.state.currentImageField]:
                    responseData.response.imageUrl,
                },
              },
              (val) => {
                console.log('inspection data is:-', this.state.inspectionData);
                Toast.show('Successfully uploaded image');
              },
            );
          }
        } else {
          this.setState({isLoading: false});
          Toast.show('Image uploading failed');
        }
      })
      .done();
  }

  handleNavigation = () => {
    let that = this;
    let data = that.state.inspectionData;
    console.log('data', data);
    if (data.files.length == 0) {
      Toast.show('Select Car Image');
    } else if (data.make == '') {
      Toast.show('Select Brand');
    } else if (data.model == '') {
      Toast.show('Select Model');
    } else if (data.variant == '') {
      Toast.show('Select Segment');
    } else if (data.bodyType == '') {
      Toast.show('Select Body Type');
    } else if (data.color == '') {
      Toast.show('Select Body Color');
    } else if (data.manufacturingYear == '') {
      Toast.show('Select Manufacturing Year');
    } else if (data.transmission == '') {
      Toast.show('Select Transmission Type');
    } else if (data.manufacturingCountry == '') {
      Toast.show('Select Manufacturing Country');
    } else if (data.accidental == '') {
      Toast.show('Select Vehicle Accidental');
    } else if (data.registrationNumber == '') {
      Toast.show('Enter RTO Registration Number');
    } else if (data.rtoState == '') {
      Toast.show('Select RTO passing state');
    } else if (data.numberOfOwners == '') {
      Toast.show('Select Number of Owners');
    } else if (data.odometerReading == '') {
      Toast.show('Enter Odometer Reading');
    } else if (
      data.odometerReadingImage == '' ||
      data.odometerReadingImage == null
    ) {
      Toast.show('Pick Odometer Image');
    } else if (data.fuelType == '') {
      Toast.show('Select Fuel Type');
    } else if (data.rcAvailable == '') {
      Toast.show('Select RC Availability');
    } else if (data.fuelType == '') {
      Toast.show('Select Fuel Type');
    } else if (data.expectedPrice == '') {
      Toast.show('Enter Expected Price');
    } else {
      this.props.inspectionDetail(data, 2);
      console.log('inspection data is:-', data);
      this.props.navigation.navigate('CarInspection');
    }
  };

  handleSheet = (val) => {
    if (val == 1) {
      this.chooseImage(1);
    } else if (val == 2) {
      this.chooseImage(2);
    } else if (val == 3) {
      this.setState({
        actionModalVisible: false,
      });
    }
  };

  render() {
    let {inspectionData, isGalleryViewVisible, galleryData} = this.state;
    return (
      <View style={Styles.container}>
        <Indicator loading={this.state.isLoading} />
        <GalleryView
          data={galleryData}
          handleGalleryView={this.handleGallery}
          modalVisible={isGalleryViewVisible}
        />

        <View style={styles.navigation_bar}>
          <View style={{flexDirection: 'row'}}>
            <BackView props={this.props} />
            <Text style={[Styles.button_font]}>Vehicle Details</Text>
          </View>
        </View>
        <KeyboardAwareScrollView keyboardShouldPersistTaps={'always'}
                                 enableResetScrollToCoords={false} bounces={false} style={{flex: 1, flexGrow: 1}}>
          <SafeAreaView
            style={[
              Styles.container,
              {backgroundColor: COLOR.BLUE_BG, padding: 15, margin: 0},
            ]}>
            <View
              style={[
                Styles.shadow_view,
                {
                  height: hp(30),
                  marginTop: 10,
                  backgroundColor: COLOR.BLUE_SHADOW,
                },
              ]}>
              {inspectionData && inspectionData.files.length > 0 ? (
                <View
                  style={{
                    flex: 1,
                    flexDirection: 'column',
                    backgroundColor: COLOR.WHITE,
                  }}>
                  <Pages
                    indicatorColor={COLOR.LIGHT_BLUE}
                    indicatorOpacity={0.2}>
                    {inspectionData.files.map((element) => {
                      return (
                        <View style={{flex: 1}}>
                          <TouchableOpacity
                            onPress={() => {
                              this.handleGallery(1);
                            }}>
                            <FastImage
                              resizeMode={FastImage.resizeMode.cover}
                              style={{
                                overflow: 'hidden',
                                width: wp(90),
                                alignSelf: 'center',
                                height: hp(30),
                              }}
                              source={{
                                uri: element,
                                priority: FastImage.priority.high,
                              }}
                            />
                          </TouchableOpacity>
                        </View>
                      );
                    })}
                  </Pages>
                  <TouchableOpacity
                    style={{
                      backgroundColor: COLOR.WHITE,
                      width: wp(90),
                      alignSelf: 'center',
                    }}
                    onPress={() => {
                      this.setState({
                        currentPicker: 1,
                        actionModalVisible: true,
                        currentImageField: 'field',
                      });
                    }}>
                    <View
                      style={{
                        height: 50,
                        width: 50,
                        backgroundColor: COLOR.WHITE,
                        borderRadius: 5,
                        alignItems: 'center',
                        justifyContent: 'center',
                        //top: -hp(29),
                        // right: 25,
                        marginRight: 10,
                        marginBottom: 0,
                        alignSelf: 'flex-end',
                        //position: 'absolute',
                      }}>
                      <Image
                        resizeMode={'stretch'}
                        style={{
                          overflow: 'hidden',
                          width: 30,
                          alignSelf: 'center',
                          height: 30,
                          tintColor: COLOR.BLACK,
                        }}
                        source={IMAGES.add_image}
                      />
                    </View>
                  </TouchableOpacity>
                </View>
              ) : (
                <TouchableOpacity
                  onPress={() => {
                    this.setState({
                      currentPicker: 1,
                      actionModalVisible: true,
                      currentImageField: 'field',
                    });
                  }}>
                  <View
                    style={{
                      height: hp(30),
                      width: wp(90),
                      alignItems: 'center',
                      justifyContent: 'center',
                      backgroundColor: COLOR.WHITE,
                      alignSelf: 'center',
                    }}>
                    <Image
                      resizeMode={'stretch'}
                      style={{
                        overflow: 'hidden',
                        width: 60,
                        alignSelf: 'center',
                        height: 60,
                        tintColor: COLOR.LIGHT_TEXT,
                      }}
                      source={IMAGES.camera}
                    />
                    <Text style={{color: COLOR.LIGHT_TEXT}}>Car Image</Text>
                  </View>
                </TouchableOpacity>
              )}
            </View>
            <View
              style={[
                Styles.shadow_view,
                {
                  marginVertical: 20,
                  backgroundColor: COLOR.BLUE_BG,
                  width: wp(90),
                  alignSelf: 'center',
                  zIndex: 100,
                },
              ]}>
              <this.dropdown
                name={
                  this.state.inspectionData
                    ? this.state.inspectionData.selectedMake
                      ? this.state.inspectionData.selectedMake.value
                      : null
                    : null
                }
                placeholder={'Make'}
                zIndex={3996}
                data={this.state.makeData}
              />
              <View style={Styles.line_view} />
              <this.dropdown
                name={
                  this.state.inspectionData
                    ? this.state.inspectionData.selectedModel
                      ? this.state.inspectionData.selectedModel.value
                      : null
                    : null
                }
                placeholder={'Model'}
                zIndex={3995}
                data={this.state.modalData}
              />
              <View style={Styles.line_view} />
              <this.dropdown
                name={
                  this.state.inspectionData
                    ? this.state.inspectionData.selectedVarient
                      ? this.state.inspectionData.selectedVarient.value
                      : null
                    : null
                }
                placeholder={'Select Segment'}
                zIndex={3994}
                data={this.state.variantData}
              />
              <View style={Styles.line_view} />
              <this.dropdown
                name={inspectionData ? inspectionData.bodyType : null}
                placeholder={'Select Body Type'}
                zIndex={3993}
                data={this.state.bodyTypeData}
              />
              <View style={Styles.line_view} />
              <this.dropdown
                name={inspectionData ? inspectionData.color : null}
                placeholder={'Select Color'}
                zIndex={3992}
                data={this.state.colorData}
              />
              <View style={Styles.line_view} />
              <this.dropdown
                name={inspectionData ? inspectionData.manufacturingYear : null}
                placeholder={'Manufacturing Year'}
                zIndex={3991}
                data={this.state.manufacturingYear}
              />
              <View style={Styles.line_view} />
              <this.dropdown
                name={inspectionData ? inspectionData.transmission : null}
                placeholder={'Transmision Type'}
                zIndex={3990}
                data={TRANSMISSION_TYPE}
              />
              <View style={Styles.line_view} />
              <this.dropdown
                name={
                  inspectionData ? inspectionData.manufacturingCountry : null
                }
                placeholder={'Manufacturing Country'}
                zIndex={3980}
                data={this.state.countryData}
              />
              <View style={Styles.line_view} />
              <this.dropdown
                name={
                  inspectionData
                    ? inspectionData.accidental
                      ? inspectionData.accidental
                      : null
                    : null
                }
                placeholder={'Accidental'}
                zIndex={3970}
                data={this.state.accidental}
              />
              <View style={Styles.line_view} />
              {/* <TouchableOpacity
                onPress={() => this.showDatePicker('manufacturingDate')}>
                <Text
                  style={{
                    paddingTop: 10,
                    marginBottom: 0,
                    fontSize: 10,
                    paddingLeft: 15,

                    color: COLOR.GRAY,
                    backgroundColor: COLOR.WHITE,
                  }}>
                  Manufacturring Date
                </Text>
                <View
                  style={{
                    paddingHorizontal: 10,
                    backgroundColor: COLOR.WHITE,
                    flexDirection: 'row',
                    height: 50,
                    alignItems: 'center',
                    justifyContent: 'space-between',
                  }}>
                  <TextInput
                    value={
                      inspectionData ? inspectionData.manufacturingDate : ''
                    }
                    style={[Styles.body_label]}
                  />
                  <Image
                    style={{height: 25, width: 25}}
                    source={IMAGES.calendar}
                    resizeMode={'contain'}
                  />
                </View>
              </TouchableOpacity>
              <View style={Styles.line_view} /> */}
              <TouchableOpacity
                onPress={() => this.showDatePicker('registrationDate')}>
                <Text
                  style={{
                    paddingTop: 10,
                    marginBottom: 0,
                    fontSize: 10,
                    paddingLeft: 15,
                    color: COLOR.GRAY,
                    backgroundColor: COLOR.WHITE,
                  }}>
                  Registration Date
                </Text>
                <View
                  style={{
                    paddingHorizontal: 10,
                    backgroundColor: COLOR.WHITE,
                    flexDirection: 'row',
                    height: 50,
                    alignItems: 'center',
                    justifyContent: 'space-between',
                  }}>
                  <TextInput
                    value={
                      inspectionData ? inspectionData.registrationDate : ''
                    }
                    style={[Styles.body_labelp]}
                  />
                  <Image
                    style={{height: 25, width: 25}}
                    source={IMAGES.calendar}
                    resizeMode={'contain'}
                  />
                </View>
              </TouchableOpacity>
              <View style={Styles.line_view} />
              {/* <View>
                <Text
                  style={{
                    paddingTop: 10,
                    marginBottom: 0,
                    fontSize: 10,
                    paddingLeft: 15,
                    color: COLOR.GRAY,
                    backgroundColor: COLOR.WHITE,
                  }}>
                  Registration Number
                </Text>
                <View
                  style={{
                    paddingHorizontal: 15,
                    backgroundColor: COLOR.WHITE,
                    flexDirection: 'row',
                    height: 50,
                    alignItems: 'center',
                    justifyContent: 'space-between',
                  }}>
                  <TextInput
                    keyboardType={'default'}
                    autoCapitalize={'characters'}
                    value={
                      inspectionData ? inspectionData.registrationNumber : ''
                    }
                    onChangeText={(text) => {
                      this.updateMasterState(text, 'registrationNumber');
                    }}
                    style={[Styles.body_label, {flex: 1}]}
                  />
                </View>
              </View>
              <View style={Styles.line_view} /> */}
              <this.dropdown
                name={inspectionData ? inspectionData.rtoState : null}
                placeholder={'State'}
                zIndex={3951}
                data={REGISTRATION_STATE}
              />
              <View style={Styles.line_view} />
              <View>
                <Text
                  style={{
                    paddingTop: 10,
                    marginBottom: 0,
                    fontSize: 10,
                    paddingLeft: 15,
                    color: COLOR.GRAY,
                    backgroundColor: COLOR.WHITE,
                  }}>
                  RTO Registration Number
                </Text>
                <View
                  style={{
                    paddingHorizontal: 15,
                    backgroundColor: COLOR.WHITE,
                    flexDirection: 'row',
                    height: 50,
                    alignItems: 'center',
                    justifyContent: 'space-between',
                  }}>
                  <TextInput
                    keyboardType={'default'}
                    autoCapitalize={'characters'}
                    value={inspectionData ? inspectionData.state : ''}
                    onChangeText={(text) => {
                      this.updateMasterState(text, 'state');
                    }}
                    style={[Styles.body_label, {flex: 1}]}
                  />
                </View>
              </View>
              <View style={Styles.line_view} />
              <this.dropdown
                name={inspectionData ? inspectionData.numberOfOwners : null}
                placeholder={'Number of Owners'}
                zIndex={3950}
                data={this.state.ownerData}
              />
              <View style={Styles.line_view} />
              <View>
                <Text
                  style={{
                    paddingTop: 10,
                    marginBottom: 0,
                    fontSize: 10,
                    paddingLeft: 15,
                    color: COLOR.GRAY,
                    backgroundColor: COLOR.WHITE,
                  }}>
                  Odometer Reading
                </Text>
                <View
                  style={{
                    paddingLeft: 15,
                    paddingRight: 10,
                    backgroundColor: COLOR.WHITE,
                    flexDirection: 'row',
                    height: 50,
                    alignItems: 'center',
                    justifyContent: 'space-between',
                  }}>
                  <TextInput
                    value={inspectionData ? inspectionData.odometerReading : ''}
                    onChangeText={(text) => {
                      this.updateMasterState(text, 'odometerReading');
                    }}
                    keyboardType={'number-pad'}
                    style={[Styles.body_label, {flex: 1}]}
                  />
                  <TouchableOpacity style={Styles.back_arrow}>
                    <Text
                      style={[
                        Styles.small_label,
                        {textAlign: 'right', color: COLOR.LIGHT_TEXT},
                      ]}>
                      In Kms
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={Styles.shadow_view}
                    onPress={() => {
                      this.setState({
                        currentPicker: 1,
                        actionModalVisible: true,
                        currentImageField: 'odometerReadingImage',
                      });
                    }}
                    style={Styles.back_arrow}>
                    <Image
                      source={
                        inspectionData.odometerReadingImage
                          ? IMAGES.tick_green
                          : IMAGES.camera
                      }
                      style={{
                        height: 30,
                        width: 30,
                      }}
                      resizeMode={'contain'}
                    />
                  </TouchableOpacity>
                  {inspectionData.odometerReadingImage && (
                    <TouchableOpacity
                      style={Styles.shadow_view}
                      onPress={() => {
                        this.handleGallery(
                          2,
                          Array(inspectionData.odometerReadingImage),
                        );
                      }}
                      style={Styles.back_arrow}>
                      <Text style={[Styles.small_label, {color: COLOR.GREEN}]}>
                        View
                      </Text>
                    </TouchableOpacity>
                  )}
                </View>
              </View>
              <View style={Styles.line_view} />
              <this.dropdown
                name={inspectionData ? inspectionData.fuelType : null}
                placeholder={'Fuel Type'}
                zIndex={3940}
                data={this.state.fuelTypeData}
              />
              <View style={Styles.line_view} />
              {(this.state.inspectionData.fuelType == 'CNG' ||
                this.state.inspectionData.fuelType == 'LPG') && (
                <View>
                  <this.dropdown
                    name={inspectionData ? inspectionData.gasFitment : null}
                    placeholder={'Gas Fitment'}
                    zIndex={3930}
                    data={this.state.gasFitmentData}
                  />
                  <View style={Styles.line_view} />
                  <this.dropdown
                    name={
                      inspectionData ? inspectionData.certificatePlate : null
                    }
                    placeholder={'CNG certificate Plate'}
                    zIndex={3929}
                    data={this.state.cngCertifiedPlate}
                  />
                  <View style={Styles.line_view} />
                  <this.dropdown
                    name={
                      inspectionData
                        ? inspectionData.FitmentEndorsementRC
                        : null
                    }
                    placeholder={'CNG/LPG Fitment Endorsement RC'}
                    zIndex={3928}
                    data={this.state.fitmentEndorsementData}
                  />
                  <View style={Styles.line_view} />
                  <this.dropdown
                    name={inspectionData ? inspectionData.gasRemoval : null}
                    placeholder={'CNG/LPG Removal'}
                    zIndex={3919}
                    data={this.state.cngRemovalData}
                  />
                  <View style={Styles.line_view} />
                  <View>
                    <Text
                      style={{
                        paddingTop: 10,
                        marginBottom: 0,
                        fontSize: 10,
                        paddingLeft: 15,
                        color: COLOR.GRAY,
                        backgroundColor: COLOR.WHITE,
                      }}>
                      CNG Registration Username
                    </Text>
                    <View
                      style={{
                        paddingHorizontal: 15,
                        backgroundColor: COLOR.WHITE,
                        flexDirection: 'row',
                        height: 50,
                        alignItems: 'center',
                        justifyContent: 'space-between',
                      }}>
                      <TextInput
                        keyboardType={'default'}
                        value={
                          inspectionData ? inspectionData.fitmentUserName : ''
                        }
                        onChangeText={(text) => {
                          this.updateMasterState(text, 'fitmentUserName');
                        }}
                        style={[Styles.body_label, {flex: 1}]}
                      />
                    </View>
                  </View>
                  <View style={Styles.line_view} />
                  <TouchableOpacity
                    onPress={() => this.showDatePicker('fitmentValidity')}>
                    <Text
                      style={{
                        paddingTop: 10,
                        marginBottom: 0,
                        fontSize: 10,
                        paddingLeft: 15,
                        color: COLOR.GRAY,
                        backgroundColor: COLOR.WHITE,
                      }}>
                      Fitment Validity
                    </Text>
                    <View
                      style={{
                        paddingHorizontal: 15,
                        backgroundColor: COLOR.WHITE,
                        flexDirection: 'row',
                        height: 50,
                        alignItems: 'center',
                        justifyContent: 'space-between',
                      }}>
                      <TextInput
                        value={
                          inspectionData ? inspectionData.fitmentValidity : ''
                        }
                        style={[Styles.body_label]}
                      />
                      <Image
                        style={{height: 25, width: 25}}
                        source={IMAGES.calendar}
                        resizeMode={'contain'}
                      />
                    </View>
                  </TouchableOpacity>
                  <View style={Styles.line_view} />
                </View>
              )}

              <View>
                <Text
                  style={{
                    paddingTop: 10,
                    marginBottom: 0,
                    fontSize: 10,
                    paddingLeft: 15,
                    color: COLOR.GRAY,
                    backgroundColor: COLOR.WHITE,
                  }}>
                  Engine Number
                </Text>

                <View
                  style={{
                    paddingHorizontal: 15,
                    backgroundColor: COLOR.WHITE,
                    flexDirection: 'row',
                    height: 50,
                    alignItems: 'center',
                    justifyContent: 'space-between',
                  }}>
                  <TextInput
                    keyboardType={'default'}
                    autoCapitalize={'characters'}
                    value={inspectionData ? inspectionData.engineNumber : ''}
                    onChangeText={(text) => {
                      this.updateMasterState(text, 'engineNumber');
                    }}
                    style={[Styles.body_label, {flex: 1}]}
                  />
                </View>
              </View>
              <View style={Styles.line_view} />
              <View>
                <Text
                  style={{
                    paddingTop: 10,
                    marginBottom: 0,
                    fontSize: 10,
                    paddingLeft: 15,
                    color: COLOR.GRAY,
                    backgroundColor: COLOR.WHITE,
                  }}>
                  Chassis Number
                </Text>

                <View
                  style={{
                    paddingHorizontal: 15,
                    backgroundColor: COLOR.WHITE,
                    flexDirection: 'row',
                    height: 50,
                    alignItems: 'center',
                    justifyContent: 'space-between',
                  }}>
                  <TextInput
                    keyboardType={'default'}
                    autoCapitalize={'characters'}
                    value={inspectionData ? inspectionData.chassisNumber : ''}
                    onChangeText={(text) => {
                      this.updateMasterState(text, 'chassisNumber');
                    }}
                    style={[Styles.body_label, {flex: 1}]}
                  />
                </View>
              </View>
              <View style={Styles.line_view} />
              <this.dropdown
                name={inspectionData ? inspectionData.chassisEmbossing : null}
                placeholder={'Chassis No. Embossing'}
                zIndex={3927}
                data={this.state.embossingData}
              />
              <View style={Styles.line_view} />
              <this.dropdown
                name={inspectionData ? inspectionData.rcAvailable : null}
                placeholder={'RC Available'}
                zIndex={3926}
                data={this.state.rcAvailableData}
              />
              <View style={Styles.line_view} />
              {this.state.inspectionData.rcAvailable == 'true' && (
                <View>
                  <Text
                    style={{
                      paddingTop: 10,
                      marginBottom: 0,
                      fontSize: 10,
                      paddingLeft: 15,
                      color: COLOR.GRAY,
                      backgroundColor: COLOR.WHITE,
                    }}>
                    Loan Number
                  </Text>
                  <View
                    style={{
                      paddingHorizontal: 15,
                      backgroundColor: COLOR.WHITE,
                      flexDirection: 'row',
                      height: 50,
                      alignItems: 'center',
                      justifyContent: 'space-between',
                    }}>
                    <TextInput
                      keyboardType={'default'}
                      value={inspectionData ? inspectionData.loanNumber : ''}
                      onChangeText={(text) => {
                        this.updateMasterState(text, 'loanNumber');
                      }}
                      style={[Styles.body_label, {flex: 1}]}
                    />
                  </View>
                  <View style={Styles.line_view} />
                </View>
              )}
              <this.dropdown
                name={inspectionData ? inspectionData.pollutionCard : null}
                placeholder={'Pollution Card'}
                zIndex={3925}
                data={this.state.pollutionData}
              />
              <View style={Styles.line_view} />
              <this.dropdown
                name={inspectionData ? inspectionData.nocIssued : null}
                placeholder={'RTO Noc Issued'}
                zIndex={3924}
                data={this.state.nocIssuedData}
              />
              <View style={Styles.line_view} />
              <this.dropdown
                name={inspectionData ? inspectionData.rcMismatch : null}
                placeholder={'RC Mismatch'}
                zIndex={3923}
                data={this.state.rcMismatchData}
              />
              <View style={Styles.line_view} />

              <View style={Styles.line_view} />
              <this.dropdown
                name={inspectionData ? inspectionData.noOfKeys : null}
                placeholder={'No. of Keys'}
                zIndex={3921}
                data={this.state.keysData}
              />
              <View style={Styles.line_view} />
              <this.dropdown
                name={inspectionData ? inspectionData.loanNoRemoval : null}
                placeholder={'Loan No. Removal'}
                zIndex={3920}
                data={this.state.loanRemovalData}
              />
              <View style={Styles.line_view} />
              <this.dropdown
                name={inspectionData ? inspectionData.typeOfInsurance : null}
                placeholder={'Type of Insurance'}
                zIndex={3919}
                data={this.state.insuranceTypeData}
              />
              <View style={Styles.line_view} />
              <TouchableOpacity
                onPress={() => {
                  this.setState({
                    currentPicker: 1,
                    actionModalVisible: true,
                    currentImageField: 'insuranceTypeImage',
                  });
                }}>
                <Text
                  style={{
                    marginTop: 0,
                    marginBottom: 0,
                    fontSize: 10,
                    paddingLeft: 15,
                    color: COLOR.GRAY,
                    height: 40,
                    backgroundColor: COLOR.WHITE,
                    paddingTop: 10,
                  }}>
                  Insurance Image
                </Text>
                <View
                  style={[
                    Styles.shadow_view,
                    {
                      height: 250,
                      alignSelf: 'center',
                      width: wp(90),
                      backgroundColor: COLOR.WHITE,
                      alignItems: 'center',
                      justifyContent: 'center',
                    },
                  ]}>
                  <Image
                    style={
                      inspectionData
                        ? inspectionData.insuranceTypeImage != ''
                          ? {height: 250, width: wp(90)}
                          : {height: 80}
                        : {height: 80}
                    }
                    source={
                      inspectionData
                        ? inspectionData.insuranceTypeImage != ''
                          ? {
                              uri: inspectionData.insuranceTypeImage,
                            }
                          : IMAGES.add_image
                        : IMAGES.add_image
                    }
                    resizeMode={
                      inspectionData
                        ? inspectionData.insuranceTypeImage != ''
                          ? 'cover'
                          : 'contain'
                        : 'contain'
                    }
                  />
                </View>
              </TouchableOpacity>
              <View style={Styles.line_view} />
              <TouchableOpacity
                onPress={() => this.showDatePicker('insuranceValidityDate')}>
                <Text
                  style={{
                    paddingTop: 10,
                    marginBottom: 0,
                    fontSize: 10,
                    paddingLeft: 15,
                    color: COLOR.GRAY,
                    backgroundColor: COLOR.WHITE,
                  }}>
                  Insurance Validity Date
                </Text>
                <View
                  style={{
                    paddingHorizontal: 10,
                    backgroundColor: COLOR.WHITE,
                    flexDirection: 'row',
                    height: 50,
                    alignItems: 'center',
                    justifyContent: 'space-between',
                  }}>
                  <TextInput
                    value={
                      inspectionData ? inspectionData.insuranceValidityDate : ''
                    }
                    style={[Styles.body_label]}
                  />
                  <Image
                    style={{height: 25, width: 25}}
                    source={IMAGES.calendar}
                    resizeMode={'contain'}
                  />
                </View>
              </TouchableOpacity>
              <View style={Styles.line_view} />
              <this.dropdown
                name={inspectionData ? inspectionData.underHypothecation : null}
                placeholder={'Under Hypothecation'}
                zIndex={3918}
                data={this.state.hypothycationData}
              />
              {this.state.inspectionData.underHypothecation == 'true' && (
                <View style={{zIndex: 3916}}>
                  {/* <View style={Styles.line_view} />
                  <this.dropdown
                    name={
                      inspectionData
                        ? inspectionData.isFinancer
                          ? inspectionData.isFinancer
                          : null
                        : null
                    }
                    placeholder={'Is Finaced'}
                    zIndex={3917}
                    data={this.state.financerData}
                  /> */}
                  <View style={Styles.line_view} />
                  <this.dropdown
                    name={
                      inspectionData ? inspectionData.bankerNocIssued : null
                    }
                    placeholder={'Banker Financer NOC Issued'}
                    zIndex={3916}
                    data={this.state.bankerNocIssuedData}
                  />
                  <View style={Styles.line_view} />
                  {this.state.inspectionData.isFinancer == 'true' && (
                    <View>
                      <Text
                        style={{
                          paddingTop: 10,
                          marginBottom: 0,
                          fontSize: 10,
                          paddingLeft: 15,
                          color: COLOR.GRAY,
                          backgroundColor: COLOR.WHITE,
                        }}>
                        Financer Name
                      </Text>
                      <View
                        style={{
                          paddingHorizontal: 15,
                          backgroundColor: COLOR.WHITE,
                          flexDirection: 'row',
                          height: 50,
                          alignItems: 'center',
                          justifyContent: 'space-between',
                        }}>
                        <TextInput
                          keyboardType={'default'}
                          value={inspectionData ? inspectionData.financer : ''}
                          onChangeText={(text) => {
                            this.updateMasterState(text, 'financer');
                          }}
                          style={[Styles.body_label, {flex: 1}]}
                        />
                      </View>
                    </View>
                  )}
                </View>
              )}
              <View style={Styles.line_view} />
              <View>
                <Text
                  style={{
                    paddingTop: 10,
                    marginBottom: 0,
                    fontSize: 10,
                    paddingLeft: 15,
                    color: COLOR.GRAY,
                    backgroundColor: COLOR.WHITE,
                  }}>
                  Car Summary
                </Text>
                <View
                  style={{
                    paddingHorizontal: 15,
                    backgroundColor: COLOR.WHITE,
                    flexDirection: 'row',
                    alignItems: 'center',
                    height: 50,
                    justifyContent: 'space-between',
                  }}>
                  <TextInput
                    keyboardType={'default'}
                    multiline={true}
                    value={inspectionData ? inspectionData.remark : ''}
                    onChangeText={(text) => {
                      this.updateMasterState(text, 'remark');
                    }}
                    style={[Styles.body_label, {flex: 1}]}
                  />
                  {/* <TouchableOpacity style={Styles.back_arrow}>
                  <Image
                    style={{height: 15, width: 15}}
                    source={IMAGES.down_arrow}
                    resizeMode={'contain'}
                  />
                </TouchableOpacity> */}
                </View>
              </View>
              <View style={Styles.line_view} />
              <View>
                <Text
                  style={{
                    paddingTop: 10,
                    marginBottom: 0,
                    fontSize: 10,
                    paddingLeft: 15,
                    color: COLOR.GRAY,
                    backgroundColor: COLOR.WHITE,
                  }}>
                  Expected Price
                </Text>

                <View
                  style={{
                    paddingHorizontal: 15,
                    backgroundColor: COLOR.WHITE,
                    flexDirection: 'row',
                    height: 50,
                    alignItems: 'center',
                    justifyContent: 'space-between',
                  }}>
                  <TextInput
                    keyboardType={'number-pad'}
                    value={inspectionData ? inspectionData.expectedPrice : ''}
                    onChangeText={(text) => {
                      this.updateMasterState(text, 'expectedPrice');
                    }}
                    style={[Styles.body_label, {flex: 1}]}
                  />
                </View>
              </View>
              <View style={Styles.line_view} />
              <View>
                <Text
                  style={{
                    paddingTop: 10,
                    marginBottom: 0,
                    fontSize: 10,
                    paddingLeft: 15,
                    color: COLOR.GRAY,
                    backgroundColor: COLOR.WHITE,
                  }}>
                  Location
                </Text>

                <View
                  style={{
                    paddingHorizontal: 15,
                    backgroundColor: COLOR.WHITE,
                    flexDirection: 'row',
                    height: 50,
                    alignItems: 'center',
                    justifyContent: 'space-between',
                  }}>
                  <TextInput
                    keyboardType={'default'}
                    value={inspectionData ? inspectionData.pickupAddress : ''}
                    onChangeText={(text) => {
                      this.updateMasterState(text, 'pickupAddress');
                    }}
                    style={[Styles.body_label, {flex: 1}]}
                  />
                </View>
              </View>
              <View style={Styles.line_view} />
            </View>
            <TouchableOpacity
              onPress={() => {
                this.setState({
                  currentPicker: 1,
                  actionModalVisible: true,
                  currentImageField: 'registrationImage',
                });
              }}>
              <View
                style={[
                  Styles.shadow_view,
                  {
                    height: 250,
                    alignSelf: 'center',
                    width: wp(90),
                    backgroundColor: COLOR.WHITE,
                    alignItems: 'center',
                    justifyContent: 'center',
                  },
                ]}>
                <Image
                  style={
                    inspectionData
                      ? inspectionData.registrationImage
                        ? {height: 250, width: wp(90)}
                        : {height: 80}
                      : {height: 80}
                  }
                  source={
                    inspectionData
                      ? inspectionData.registrationImage
                        ? {
                            uri: inspectionData.registrationImage,
                          }
                        : IMAGES.registration
                      : IMAGES.registration
                  }
                  resizeMode={
                    inspectionData
                      ? inspectionData.registrationImage
                        ? 'cover'
                        : 'contain'
                      : 'contain'
                  }
                />
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.handleNavigation()}
              style={[
                Styles.button_content,
                {
                  marginTop: 20,
                  marginBottom: 0,
                  width: wp(100),
                  borderRadius: 0,
                },
              ]}>
              <Text style={Styles.button_font}>Continue</Text>
            </TouchableOpacity>
            <ActionSheet
              modalVisible={this.state.actionModalVisible}
              handleSheet={this.handleSheet}
            />
          </SafeAreaView>
        </KeyboardAwareScrollView>

        <DateTimePickerModal
          isVisible={this.state.pickerMode !== null}
          mode={this.state.pickerMode}
          headerTextIOS={this.state.pickerHeading}
          onConfirm={this.handleConfirm}
          onCancel={this.handleCancel}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  navigation_bar: {
    height: getStatusBarHeight(true) + 45,
    backgroundColor: COLOR.DARK_BLUE,
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: getStatusBarHeight(true),
    justifyContent: 'space-between',
  },
});

const mapStateToProps = (state, ownProps) => {
  return {
    vehicleInspectionData: state.reducer.inspectionDetail,
    makeData: state.reducer.makeData,
    variantData: state.reducer.varientData,
    modalData: state.reducer.modelData,
    colorData: state.reducer.colorData,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getMakeData: () => {
      dispatch(actions.getMakeData());
    },
    getVarientData: (val) => {
      dispatch(actions.getVarientData(val));
    },
    getColorData: (val) => {
      dispatch(actions.getColorData(val));
    },
    getModalData: (val) => {
      dispatch(actions.getModalData(val));
    },
    inspectionDetail: (data, type) => {
      dispatch(actions.inspectionDetail(data, type));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(VehicleDetail);
