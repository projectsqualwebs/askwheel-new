import React from 'react';
import {
  View,
  Image,
  Text,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import IMAGES from '../../res/styles/Images';
import Styles from '../../res/styles/Styles';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import COLOR from '../../res/styles/Color';
import {ScrollView, TextInput} from 'react-native-gesture-handler';
import Accordion from 'react-native-collapsible/Accordion';
import {connect} from 'react-redux';
import Actions from '../../redux/actions/Actions';
import ImagePicker from 'react-native-image-crop-picker';
import {U_BASE, U_FILE_UPLOAD, U_IMAGE_BASE} from '../../res/Constants';
import Toast from 'react-native-simple-toast';
import {Pages} from 'react-native-pages';
import ActionSheet from '../../commonView/ActionSheet';
import Indicator from '../../commonView/ActivityIndicator';
import FastImage from 'react-native-fast-image';
import GalleryView from '../../commonView/GalleryView';

class Exterior extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedSectionId: null,
      actionModalVisible: false,
      inspectionIndex: null,
      isGalleryViewVisible: false,
      isLoading: false,
      currentPicker: null,
      activeSections: [],
      collapsed: true,
      multipleSelect: false,
      parameter: null,
      headingLabel: '',
      index: null,
      galleyData: [],
    };

    this.props.getInspectionDetail(null, 1);
    this.toggleExpanded = this.toggleExpanded.bind(this);
    this.handleSectionSelection = this.handleSectionSelection.bind(this);
    this.renderContent = this.renderContent.bind(this);
    this.renderHeader = this.renderHeader.bind(this);
    this.handleIssueCount = this.handleIssueCount.bind(this);
    this.saveCurrentDetails = this.saveCurrentDetails.bind(this);
    this.handleSectionShowhide = this.handleSectionShowhide.bind(this);
    this.handleSubcategoryOptionSelected = this.handleSubcategoryOptionSelected.bind(
      this,
    );
    this.handleGallery = this.handleGallery.bind(this);
    this.handleGalleryIndex = this.handleGalleryIndex.bind(this);
  }

  componentDidMount() {
    if (this.props.route.params.inspectionIndex != undefined) {
      let that = this;
      that.setState(
        {
          inspectionIndex: that.props.route.params.inspectionIndex,
        },
        (val) => {
          console.log('gallery data is', that.props.inspectionData)

          that.setState({
            parameter:
              that.props.inspectionData.bodyFrame[that.state.inspectionIndex],
            headingLabel: this.props.inspectionData.bodyFrame[
              this.state.inspectionIndex
            ].name,
          });
        },
      );
    }
  }



  handleGallery = (val, data) => {
    let that = this;
    let array = [];
    if (data) {
      for (let i = 0; i < data.length; i++) {
        array.push(data[i].path);
      }
    }
    console.log('array is', array)

    that.setState({
      isGalleryViewVisible: !that.state.isGalleryViewVisible,
      galleyData: array,
    });
  };

  chooseImage = (val) => {
    let options = {
      title: 'Select Image',
      width: 1630,
      height: 750,
      compressImageMaxWidth: 1630,
      compressImageMaxHeight: 750,
      multiple: true,
    };
    if (val == 1) {
      ImagePicker.openCamera({
        title: 'Select Image',
        width: 1630,
        height: 750,
        compressImageMaxWidth: 1630,
        compressImageMaxHeight: 750,
      }).then((response) => {
        if (response.didCancel) {
          console.log('User cancelled image picker');
        } else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        } else if (response.customButton) {
          console.log('User tapped custom button: ', response.customButton);
          alert(response.customButton);
        } else {
          this.setState({
            currentPicker: val,
            actionModalVisible: false,
            isLoading: true,
          });
          this.callUploadImage(response, val);
        }
      });
    } else if (val == 2) {
      ImagePicker.openPicker(options).then((response) => {
        if (response.didCancel) {
          console.log('User cancelled image picker');
        } else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        } else if (response.customButton) {
          console.log('User tapped custom button: ', response.customButton);
          alert(response.customButton);
        } else {
          this.setState({
            currentPicker: val,
            actionModalVisible: false,
            isLoading: true,
          });
          for (let i = 0; i < response.length; i++) {
            this.callUploadImage(response[i], val);
          }
        }
      });
    }
  };

  callUploadImage(res, val) {
    var data = new FormData();
    var sectionId = this.state.selectedSectionId;
    var photo = {
      uri: res.path,
      type: res.type ? res.type : res.mime,
      name: 'image.jpg',
    };
    data.append('directory', 'vehicle/');
    data.append('file', photo);
    fetch(U_BASE + U_FILE_UPLOAD, {
      body: data,
      method: 'POST',
      headers: {
        Accept: 'application/json',
        Authorization: 'Bearer ' + this.state.token,
      },
    })
      .then((response) => response.json())
      .catch((error) => {
        console.log('error data is:-', error);
        this.setState({isLoading: false});
      })
      .then((responseData) => {
        if (responseData !== undefined) {
          let currentData = this.state.parameter;
          if (val == 1) {
            currentData.subcategory[sectionId].files.push({
              type: 'image',
              path: responseData.response.imageUrl,
            });
          } else if (val == 2) {
            currentData.subcategory[sectionId].files.push({
              type: 'image',
              path: responseData.response.imageUrl,
            });
          } else if (val == 3) {
            currentData.subcategory[sectionId].files.push({
              type: 'image',
              path: responseData.response.imageUrl,
            });
          }

          this.setState(
            {
              isLoading: false,
              parameter: currentData,
            },
            (val) => {
              Toast.show('Successfully uploaded image');
            },
          );
        } else {
          Toast.show('Profile image uploading failed');
          this.setState({isLoading: false});
        }
      })
      .done();
  }

  handleSectionSelection = (row, section) => {
    console.log('selected row section is:-', row, section);
  };

  toggleExpanded = () => {
    this.setState({collapsed: !this.state.collapsed});
  };

  saveCurrentDetails = () => {
    let data = this.state.parameter;
    data.issueListed = 0;
    for (let i = 0; i < data.subcategory.length; i++) {
      if (data.subcategory[i].issueCount > 0) {
        data.issueListed += 1;
      }
    }
    let percent = (data.issueListed / data.subcategory.length) * 100;
    data.rating = 100 - percent;
    let val = this.props.inspectionData;
    //[that.state.inspectionIndex]
    val.bodyFrame[this.state.inspectionIndex] = data;
    this.props.getInspectionDetail(val, 2);
    this.props.navigation.goBack(null);
  };

  handleIssueCount = (val, id, optionId) => {
    let data = val;
    data.subcategory[id].issueCount = 0;
    for (let i = 0; i < data.subcategory[id].options.length; i++) {
      if (data.subcategory[id].options[i].isSelected) {
        switch (data.subcategory[id].options[i].name) {
          case 'No':
          case 'Dirty':
          case 'Off':
          case 'Not Available':
          case 'Not Working':
          case 'Not Smooth':
          case 'Bad':
          case 'Scratched':
          case 'Repainted':
          case 'Dented':
          case 'Repaired':
          case 'Hard':
          case 'Unsatisfactory':
          case 'Spotted':
          case 'Cracked':
          case 'Changed':
          case 'Damaged':
          case 'Torn':
          case 'Non Operational':
          case 'Leak':
          case 'Not Ok':
            if (
              data.subcategory[id].name != 'Gear Box Leakage' &&
              data.subcategory[id].name != 'Engine Oil Leakage' &&
              data.subcategory[id].name != 'Engine Oil Back Pressure'
            ) {
              data.subcategory[id].issueCount += 1;
            }
            break;
        }

        if (
          data.subcategory[id].name == 'Gear Box Leakage' ||
          data.subcategory[id].name == 'Engine Oil Leakage' ||
          data.subcategory[id].name == 'Engine Oil Back Pressure'
        ) {
          if (data.subcategory[id].options[i].name == 'Yes') {
            data.subcategory[id].issueCount += 1;
          }
        }
      }
    }

    this.setState({
      parameter: data,
    });
  };

  handleSectionShowhide = (section) => {
    this.setState({
      actionModalVisible: true,
      selectedSectionId: section.id,
    });
  };

  handleSubcategoryOptionSelected = (section, data) => {
    let that = this;
    let val = that.state.parameter;

    if (val.subcategory[section.id].isMultiSelect == 1) {
      val.subcategory[section.id].options[data.id].isSelected = !val
        .subcategory[section.id].options[data.id].isSelected;
      let count = val.subcategory[section.id].options.find(
        (x) => x.isSelected == 1,
      );
      if (count) {
        val.subcategory[section.id].isSelected = 1;
      } else {
        val.subcategory[section.id].isSelected = 0;
      }
    } else if (val.subcategory[section.id].isMultiSelect == 0) {
      if (val.subcategory[section.id].isSelected == 1) {
        if (val.subcategory[section.id].options[data.id].isSelected == 1) {
          val.subcategory[section.id].options[data.id].isSelected = 0;
          let count = 0;
          for (let i = 0; i < val.subcategory[section.id].options.length; i++) {
            if (val.subcategory[section.id].options[data.id].isSelected == 1) {
              count += 1;
            }
          }
          if (count == 0) {
            val.subcategory[section.id].isSelected = 0;
          } else {
            val.subcategory[section.id].isSelected = 1;
          }
        } else {
          for (let i = 0; i < val.subcategory[section.id].options.length; i++) {
            val.subcategory[section.id].options[i].isSelected = 0;
          }
          val.subcategory[section.id].options[data.id].isSelected = 1;
        }
      } else {
        val.subcategory[section.id].isSelected = 1;
        if (val.subcategory[section.id].options[data.id].isSelected == 1) {
          val.subcategory[section.id].options[data.id].isSelected = 0;
        } else {
          val.subcategory[section.id].options[data.id].isSelected = 1;
        }
      }
    }
    this.handleIssueCount(val, section.id, data.id);
  };

  renderHeader = (section, _, active) => {
    return (
      <View
        style={[
          styles.image_collection,
          {
            backgroundColor: COLOR.WHITE,
            padding: 15,
            borderRadius: 10,
            borderTopLeftRadius: 10,
            borderTopRightRadius: 10,
            borderBottomLeftRadius: active ? 0 : 10,
            borderBottomRightRadius: active ? 0 : 10,
            flexDirection: 'column',
            marginBottom: active ? 0 : 15,
          },
        ]}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Image
              style={{
                width: 20,
                height: 20,
                marginRight: 10,
              }}
              source={
                section.isSelected == 1
                  ? IMAGES.radio_button
                  : IMAGES.circle_boundary
              }
              resizeMode={'contain'}
            />
            <Text style={Styles.medium_label}>{section.name}</Text>
          </View>

          <Image
            style={{width: 20, height: 20}}
            source={
              active
                ? IMAGES.minus_icon
                : section.issueCount > 0
                ? IMAGES.alert_icon
                : IMAGES.plus_icon
            }
            resizeMode={'contain'}
          />
        </View>
        {active && <View style={[Styles.line_view, {marginTop: 20}]} />}
      </View>
    );
  };

  renderContent(section) {
    return (
      <View
        style={{
          width: wp(90),
          alignItems: 'center',
          justifyContent: 'center',
          paddingHorizontal: 15,
        }}>
        <View
          style={{
            flexDirection: 'row',
            marginBottom: 20,
            borderRadius: 10,
            overflow: 'hidden',
            height: 160,
            width: wp(85),
          }}>
          {section.files.length > 0 ? (
            <View style={{flex: 1, flexDirection: 'column'}}>
              <Pages indicatorColor={COLOR.LIGHT_BLUE} indicatorOpacity={0.2}>
                {section.files.map((element) => {
                  return (
                    <View style={{flex: 1}}>
                      <TouchableOpacity
                          onPress={() => this.handleGallery(2, section.files)}
                      >
                        <FastImage
                          resizeMode={FastImage.resizeMode.cover}
                          style={{
                            overflow: 'hidden',
                            width: wp(85),
                            alignSelf: 'center',
                            height: 160,
                          }}
                          source={{
                            uri: element.path,
                            priority: FastImage.priority.high,
                          }}
                        />
                      </TouchableOpacity>
                    </View>
                  );
                })}
              </Pages>
              <TouchableOpacity
                onPress={() => this.handleSectionShowhide(section)}>
                <View
                  style={{
                    height: 40,
                    width: 40,
                    backgroundColor: COLOR.WHITE,
                    borderRadius: 5,
                    alignItems: 'center',
                    justifyContent: 'center',
                    //  top: -150,
                    //  right: 15,
                    alignSelf: 'flex-end',
                    //  position: 'absolute',
                  }}>
                  <Image
                    resizeMode={'stretch'}
                    style={{
                      overflow: 'hidden',
                      width: 20,
                      alignSelf: 'center',
                      height: 20,
                      tintColor: COLOR.BLACK,
                    }}
                    source={IMAGES.camera}
                  />
                </View>
              </TouchableOpacity>
            </View>
          ) : (
            <TouchableOpacity
              onPress={() => this.handleSectionShowhide(section)}>
              <View
                style={{
                  height: 160,
                  width: wp(85),
                  alignItems: 'center',
                  justifyContent: 'center',
                  alignSelf: 'center',
                  backgroundColor: '#f0f0f0',
                }}>
                <Image
                  resizeMode={'stretch'}
                  style={{
                    overflow: 'hidden',
                    width: 80,
                    alignSelf: 'center',
                    height: 80,
                  }}
                  source={IMAGES.add_image}
                />
              </View>
            </TouchableOpacity>
          )}
        </View>
        {section
          ? section.options.map((data) => {
              return (
                <View
                  style={{
                    width: wp(90),
                    alignSelf: 'center',
                    borderBottomLeftRadius: 10,
                    borderBottomRightRadius: 10,
                    paddingHorizontal: 15,
                  }}>
                  <TouchableOpacity
                    onPress={() =>
                      this.handleSubcategoryOptionSelected(section, data)
                    }>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        height: 30,
                      }}>
                      <Text style={[Styles.body_label]}>{data.name}</Text>
                      <Image
                        style={{
                          height: 12,
                          width: 12,
                        }}
                        source={
                          data.isSelected == 1
                            ? IMAGES.blue_dot
                            : IMAGES.circle_boundary
                        }
                        resizeMode={'contain'}
                      />
                    </View>
                  </TouchableOpacity>
                </View>
              );
            })
          : null}
        <View
          style={{
            marginVertical: 20,
            borderWidth: 0.5,
            borderColor: COLOR.GRAY,
            borderRadius: 10,
            height: 80,
            padding: 10,
            width: wp(85),
          }}>
          <TextInput
            style={{flex: 1, margin: 0}}
            multiline={true}
            onChangeText={(text) => {
              let val = this.state.parameter;
              val.subcategory[section.id].comment = text;
              this.setState({
                parameter: val,
              });
            }}
            value={section.comment}
            placeholder={'Add your comments'}
          />
        </View>
      </View>
    );
  }

  TickView = (val) => {
    return;
  };

  updateSection = (activeSections) => {
    this.setState({activeSections});
  };

  handleSheet = (val) => {
    if (val == 1) {
      this.chooseImage(1);
    } else if (val == 2) {
      this.chooseImage(2);
    } else if (val == 3) {
      this.setState({
        actionModalVisible: false,
      });
    }
  };

  handleGalleryIndex = () => {
    this.setState({
      index: null,
    });
  };

  render() {
    return (
      <View style={Styles.container}>
        <GalleryView
          data={this.state.galleyData}
          handleGalleryView={this.handleGallery}
          modalVisible={this.state.isGalleryViewVisible}
          handleGalleryIndex={this.handleGalleryIndex}
        />
        <Indicator loading={this.state.isLoading} />
        <View style={styles.navigation_bar}>
          <View style={{flexDirection: 'row'}}>
            <TouchableOpacity
              onPress={() => this.saveCurrentDetails()}
              style={Styles.back_arrow}>
              <Image
                style={{height: 25, width: 35}}
                resizeMode={'contain'}
                source={IMAGES.left_arrow_white}
              />
            </TouchableOpacity>

            <Text style={[Styles.button_font]}>{this.state.headingLabel}</Text>
          </View>
        </View>
        <SafeAreaView style={[Styles.container]}>
          <ScrollView
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
            style={[Styles.container, {backgroundColor: COLOR.BLUE_BG}]}>
            <View style={{marginVertical: 20}}>
              {this.state.parameter ? (
                <Accordion
                  style={{
                    backgroundColor: COLOR.WHITE,
                    width: wp(90),
                    alignSelf: 'center',
                  }}
                  activeSections={this.state.activeSections}
                  sections={
                    this.state.parameter ? this.state.parameter.subcategory : []
                  }
                  renderHeader={this.renderHeader}
                  renderContent={this.renderContent}
                  onChange={this.updateSection}
                  underlayColor={'#00000000'}
                />
              ) : null}
            </View>
          </ScrollView>
          <View
            style={{
              width: wp(100),
              height: 45,
              flexDirection: 'row',
              backgroundColor: COLOR.LIGHT_BLUE,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <ActivityIndicator loading={this.state.isLoading} />
            <TouchableOpacity
              style={{flex: 1}}
              onPress={() => this.saveCurrentDetails()}>
              <Text style={Styles.button_font}>Save</Text>
            </TouchableOpacity>
            <View style={{width: 1, backgroundColor: COLOR.RED}} />
          </View>
          <ActionSheet
            modalVisible={this.state.actionModalVisible}
            handleSheet={this.handleSheet}
          />
        </SafeAreaView>
        {/* <SuccessPopup
          handleOk={this.handleGetin}
          modalVisible={this.state.modalVisible}
        /> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  vehicle_image: {
    height: hp(35),
    width: wp(100),
    alignSelf: 'center',
    marginTop: -2,
    overflow: 'hidden',
  },
  back_arrow_view: {
    marginTop: getStatusBarHeight(true),
    width: wp(100),
    alignSelf: 'center',
    height: 45,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  image_count_view: {
    height: 45,
    width: 60,
    borderRadius: 10,
    backgroundColor: '#00000080',
    marginRight: 10,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: COLOR.BLACK_GRAD,
    shadowOpacity: 0.2,
    elevation: 3,
    shadowOffset: {width: 0.2, height: 0.2},
  },
  image_collection: {
    width: wp(90),
    alignSelf: 'center',
    flexDirection: 'row',
    shadowColor: COLOR.BLACK_GRAD,
    shadowOpacity: 0.2,
    elevation: 3,
    shadowOffset: {width: 0.2, height: 0.2},
  },
  flatlist_view: {
    width: wp(100),
    flex: 1,
  },
  navigation_bar: {
    height: getStatusBarHeight(true) + 45,
    backgroundColor: COLOR.DARK_BLUE,
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: getStatusBarHeight(true),
    justifyContent: 'space-between',
  },
});

const mapStateToProps = (state, ownProps) => {
  return {
    inspectionData: state.reducer.inspectionDetail,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getInspectionDetail: (data, val) => {
      dispatch(Actions.inspectionDetail(data, val));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Exterior);
