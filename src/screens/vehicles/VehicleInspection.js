import React from 'react';
import {
    View,
    Image,
    Text,
    StyleSheet,
    SafeAreaView,
    FlatList,
    TouchableOpacity,
    Share,
} from 'react-native';
import {
    heightPercentageToDP as hp,
    widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import IMAGES from '../../res/styles/Images';
import Styles from '../../res/styles/Styles';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import COLOR from '../../res/styles/Color';
import {BackView} from '../../commonView/BackView';
import {AnimatedCircularProgress} from 'react-native-circular-progress';
import FONTS from '../../res/styles/Fonts';
import {
    ScrollView,
} from 'react-native-gesture-handler';
import Accordion from 'react-native-collapsible/Accordion';
import {insertComma} from '../../commonView/Helpers';
import Moment from 'moment';
import {connect} from 'react-redux';
import Actions from '../../redux/actions/Actions';
import GalleryView from '../../commonView/GalleryView';
import {Pages} from 'react-native-pages';
import FastImage from 'react-native-fast-image';
import API from '../../api/Api';

let api = new API();

class VehicleInspection extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showHideSubview: [],
            currentImageIndex: 0,
            isGalleryViewVisible: false,
            activeSections: [],
            collapsed: true,
            multipleSelect: false,
            vehicleData: null,
            vehicleType: [],
            parameter: [],
            galleryData: [],
            index: null,
        };
        this.toggleExpanded = this.toggleExpanded.bind(this);
        this.handleVehicleEdit = this.handleVehicleEdit.bind(this);
        this.handleGallery = this.handleGallery.bind(this);
        this.handleGalleryIndex = this.handleGalleryIndex.bind(this);
        this.handleShowhideSubview = this.handleShowhideSubview.bind(this);
        this.renderContent = this.renderContent.bind(this);
        this.getCommisionPrice = this.getCommisionPrice.bind(this);
        this.onShare = this.onShare.bind(this);
    }

    componentDidMount() {
        if (this.props.route.params && this.props.route.params.data) {
            console.log('data is', this.props.route.params.data.inspectionPdf);
            this.setState({
                vehicleData: this.props.route.params.data,
            });
        }
    }

    handleVehicleEdit() {
        this.props.inspectionDetail(this.state.vehicleData, 2);
        this.props.navigation.navigate('VehicleDetail');
    }

    commonView = (props) => {
        return (
            <TouchableOpacity
                onPress={() => {
                    props.type == 'Odometer Reading' &&
                    this.handleGallery(
                        2,
                        Array({
                            path: this.state.vehicleData
                                ? this.state.vehicleData.odometerReadingImage
                                : '',
                        }),
                    );
                }}>
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'flex-start',
                        margin: 0,
                        marginHorizontal: 5,
                    }}>
                    <Image
                        source={
                            props.type == 'Odometer Reading'
                                ? {
                                    uri: this.state.vehicleData
                                        ? this.state.vehicleData.odometerReadingImage
                                        : '',
                                }
                                : props.image
                        }
                        resizeMode={'contain'}
                        style={{
                            height: 25,
                            width: 25,
                            marginRight: 10,
                            marginTop: 5,
                            overflow: 'hidden',
                        }}
                    />
                    <View style={{margin: 0}}>
                        <Text
                            style={[
                                Styles.medium_label,
                                {
                                    width: null,
                                    paddingRight: 30,
                                },
                            ]}>
                            {props.name}
                        </Text>
                        <Text
                            style={[
                                Styles.small_label,
                                {textAlign: 'left', color: COLOR.GRAY, width: null},
                            ]}>
                            {props.type}
                        </Text>
                    </View>
                </View>
            </TouchableOpacity>
        );
    };

    toggleExpanded = () => {
        this.setState({collapsed: !this.state.collapsed});
    };

    handleShowhideSubview = (index) => {
        let data = this.state.showHideSubview;
        data[index] = !data[index];
        this.setState({
            showHideSubview: data,
        });
    };

    renderHeader = (section, _, active) => {
        return (
            <View
                style={[
                    styles.image_collection,
                    {
                        backgroundColor: COLOR.WHITE,
                        padding: 15,
                        borderRadius: 10,
                        borderTopLeftRadius: 10,
                        borderTopRightRadius: 10,
                        borderBottomLeftRadius: active ? 0 : 10,
                        borderBottomRightRadius: active ? 0 : 10,
                        flexDirection: 'column',
                        marginTop: 20,
                    },
                ]}>
                <View
                    style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                    }}>
                    <View
                        style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                        }}>
                        <Image
                            style={{
                                width: 40,
                                height: 40,
                                marginRight: 10,
                                borderRadius: 10,
                                overflow: 'hidden',
                            }}
                            source={section.image}
                            resizeMode={'contain'}
                        />
                        <Text style={Styles.medium_label}>{section.name}</Text>
                    </View>

                    <Image
                        style={{width: 20, height: 20}}
                        source={active ? IMAGES.minus_icon : IMAGES.plus_icon}
                        resizeMode={'contain'}
                    />
                </View>
                {active && <View style={[Styles.line_view, {marginTop: 20}]} />}
            </View>
        );
    };


    renderContent(section) {
        let that = this;
        return section.subcategory.map((data, index) => {
            return (
                <View>
                    <TouchableOpacity onPress={() => this.handleShowhideSubview(index)}>
                        <View
                            style={{
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                                alignItems: 'center',
                                marginBottom: 20,
                                width: wp(90),
                                alignSelf: 'center',
                                borderBottomLeftRadius: 10,
                                borderBottomRightRadius: 10,
                                paddingHorizontal: 15,
                                backgroundColor: COLOR.WHITE,
                            }}>
                            <View>
                                <Text style={[Styles.medium_label]}>{data.name}</Text>
                            </View>
                            <Image
                                style={{height: 30, width: 30}}
                                source={
                                    data.issueCount == 0 ? IMAGES.tick_green : IMAGES.alert_icon
                                }
                                resizeMode={'contain'}
                            />
                        </View>
                    </TouchableOpacity>
                    {that.state.showHideSubview[index] && (
                        <View
                            style={{
                                width: wp(90),
                                alignItems: 'center',
                                justifyContent: 'center',
                                paddingHorizontal: 15,
                            }}>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    marginBottom: 20,
                                    borderRadius: 10,
                                    overflow: 'hidden',
                                    height: data.files.length > 0 ? 160 : 0,
                                    width: wp(85),
                                }}>
                                {data.files.length > 0 ? (
                                    <View style={{flex: 1, flexDirection: 'column'}}>
                                        <Pages
                                            indicatorColor={COLOR.LIGHT_BLUE}
                                            indicatorOpacity={0.2}>
                                            {data.files.map((element) => {
                                                return (
                                                    <View style={{flex: 1}}>
                                                        <TouchableOpacity
                                                            onPress={() => this.handleGallery(2, data.files)}>
                                                            <FastImage
                                                                style={{
                                                                    overflow: 'hidden',
                                                                    width: wp(85),
                                                                    alignSelf: 'center',
                                                                    height: 160,
                                                                }}
                                                                source={{
                                                                    uri: element.path,
                                                                    priority: FastImage.priority.high,
                                                                }}
                                                                resizeMode={FastImage.resizeMode.cover}
                                                            />
                                                        </TouchableOpacity>
                                                    </View>
                                                );
                                            })}
                                        </Pages>

                                        <View
                                            style={{
                                                height: 40,
                                                width: 40,
                                                backgroundColor: COLOR.WHITE,
                                                borderRadius: 5,
                                                alignItems: 'center',
                                                justifyContent: 'center',
                                                //  top: -150,
                                                //  right: 15,
                                                alignSelf: 'flex-end',
                                                //  position: 'absolute',
                                            }}>
                                            <Image
                                                resizeMode={'stretch'}
                                                style={{
                                                    overflow: 'hidden',
                                                    width: 20,
                                                    alignSelf: 'center',
                                                    height: 20,
                                                    tintColor: COLOR.BLACK,
                                                }}
                                                source={IMAGES.camera}
                                            />
                                        </View>
                                    </View>
                                ) : null}
                            </View>
                            {data
                                ? data.options.map((data) => {
                                    return (
                                        data.isSelected == 1 && (
                                            <View
                                                style={{
                                                    width: wp(90),
                                                    alignSelf: 'center',
                                                    borderBottomLeftRadius: 10,
                                                    borderBottomRightRadius: 10,
                                                    paddingHorizontal: 15,
                                                }}>
                                                <View
                                                    style={{
                                                        flexDirection: 'row',
                                                        justifyContent: 'space-between',
                                                        height: 30,
                                                    }}>
                                                    <Text style={[Styles.body_label]}>{data.name}</Text>
                                                    <Image
                                                        style={{
                                                            height: 12,
                                                            width: 12,
                                                        }}
                                                        source={
                                                            data.isSelected == 1
                                                                ? IMAGES.blue_dot
                                                                : IMAGES.circle_boundary
                                                        }
                                                        resizeMode={'contain'}
                                                    />
                                                </View>
                                            </View>
                                        )
                                    );
                                })
                                : null}
                            {data.comment != '' && (
                                <View
                                    style={{
                                        marginVertical: 20,
                                        borderWidth: 0.5,
                                        borderColor: COLOR.GRAY,
                                        borderRadius: 10,
                                        height: 80,
                                        padding: 10,
                                        width: wp(85),
                                    }}>
                                    <Text style={{flex: 1, margin: 0}}>{data.comment}</Text>
                                </View>
                            )}
                            <View style={[Styles.line_view, {marginBottom: 10}]} />
                        </View>
                    )}
                </View>
            );
        });
    }

    updateSection = (activeSections) => {
        this.setState({activeSections});
    };


    getCommisionPrice = (percent, value) => {
        percent = percent / 100;
        value = value * percent;
        return value;
    };

    handleGalleryIndex = () => {
        this.setState({
            index: null,
        });
    };

    handleGallery = (val, data) => {
        let that = this;
        let array = [];
        if (data) {
            for (let i = 0; i < data.length; i++) {
                array.push(data[i].path);
            }
        }
        that.setState({
            isGalleryViewVisible: !that.state.isGalleryViewVisible,
            galleryData: val == 1 ? this.state.vehicleData.files : array,
        });
    };

    onShare = async (link) => {
        try {
            const result = await Share.share({
                title: 'Car Inspection details',
                message: link,
                url: '',
            });
            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    // shared with activity type of result.activityType
                } else {
                    // shared
                }
            } else if (result.action === Share.dismissedAction) {
                // dismissed
            }
        } catch (error) {
            alert(error.message);
        }
    };

    render() {
        const {vehicleData, currentImageIndex, galleryData} = this.state;
        return (
            <ScrollView
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
                style={[Styles.container, {backgroundColor: COLOR.BLUE_BG}]}>
                <View style={Styles.container}>
                    <GalleryView
                        data={galleryData}
                        handleGalleryView={this.handleGallery}
                        modalVisible={this.state.isGalleryViewVisible}
                        index={this.state.index}
                        handleGalleryIndex={this.handleGalleryIndex}
                    />

                    <TouchableOpacity
                        onLongPress={() => {
                        }}
                        onPress={() => {
                            this.handleGallery(1);
                        }}>
                        <FastImage
                            style={styles.vehicle_image}
                            source={
                                vehicleData
                                    ? vehicleData.files.length > currentImageIndex
                                    ? {
                                        uri: vehicleData.files[currentImageIndex],
                                        priority: FastImage.priority.normal,
                                    }
                                    : IMAGES.white_car
                                    : IMAGES.white_car
                            }
                            resizeMode={FastImage.resizeMode.cover}>
                            <View style={styles.back_arrow_view}>
                                <BackView props={this.props} />
                                <View style={styles.image_count_view}>
                                    <Text style={[Styles.body_label, {color: COLOR.WHITE}]}>
                                        {currentImageIndex + 1}/
                                        {vehicleData ? vehicleData.files.length : 0}
                                    </Text>
                                </View>
                            </View>
                        </FastImage>
                    </TouchableOpacity>
                    <SafeAreaView
                        style={[Styles.container, {backgroundColor: COLOR.BLUE_BG}]}>
                        <View
                            style={[styles.image_collection, {height: 60, marginTop: -30}]}>
                            <View
                                style={{
                                    backgroundColor: COLOR.WHITE,
                                    borderRadius: 10,
                                    padding: 5,
                                    marginRight: 10,
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    flex: 1,
                                }}>
                                <FlatList
                                    data={vehicleData ? vehicleData.files : null}
                                    renderItem={({item, index}) => (
                                        <TouchableOpacity
                                            onPress={() => {
                                                this.handleGallery(1);
                                                this.setState({index: index});
                                            }}>
                                            <View
                                                style={[
                                                    {
                                                        justifyContent: 'center',
                                                        alignItems: 'center',
                                                        width: 40,
                                                        aspectRatio: 1,
                                                        alignSelf: 'center',
                                                        marginRight: 10,
                                                    },
                                                ]}>
                                                <FastImage
                                                    style={{
                                                        width: 45,
                                                        height: 45,
                                                        borderRadius: 5,
                                                        overflow: 'hidden',
                                                    }}
                                                    resizeMode={FastImage.resizeMode.cover}
                                                    source={{
                                                        uri: item,
                                                        priority: FastImage.priority.high,
                                                    }}
                                                />
                                            </View>
                                        </TouchableOpacity>
                                    )}
                                    horizontal={true}
                                    showsVerticalScrollIndicator={false}
                                    showsHorizontalScrollIndicator={false}
                                    keyExtractor={(item, index) => index.toString()}
                                />
                            </View>
                            <View
                                style={{
                                    height: 60,
                                    aspectRatio: 1,
                                    borderRadius: 10,
                                    backgroundColor: COLOR.WHITE,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                }}>
                                <AnimatedCircularProgress
                                    style={{backgroundColor: COLOR.WHITE}}
                                    size={40}
                                    width={3}
                                    fill={parseInt(
                                        vehicleData
                                            ? vehicleData.avgRating
                                            ? vehicleData.avgRating
                                            : 0
                                            : 0,
                                    )}
                                    rotation={0}
                                    minva
                                    tintColor={COLOR.LIGHT_BLUE}
                                    backgroundColor={COLOR.LIGHT_GRAY}>
                                    {(fill) => (
                                        <Text
                                            style={[
                                                Styles.small_label,
                                                {
                                                    color: COLOR.BLACK,
                                                    fontFamily: FONTS.FAMILY_BOLD,
                                                },
                                            ]}>
                                            {parseFloat(
                                                vehicleData
                                                    ? vehicleData.avgRating
                                                    ? vehicleData.avgRating / 10
                                                    : 0
                                                    : 0,
                                            ).toFixed(1)}
                                        </Text>
                                    )}
                                </AnimatedCircularProgress>
                            </View>
                        </View>

                        <View
                            style={[
                                styles.image_collection,
                                {
                                    marginTop: 20,
                                    backgroundColor: COLOR.WHITE,
                                    borderRadius: 10,
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                    paddingVertical: 10,
                                    paddingHorizontal: 15,
                                    justifyContent: 'space-between',
                                },
                            ]}>
                            <View style={{flex: 1}}>
                                <Text style={[Styles.medium_label, {marginBottom: 5}]}>
                                    {vehicleData
                                        ? vehicleData.selectedMake
                                            ? vehicleData.selectedMake.label
                                            : vehicleData.make
                                        : ''}{' '}
                                    {vehicleData
                                        ? vehicleData.selectedModel
                                            ? vehicleData.selectedModel.label
                                            : vehicleData.model
                                        : ''}
                                </Text>
                                <Text style={[Styles.body_label, {textAlign: 'left'}]}>
                                    {vehicleData
                                        ? vehicleData.selectedVarient
                                            ? vehicleData.selectedVarient.label
                                            : ''
                                        : ''}
                                </Text>
                            </View>
                            <View style={{flex: 1}}>
                                <Text
                                    style={[
                                        Styles.medium_label,
                                        {
                                            color: COLOR.LIGHT_BLUE,
                                            textAlign: 'right',
                                            marginBottom: 5,
                                            marginLeft: 10,
                                            marginRight: 0,
                                            fontFamily: FONTS.FAMILY_BOLD,
                                        },
                                    ]}>
                                    ₹{' '}
                                    {vehicleData
                                        ? vehicleData.highBid
                                            ? insertComma(
                                                vehicleData.highBid -
                                                parseFloat(
                                                    this.getCommisionPrice(
                                                        vehicleData.commissionPercent,
                                                        vehicleData.highBid,
                                                    ),
                                                ).toFixed(2),
                                            )
                                            : insertComma(vehicleData.expectedPrice)
                                        : ''}
                                </Text>
                                <Text
                                    style={[
                                        Styles.small_label,
                                        {
                                            textAlign: 'right',
                                            color: COLOR.GRAY,
                                            marginLeft: 10,
                                            marginRight: 0,
                                        },
                                    ]}>
                                    posted on{' '}
                                    {vehicleData
                                        ? Moment(vehicleData.createdAt).format('yyyy-MM-DD')
                                        : ''}
                                </Text>
                            </View>
                        </View>

                        <View
                            style={{
                                height: 60,
                                borderRadius: 10,
                                backgroundColor: COLOR.WHITE,
                                justifyContent: 'space-between',
                                alignItems: 'center',
                                width: wp(90),
                                alignSelf: 'center',
                                marginTop: 10,
                                flexDirection: 'row',
                                paddingHorizontal: 10,
                                flex: 1,
                            }}>
                            <View style={{flex: 1}}>
                                <Text
                                    style={[
                                        Styles.small_label,
                                        {
                                            textDecorationLine: 'underline',
                                            color: COLOR.DARK_BLUE,
                                            marginHorizontal: 0,
                                        },
                                    ]}>
                                    {/*{vehicleData ? vehicleData.inspectionPdf*/}
                                    {/*    ? vehicleData.inspectionPdf.substring(*/}
                                    {/*        vehicleData.inspectionPdf.lastIndexOf('/') + 1,*/}
                                    {/*    )*/}
                                    {/*    : '' : ''}*/}
                                    View or share inspection report
                                </Text>
                            </View>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    flex: 1,
                                    justifyContent: 'flex-end',
                                }}>
                                <TouchableOpacity
                                    onPress={() =>
                                        this.props.navigation.navigate('PdfView', {
                                            url: vehicleData ? vehicleData.inspectionPdf : '',
                                            id: vehicleData.id,
                                        })
                                    }
                                    style={{
                                        borderWidth: 1,
                                        borderColor: COLOR.DARK_BLUE,
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                        padding: 5,
                                        flexDirection: 'row',
                                        borderRadius: 5,
                                        marginRight: 10,
                                    }}>
                                    <Image
                                        style={{
                                            height: 15,
                                            width: 15,
                                            marginRight: 10,
                                            tintColor: COLOR.DARK_BLUE,
                                        }}
                                        source={IMAGES.visibility}
                                        resizeMode={'contain'}
                                    />
                                    <Text
                                        style={[
                                            Styles.small_label,
                                            {textAlign: 'center', color: COLOR.DARK_BLUE},
                                        ]}>
                                        {'View'}
                                    </Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    onPress={() => this.onShare(vehicleData.inspectionPdf)}
                                    style={{
                                        borderWidth: 1,
                                        borderColor: COLOR.LIGHT_BLUE,
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                        padding: 5,
                                        flexDirection: 'row',
                                        borderRadius: 5,
                                    }}>
                                    <Image
                                        style={{
                                            height: 15,
                                            width: 15,
                                            marginRight: 10,
                                            tintColor: COLOR.LIGHT_BLUE,
                                        }}
                                        source={IMAGES.share}
                                        resizeMode={'contain'}
                                    />
                                    <Text
                                        style={[
                                            Styles.small_label,
                                            {textAlign: 'center', color: COLOR.LIGHT_BLUE},
                                        ]}>
                                        {'Share'}
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        </View>

                        <View
                            style={[
                                styles.image_collection,
                                {
                                    backgroundColor: COLOR.WHITE,
                                    flexDirection: 'column',
                                    marginVertical: 20,
                                    padding: 15,
                                    borderRadius: 10,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    paddingHorizontal: 0,
                                },
                            ]}>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                    alignItems: 'center',
                                    marginTop: 0,
                                    marginHorizontal: 5,
                                    marginBottom: 30,
                                }}>
                                <View style={{flex: 1}}>
                                    <this.commonView
                                        image={IMAGES.blue_fuel}
                                        name={vehicleData ? vehicleData.fuelType : ''}
                                        type={'Fuel Type'}
                                    />
                                </View>
                                <View style={{flex: 1}}>
                                    <this.commonView
                                        image={IMAGES.blue_numberplate}
                                        name={vehicleData ? vehicleData.rtoState : ''}
                                        type={'RTO'}
                                    />
                                </View>
                            </View>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                    alignItems: 'center',
                                    marginTop: 0,
                                    marginHorizontal: 5,
                                    marginBottom: 30,
                                }}>
                                <View style={{flex: 1}}>
                                    <this.commonView
                                        image={IMAGES.blue_transmission}
                                        name={vehicleData ? vehicleData.transmission : ''}
                                        type={'Transmission'}
                                    />
                                </View>
                                <View
                                    style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
                                    <this.commonView
                                        image={IMAGES.speedometer}
                                        name={
                                            vehicleData
                                                ? `${insertComma(vehicleData.odometerReading)} Kms`
                                                : ''
                                        }
                                        type={'Odometer Reading'}
                                    />
                                </View>
                            </View>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                    alignItems: 'center',
                                    marginTop: 0,
                                    marginHorizontal: 5,
                                    marginBottom: 30,
                                }}>
                                <View style={{flex: 1}}>
                                    <this.commonView
                                        image={IMAGES.color_picker}
                                        name={vehicleData ? vehicleData.color.toUpperCase() : ''}
                                        type={'Color'}
                                    />
                                </View>
                                <View style={{flex: 1}}>
                                    <this.commonView
                                        image={IMAGES.car_location}
                                        name={
                                            vehicleData ? vehicleData.pickupAddress.toUpperCase() : ''
                                        }
                                        type={'Loction'}
                                    />
                                </View>
                            </View>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                    alignItems: 'center',
                                    marginTop: 0,
                                    marginHorizontal: 5,
                                    marginBottom: 30,
                                }}>
                                <View style={{flex: 1}}>
                                    <this.commonView
                                        image={IMAGES.blue_user}
                                        name={vehicleData ? vehicleData.numberOfOwners : ''}
                                        type={'No. of Owners'}
                                    />
                                </View>
                                <View style={{flex: 1}}>
                                    <this.commonView
                                        image={IMAGES.car_key}
                                        name={vehicleData ? vehicleData.noOfKeys : ''}
                                        type={'No. of Keys'}
                                    />
                                </View>
                            </View>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                    alignItems: 'center',
                                    marginTop: 0,
                                    marginHorizontal: 5,
                                    marginBottom: 30,
                                }}>
                                <View style={{flex: 1}}>
                                    <this.commonView
                                        image={IMAGES.crash}
                                        name={
                                            vehicleData
                                                ? vehicleData.accidental == 'true'
                                                ? 'YES'
                                                : 'NO'
                                                : ''
                                        }
                                        type={'Accidental'}
                                    />
                                </View>
                                <View style={{flex: 1}}>
                                    <this.commonView
                                        image={IMAGES.id_card}
                                        name={vehicleData ? vehicleData.rcAvailable : ''}
                                        type={'RC Availability'}
                                    />
                                </View>
                            </View>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                    alignItems: 'center',
                                    marginTop: 0,
                                    marginHorizontal: 5,
                                    marginBottom: 30,
                                }}>
                                {/* <View style={{flex: 1}}>
                  <this.commonView
                    image={IMAGES.blue_calendar}
                    name={vehicleData ? vehicleData.manufacturingDate : ''}
                    type={'Manufacturing Date'}
                  />
                </View> */}
                                <View style={{flex: 1}}>
                                    <this.commonView
                                        image={IMAGES.blue_calendar}
                                        name={vehicleData ? vehicleData.manufacturingYear : ''}
                                        type={'Manufacturing Year'}
                                    />
                                </View>
                                <View style={{flex: 1}}>
                                    <this.commonView
                                        image={IMAGES.blue_calendar}
                                        name={vehicleData ? vehicleData.registrationDate : ''}
                                        type={'Registration Date'}
                                    />
                                </View>
                            </View>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                    alignItems: 'center',
                                    marginTop: 0,
                                    marginHorizontal: 5,
                                    marginBottom: 30,
                                }}>
                                <View style={{flex: 1}}>
                                    <this.commonView
                                        image={IMAGES.blue_clock}
                                        name={vehicleData ? vehicleData.insuranceValidityDate : ''}
                                        type={'Insurance'}
                                    />
                                </View>
                                <View style={{flex: 1}}>
                                    <this.commonView
                                        image={IMAGES.blue_shield}
                                        name={vehicleData ? vehicleData.typeOfInsurance : ''}
                                        type={'Insurance Type'}
                                    />
                                </View>
                            </View>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                    alignItems: 'center',
                                    marginTop: 0,
                                    marginHorizontal: 5,
                                }}>
                                <View style={{flex: 1}}>
                                    <this.commonView
                                        image={IMAGES.id_card}
                                        name={
                                            vehicleData
                                                ? vehicleData.underHypothecation == 'true'
                                                ? 'YES'
                                                : 'NO'
                                                : ''
                                        }
                                        type={'Under Hypothecation'}
                                    />
                                </View>
                                <View style={{flex: 1}}>
                                    <this.commonView
                                        image={IMAGES.id_card}
                                        name={
                                            vehicleData
                                                ? vehicleData.rcMismatch.toUpperCase() == 'TRUE' ||
                                                vehicleData.rcMismatch.toUpperCase() == 'YES'
                                                ? 'YES'
                                                : 'NO'
                                                : ''
                                        }
                                        type={'RC Mismatch'}
                                    />
                                </View>
                            </View>
                        </View>
                        <View
                            style={[
                                Styles.shadow_view,
                                {
                                    width: wp(90),
                                    alignSelf: 'center',
                                    padding: 15,
                                    marginBottom: 20,
                                },
                            ]}>
                            <Text style={[Styles.medium_label, {marginBottom: 10}]}>
                                Car Summary
                            </Text>
                            <Text style={Styles.body_label}>
                                {vehicleData ? vehicleData.remark : ''}
                            </Text>
                        </View>
                        <View style={{marginBottom: 10, borderRadius: 10}}>
                            {vehicleData && (
                                <Accordion
                                    style={{
                                        backgroundColor: COLOR.WHITE,
                                        width: wp(90),
                                        alignSelf: 'center',
                                    }}
                                    activeSections={this.state.activeSections}
                                    sections={vehicleData ? vehicleData.bodyFrame : []}
                                    renderHeader={this.renderHeader}
                                    renderContent={this.renderContent}
                                    onChange={this.updateSection}
                                    underlayColor={'#00000000'}
                                />
                            )}
                        </View>

                        {/* {vehicleData
              ? vehicleData.approvedForBid == false && (
                  <View
                    style={{
                      width: wp(100),
                      height: 55,
                      flexDirection: 'row',
                      backgroundColor: COLOR.LIGHT_BLUE,
                      alignItems: 'center',
                      justifyContent: 'space-between',
                      marginVertical: 0,
                      marginBottom: 0,
                    }}>
                    <TouchableOpacity
                     // onPress={this.handleVehicleEdit}
                      style={{flex: 1}}>
                      <Text style={Styles.button_font}>Edit</Text>
                    </TouchableOpacity>
                  </View>
                )
              : null} */}
                    </SafeAreaView>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    vehicle_image: {
        height: hp(35),
        width: wp(100),
        alignSelf: 'center',
        marginTop: -2,
        overflow: 'hidden',
    },
    back_arrow_view: {
        marginTop: getStatusBarHeight(true)+20,
        width: wp(100),
        alignSelf: 'center',
        height: 45,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    image_count_view: {
        height: 45,
        width: 60,
        borderRadius: 10,
        backgroundColor: '#00000080',
        marginRight: 10,
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: COLOR.BLACK_GRAD,
        shadowOpacity: 0.2,
        elevation: 3,
        shadowOffset: {width: 0.2, height: 0.2},
    },
    image_collection: {
        width: wp(90),
        alignSelf: 'center',
        flexDirection: 'row',
        shadowColor: COLOR.BLACK_GRAD,
        shadowOpacity: 0.2,
        elevation: 3,
        shadowOffset: {width: 0.2, height: 0.2},
    },
    flatlist_view: {
        width: wp(100),
        flex: 1,
    },
});

const mapDispatchToProps = (dispatch) => {
    return {
        inspectionDetail: (data, type) => {
            dispatch(Actions.inspectionDetail(data, type));
        },
    };
};

export default connect(null, mapDispatchToProps)(VehicleInspection);
