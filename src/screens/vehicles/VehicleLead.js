import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  FlatList,
  TextInput,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';

import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import COLOR from '../../res/styles/Color';
import FONTS from '../../res/styles/Fonts';
import IMAGES from '../../res/styles/Images';
import Styles from '../../res/styles/Styles';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {AnimatedCircularProgress} from 'react-native-circular-progress';
import {BackView} from '../../commonView/BackView';
import Actions from '../../redux/actions/Actions';
import {connect} from 'react-redux';
import {insertComma} from '../../commonView/Helpers';

class VehicleLeads extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedIndex: 0,
      carData: [],
    };

    this.props.getListedVehicles(1,0);
  }

  componentDidMount() {
    this.setState({
      carData: this.props.listedVehicleData,
    });
  }

  componentWillReceiveProps(props) {
    if (props.listedVehicleData != this.props.listedVehicleData) {
      this.setState({
        carData: props.listedVehicleData,
      });
    }
  }

  totalView = (props) => {
    return (
      <View
        style={{
          borderRadius: 10,
          backgroundColor: COLOR.WHITE,
          shadowOffset: {width: 0.2, height: 0.2},
          shadowColor: COLOR.GRAY,
          shadowOpacity: 0.2,
          width: wp(27),
          height: 100,
          elevation: 3,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <Text
          style={[
            Styles.heading_label,
            {color: COLOR.LIGHT_BLUE, fontSize: 26},
          ]}>
          {props.count}
        </Text>
        <Text style={[Styles.small_label, {fontFamily: FONTS.FAMILY_SEMIBOLD}]}>
          {props.name}
        </Text>
      </View>
    );
  };

  render() {
    const {carData,pageLoading,currentPage} = this.state;
    return (
      <View style={[Styles.container, {backgroundColor: COLOR.BLUE_BG}]}>
        <View style={styles.navigation_bar}>
          <View style={{flexDirection: 'row'}}>
            <BackView props={this.props} />
            <Text style={[Styles.button_font]}>Leads</Text>
          </View>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('VehicleDetail')}>
            <View
              style={{
                paddingHorizontal: 10,
                height: 30,
                borderRadius: 15,
                backgroundColor: COLOR.LIGHT_BLUE,
                justifyContent: 'center',
                marginRight: 15,
              }}>
              <Text
                style={[
                  Styles.small_label,
                  {color: COLOR.WHITE, fontFamily: FONTS.FAMILY_SEMIBOLD},
                ]}>
                + Add New Leads
              </Text>
            </View>
          </TouchableOpacity>
        </View>
        <View
          style={{
            flex: 1,
            backgroundColor: COLOR.BLUE_BG,
            paddingTop: 10,
          }}>
          <FlatList
           onScroll={e => {
            let paddingToBottom = 10;
            paddingToBottom += e.nativeEvent.layoutMeasurement.height;
            var currentOffset = e.nativeEvent.contentOffset.y;
            var direction = currentOffset > this.offset ? 'down' : 'up';
            if (direction === 'up') {
              if (
                e.nativeEvent.contentOffset.y >=
                e.nativeEvent.contentSize.height - paddingToBottom
              ) {
                if(!pageLoading){
                  this.setState(
                    {
                      pageLoading:true,
                      currentPage: this.state.currentPage + 1,
                    },
                    () => {
                      setTimeout(() => {
                        this.props.getListedVehicles(2,this.state.currentPage);
                      }, 1000);
                    },
                  );
                }
              }
            }
          }}
            style={[styles.flatlist_view, {alignSelf: 'center', flex: 1}]}
            data={carData}
            renderItem={({item}) => (
              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate('VehicleInspection', {
                    data: item,
                  })
                }
                style={[styles.car_view]}>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'flex-start',
                  }}>
                  <Image
                    source={
                      item.files.length > 0
                        ? {uri: item.files[0]}
                        : IMAGES.white_car
                    }
                    resizeMode={'cover'}
                    style={{
                      width: 110,
                      marginRight: 10,
                      height: 80,
                      borderRadius: 10,
                    }}
                  />
                  <View style={{flex: 1, marginVertical: 0}}>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'flex-start',
                        flex: 1,
                      }}>
                      <View style={{flex: 1}}>
                        <Text
                          style={[
                            Styles.body_label,
                            {
                              width: null,
                              alignSelf: 'flex-start',
                            },
                          ]}>
                          {item.selectedModel ? item.selectedModel.label : ''}
                        </Text>
                        <Text
                          style={[
                            Styles.body_label,
                            {
                              fontFamily: FONTS.FAMILY_SEMIBOLD,
                              width: null,
                              alignSelf: 'flex-start',
                            },
                          ]}>
                          ₹ {insertComma(String(item.expectedPrice))}
                        </Text>
                      </View>
                      <AnimatedCircularProgress
                        style={{
                          backgroundColor: COLOR.WHITE,
                          marginLeft: 10,
                          marginRight: 0,
                        }}
                        size={35}
                        width={3}
                        fill={parseInt(item.avgRating ? item.avgRating : 0)}
                        rotation={0}
                        minva
                        tintColor={COLOR.LIGHT_BLUE}
                        backgroundColor={COLOR.LIGHT_GRAY}>
                        {(fill) => (
                          <Text
                            style={[
                              Styles.small_label,
                              {
                                color: COLOR.LIGHT_BLUE,
                                fontFamily: FONTS.FAMILY_SEMIBOLD,
                              },
                            ]}>
                            {parseInt(item.avgRating ? item.avgRating : 0)}
                          </Text>
                        )}
                      </AnimatedCircularProgress>
                    </View>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        marginBottom: 0,
                      }}>
                      <Text
                        style={[
                          Styles.small_label,
                          {
                            color: COLOR.BLACK,
                            width: null,
                            fontFamily: FONTS.FAMILY_MEDIUM,
                          },
                        ]}>
                        {insertComma(String(item.odometerReading))} Kms
                      </Text>
                      <Text
                        style={[
                          Styles.small_label,
                          {
                            color: COLOR.LIGHT_BLUE,
                            width: null,
                            fontFamily: FONTS.FAMILY_MEDIUM,
                          },
                        ]}>
                        |
                      </Text>
                      <Text
                        style={[
                          Styles.small_label,
                          {
                            color: COLOR.BLACK,
                            width: null,
                            fontFamily: FONTS.FAMILY_MEDIUM,
                          },
                        ]}>
                        {item.fuelType}
                      </Text>
                      <Text
                        style={[
                          Styles.small_label,
                          {
                            color: COLOR.LIGHT_BLUE,
                            width: null,
                            fontFamily: FONTS.FAMILY_MEDIUM,
                          },
                        ]}>
                        |
                      </Text>
                      <Text
                        style={[
                          Styles.small_label,
                          {
                            color: COLOR.BLACK,
                            width: null,
                            fontFamily: FONTS.FAMILY_MEDIUM,
                          },
                        ]}>
                        {item.numberOfOwners == 1
                          ? `${item.numberOfOwners}st`
                          : item.numberOfOwners == 2
                          ? `${item.numberOfOwners}nd`
                          : item.numberOfOwners == 3
                          ? `${item.numberOfOwners}rd`
                          : `${item.numberOfOwners}th`}{' '}
                        owner
                      </Text>
                    </View>
                  </View>
                </View>
              </TouchableOpacity>

              // <TouchableOpacity
              //   onPress={() => {
              //     this.props.inspectionDetail(item, 2);
              //     this.props.navigation.navigate('VehicleInspection');
              //   }}>
              //   <View style={styles.car_on_bid}>
              //     <View
              //       style={{
              //         flexDirection: 'row',
              //         alignItems: 'flex-start',
              //         padding: 15,
              //       }}>
              //       <Image
              //         style={[styles.image_view, {marginRight: 15}]}
              //         resizeMode={'cover'}
              //         source={
              //           item.files.length > 0
              //             ? {uri: item.files[0]}
              //             : IMAGES.white_car
              //         }
              //       />
              //       <View style={{flex: 1}}>
              //         <Text style={Styles.medium_label}>
              //           {item.selectedModel ? item.selectedModel.label : ''}
              //         </Text>
              //         <Text
              //           style={[Styles.small_label, {color: COLOR.LIGHT_TEXT}]}>
              //           {item.customer ? item.customer.inspectionAddress : ''}
              //         </Text>
              //         <View
              //           style={{
              //             flexDirection: 'row',
              //             justifyContent: 'space-between',
              //           }}>
              //           <Text
              //             style={[
              //               Styles.body_label,
              //               {fontFamily: FONTS.FAMILY_MEDIUM},
              //             ]}>
              //             {item.customer ? item.customer.fullName : ''}
              //           </Text>
              //           <Text style={[Styles.small_label]}>
              //             {item.customer ? item.customer.mobile : ''}
              //           </Text>
              //         </View>
              //       </View>
              //     </View>
              //     <View style={[Styles.line_view]} />
              //     <View
              //       style={{
              //         flexDirection: 'row',
              //         justifyContent: 'space-between',
              //         marginVertical: 15,
              //         paddingHorizontal: 15,
              //       }}>
              //       <Text
              //         style={[
              //           Styles.small_label,
              //           {fontFamily: FONTS.FAMILY_SEMIBOLD},
              //         ]}>
              //         Date - {item.inspectionDate}
              //       </Text>
              //       <Text
              //         style={[
              //           Styles.small_label,
              //           {fontFamily: FONTS.FAMILY_SEMIBOLD},
              //         ]}>
              //         Time - {item.inspectionTime}
              //       </Text>
              //     </View>
              //   </View>
              // </TouchableOpacity>
            )}
            numColumns={1}
            showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
        <View
          style={{
            flexDirection: 'row',
            width: wp(90),
            alignSelf: 'center',
            justifyContent: 'space-between',
            marginVertical: 20,
          }}>
          <this.totalView count={'0'} name={'New Leads'} />
          <this.totalView count={'0'} name={'Inspected'} />
          <this.totalView count={carData.length} name={'Total Leads'} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  navigation_bar: {
    height: getStatusBarHeight(true) + 45,
    backgroundColor: COLOR.DARK_BLUE,
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: getStatusBarHeight(true),
    justifyContent: 'space-between',
  },
  vehicle_type_view: {
    width: wp(90),
    borderRadius: 10,
    backgroundColor: COLOR.WHITE,
    alignSelf: 'center',
    marginVertical: 20,
    shadowColor: COLOR.GRAY,
    shadowOpacity: 0.2,
    elevation: 3,
    shadowOffset: {width: 2, height: 2},
  },
  flatlist_view: {
    width: wp(100),
    marginHorizontal: 10,
    flex: 1,
  },
  car_on_bid: {
    backgroundColor: COLOR.WHITE,
    borderRadius: 10,
    padding: 0,
    width: wp(90),
    alignSelf: 'center',
    marginBottom: 20,
    shadowColor: COLOR.GRAY,
    shadowOpacity: 0.2,
    elevation: 3,
    shadowOffset: {width: 2, height: 2},
  },
  image_view: {
    height: 40,
    width: 40,
    borderRadius: 10,
    backgroundColor: COLOR.LIGHT_BLUE,
  },
  car_view: {
    width: wp(90),
    height: 110,
    backgroundColor: COLOR.WHITE,
    marginBottom: 10,
    borderRadius: 5,
    paddingHorizontal: 10,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: COLOR.BLACK_GRAD,
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.3,
    elevation: 3,
  },
});

const mapStateToProps = (state, ownProps) => {
  return {
    listedVehicleData: state.reducer.listedVehicleData,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getListedVehicles: (val,page) => {
      dispatch(Actions.getListedVehicles(val,page));
    },
    inspectionDetail: (data, type) => {
      dispatch(Actions.inspectionDetail(data, type));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(VehicleLeads);
