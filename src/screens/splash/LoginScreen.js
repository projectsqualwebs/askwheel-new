import React, {useState} from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  Modal,
  ImageBackground,
  TouchableOpacity,
} from 'react-native';

import IMAGES from '../../res/styles/Images';
import Styles from '../../res/styles/Styles';
import {
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import COLOR from '../../res/styles/Color';
import {FloatingTitleTextInputField} from '../../commonView/FloatingTextField';

import {CustomButton} from '../../commonView/CustomButton';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {useNavigation} from '@react-navigation/native';
import Indicator from '../../commonView/ActivityIndicator';

export default function LoginScreen(props) {
  const navigation = useNavigation();
  const [showPass, setShowPass] = useState(true);

  const handleLoginPress = () => {
    props.handleGetin();
  };

  return (
    <Modal
      animationType={'slide'}
      transparent={true}
      visible={props.modalVisible}
      onRequestClose={() => {
        props.handleBack();
      }}>
      <Indicator loading={props.showLoader} />
      <View style={styles.modal}>
        <View style={styles.modalBody}>
          <KeyboardAwareScrollView
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
            bounces={false}>
            <ImageBackground
              style={Styles.backgroundImage}
              resizeMode={'cover'}
              source={IMAGES.bg}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  width: wp(90),
                  alignSelf: 'center',
                  alignItems: 'center',
                }}>
                <Image
                  source={IMAGES.appicon}
                  resizeMode={'contain'}
                  style={styles.appicon_image}
                />
                <TouchableOpacity
                  onPress={props.handleBack}
                  style={[Styles.back_arrow, {borderRadius: 22.5}]}>
                  <Image
                    style={{height: 40, width: 40, borderRadius: 20}}
                    source={IMAGES.left_arrow_white}
                  />
                </TouchableOpacity>
              </View>
              <View
                style={{
                  width: wp(90),
                  alignSelf: 'center',
                  marginVertical: 30,
                }}>
                <Text
                  style={[
                    Styles.heading_label,
                    {color: COLOR.WHITE, marginBottom: 10},
                  ]}>
                  Welcome to Ask Wheels
                </Text>
                <Text style={[Styles.medium_label, {color: COLOR.WHITE}]}>
                  Please enter your account credentials{'\n'}to login to Ask
                  Wheels.
                </Text>
              </View>
              <View style={styles.white_view}>
                <View style={styles.textfield_view}>
                  <View style={[styles.textfield_blueview, {marginBottom: 20}]}>
                    <FloatingTitleTextInputField
                      attrName="email"
                      title="Email Address"
                      keyboardType={'email-address'}
                      value={props.email}
                      updateMasterState={props.handleState}
                    />
                  </View>
                  <View
                    style={[
                      styles.textfield_blueview,
                      {flexDirection: 'row', justifyContent: 'space-between'},
                    ]}>
                    <View style={{flex: 1}}>
                      <FloatingTitleTextInputField
                        attrName="password"
                        title="Password"
                        isSecureTextEntry={showPass}
                        value={props.password}
                        placeholderTextColor={COLOR.RED}
                        updateMasterState={props.handleState}
                      />
                    </View>
                    <TouchableOpacity onPress={() => setShowPass(!showPass)}>
                      <Image
                        style={{height: 30, width: 35}}
                        source={
                          showPass
                            ? IMAGES.visibility_hidden
                            : IMAGES.visibility
                        }
                        resizeMode={'contain'}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
                <CustomButton
                  text={'Get in'}
                  bg={COLOR.LIGHT_BLUE}
                  onPress={handleLoginPress}
                  labelColor={COLOR.WHITE}
                />

                <View style={[styles.forgot_pass_view]}>
                  <Text style={Styles.small_label}>Forgot your password</Text>
                  <TouchableOpacity
                    onPress={() => {
                      props.handleBack();
                      navigation.navigate('ForgetPassword');
                    }}>
                    <Text
                      style={[
                        Styles.body_label,
                        {
                          color: COLOR.LIGHT_BLUE,
                          textDecorationLine: 'underline',
                        },
                      ]}>
                      {' '}
                      Click here
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </ImageBackground>
          </KeyboardAwareScrollView>
        </View>
      </View>
    </Modal>
  );
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    flex: 1,
  },
  appicon_image: {
    height: 90,
    width: 90,
    marginTop: 50,
    marginLeft: 20,
    overflow: 'hidden',
  },
  white_view: {
    width: wp(100),
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    bottom: 0,
    position: 'absolute',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  textfield_view: {
    width: wp(90),
    alignSelf: 'center',
    marginVertical: 50,
  },
  textfield_blueview: {
    paddingHorizontal: 15,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: COLOR.BLUE_SHADOW,
    borderRadius: 10,
    height: 60,
    paddingTop: 5,
  },
  forgot_pass_view: {
    alignItems: 'center',
    flexDirection: 'row',
    marginVertical: 30,
    alignSelf: 'center',
  },
});
