import React, {useState} from 'react';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import COLOR from '../../res/styles/Color';
import Styles from '../../res/styles/Styles';
import {Text, View, Image, SafeAreaView} from 'react-native';
import IMAGES from '../../res/styles/Images';
import {CustomButton} from '../../commonView/CustomButton';
import {TextInput, TouchableOpacity} from 'react-native-gesture-handler';
import Toast from 'react-native-simple-toast';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import API from '../../api/Api';
import {validateEmail} from '../../commonView/Helpers';
import Indicator from '../../commonView/ActivityIndicator';
import {connect} from 'react-redux';
import Actions from '../../redux/actions/Actions';
import {FloatingTitleTextInputField} from '../../commonView/FloatingTextField';
import styles from 'react-native-swipe/styles';

const Api = new API();
class ForgetPassword extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      isLoading: false,
    };
    this.callResetPasswordAPI = this.callResetPasswordAPI.bind(this);
  }

  callResetPasswordAPI = (async) => {
    const {email} = this.state;
    let that = this;
    var data = JSON.stringify({
      email: email.toLowerCase(),
    });

    if (email.length == 0) {
      Toast.show('Enter your Email Address');
    } else if (validateEmail(email) == false) {
      Toast.show('Enter correct Email Address');
    } else {
      that.setState({isLoading: true});
      Api.resetPasswordAPI(data)
        .then((json) => {
          that.setState({isLoading: false});
          let data = JSON.stringify(json.data);
          if (json.status == 200) {
            this.props.navigation.goBack();
              Toast.show(json.data.message);
          }
        })
        .catch(function (error) {
          that.setState({isLoading: false});
          setTimeout(() => {
            Toast.show(error.response.data.message);
          }, 0);
        });
    }
  };

  render() {
    return (
      <View style={[{flex: 1, backgroundColor: COLOR.LIGHT_BLUE}]}>
        <SafeAreaView style={{flex: 1}}>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.goBack();
            }}
            style={[Styles.back_arrow, {marginTop: 0, paddingLeft: 10}]}>
            <Image
              style={{height: 24, width: 24, borderRadius: 20}}
              source={IMAGES.left_arrow_white}
            />
          </TouchableOpacity>
          <View
            style={{
              flexDirection: 'row',
              alignSelf: 'center',
              alignItems: 'center',
              width: wp(90),
              marginVertical: 15,
            }}>
            <Text style={[Styles.heading_label, {color: COLOR.WHITE}]}>
              Forget Password
            </Text>
          </View>
          <Text style={[{marginStart: 20, color: COLOR.WHITE}]}>
            Please Enter Registered Email Address
          </Text>
          <View
            style={[
              {
                margin: 20,
                backgroundColor: COLOR.WHITE,
                borderRadius: 5,
              },
            ]}>
            <View style={[styles.textfield_blueview]}>
              <FloatingTitleTextInputField
                attrName="email"
                title="Email Address"
                keyboardType={'email-address'}
                value={this.state.email}
                updateMasterState={(name, value) => {
                  this.setState({email: value});
                }}
              />
            </View>
          </View>
          <View
            style={{
              marginBottom: 20,
              position: 'absolute',
              bottom: 0,
              left: 0,
              right: 0,
            }}>
            <CustomButton
              text={'Reset password'}
              bg={COLOR.WHITE}
              onPress={() => {
                this.callResetPasswordAPI();
              }}
              labelColor={COLOR.LIGHT_BLUE}
            />
          </View>
          <Indicator loading={this.state.isLoading} />
        </SafeAreaView>
      </View>
    );
  }
}

export default ForgetPassword;
