import React from 'react';
import {Alert, Linking, View, BackHandler} from 'react-native';
import {AS_INITIAL_ROUTE, U_TERMS_CONDITION} from '../../res/Constants';
import {CommonActions} from '@react-navigation/native';
import COLOR from '../../res/styles/Color';
import Auth from '../../asyncStore/Index';
import VersionCheck from 'react-native-version-check';

const auth = new Auth();

export default class InitialLoadingScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      initialRoute: 'SplashScreen',
    };
  }

  async componentDidMount() {
    this.checkVersion();
    const routeName = await auth.getValue(AS_INITIAL_ROUTE);
    if (routeName === null || routeName === '') {
      this.props.navigation.navigate(this.state.initialRoute);
    } else {
      this.props.navigation.dispatch(
        CommonActions.reset({
          index: 0,
          routes: [{name: routeName}],
        }),
      );
    }
  }

  checkVersion = async () => {
    try {
      let updateNeeded = await VersionCheck.needUpdate();
      console.log(
        'update needed:-',
        updateNeeded,
        updateNeeded.latestVersion,
        updateNeeded.currentVersion,
      );
      if (updateNeeded.currentVersion != updateNeeded.latestVersion) {
        Alert.alert(
          'Update Available',
          'New app is available over store please update.',
          [
            {
              text: 'Update',
              onPress: () => {
                Linking.openURL(updateNeeded.storeUrl).catch(() => {
                  Linking.openURL(updateNeeded.storeUrl);
                });
              },
            },
            {
              text: 'Cancel',
              onPress: () => {},
            }
          ],
          {cancelable: true},
        );
      }
    } catch (error) {}
  };

  // Render any loading content that you like here
  render() {
    return (
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          alignSelf: 'center',
          backgroundColor: COLOR.BLACK,
        }}>
        {/* <ImageBackground style={{ width: widthPercentageToDP('100%'), height: heightPercentageToDP('100%') }} source={IMAGES.gradient_icon} >
				</ImageBackground> */}
      </View>
    );
  }
}
