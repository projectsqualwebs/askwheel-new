import React from 'react';
import {
  StatusBar,
  Text,
  View,
  Image,
  SafeAreaView,
  ImageBackground,
  StyleSheet,
    Platform,
    Alert
} from 'react-native';
import {Pages} from 'react-native-pages';
import COLOR from '../../res/styles/Color';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP,
} from 'react-native-responsive-screen';
import Styles from '../../res/styles/Styles';
import IMAGES from '../../res/styles/Images';
import {CustomButton} from '../../commonView/CustomButton';
import LoginScreen from './LoginScreen';
import SignupScreen from './SignupScreen';

import actions from '../../redux/actions/Actions';
import {connect} from 'react-redux';
import Toast from 'react-native-simple-toast';
import API from '../../api/Api';
import Auth from '../../asyncStore/Index';
import {
  AS_USER_TOKEN,
  AS_FCM_TOKEN,
  AS_INITIAL_ROUTE,
} from '../../res/Constants';
import Indicator from '../../commonView/ActivityIndicator';
import {validateEmail} from '../../commonView/Helpers';
import {CommonActions} from '@react-navigation/native';

Platform.select({
  ios: () => StatusBar.setBarStyle('light-content'),
  android: () => StatusBar.setBackgroundColor('#263238'),
})();

const api = new API();
const auth = new Auth();

class SplashScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      isLoading: false,
      modalVisible: false,
      signupModalVisible: false,
      email: '',
      password: '',
      firstName: '',
      lastName: '',
      mobile: '',
      companyName: '',
      confirmPassword: '',
      termsConditionChecked: false,
    };
    this.updateRef = this.updateRef.bind(this);
    this.handleGetin = this.handleGetin.bind(this);
    this.handleLogin = this.handleLogin.bind(this);
    this.forgetPassword = this.forgetPassword.bind(this);
  }

  updateRef(ref) {
    this._pages = ref;
  }

  changePage = () => {
    // console.log('reference is:-', index);
    const {index} = this.state;
    if (this._pages) {
      this.setState(
        {
          index: index + 1,
        },
        () => {
          if (index == 2 || index > 2) {
            this.props.navigation.navigate('LoginScreen');
          } else {
            this._pages.scrollToPage(this.state.index);
          }
        },
      );
    }
  };

  handleLogin = () => {
    this.setState({modalVisible: true});
  };

  handleSignup = () => {
    this.setState({
      signupModalVisible: true,
      email: '',
      password: '',
      firstName: '',
      lastName: '',
      mobile: '',
      companyName: '',
    });
  };

  handleBack = () => {
    this.setState({signupModalVisible: false, modalVisible: false});
  };

  handleTermsCondition = () => {
    this.setState({
      termsConditionChecked: !this.state.termsConditionChecked,
    });
  };

  handleGetin = (val) => {
    const {
      email,
      password,
      firstName,
      lastName,
      mobile,
      confirmPassword,
      companyName,
      termsConditionChecked,
    } = this.state;
    if (firstName.length == 0 && this.state.signupModalVisible) {
      Toast.show('Enter First Name');
    } else if (lastName.length == 0 && this.state.signupModalVisible) {
      Toast.show('Enter Last Name');
    } else if (email.length == 0) {
      Toast.show('Enter your Email Address');
    } else if (validateEmail(email) == false) {
      Toast.show('Enter correct Email Address');
    } else if (mobile.length == 0 && this.state.signupModalVisible) {
      Toast.show('Enter Mobile Number');
    } else if (companyName.length == 0 && this.state.signupModalVisible) {
      Toast.show('Enter Company Name');
    } else if (password.length == 0) {
      Toast.show('Enter password');
    } else if (confirmPassword.length == 0 && this.state.signupModalVisible) {
      Toast.show('Enter confirm password');
    } else if (confirmPassword != password && this.state.signupModalVisible) {
      Toast.show('Password not matched');
    } else if (
      termsConditionChecked == false &&
      this.state.signupModalVisible
    ) {
      Toast.show('Please agree to our Terms & Condition');
    } else {
      this.callLoginApi(val);
    }
  };

  forgetPassword = () => {
    this.props.navigation.navigate('ForgetPassword');
  };

  updateMasterState = (attrName, value) => {
    this.setState({[attrName]: value});
  };

  callLoginApi = async (val) => {
    let that = this;
    const {
      email,
      password,
      firstName,
      lastName,
      mobile,
      companyName,
    } = that.state;
    var data = null;
    const token = await auth.getValue(AS_FCM_TOKEN);

    if (val == 1) {
      data = JSON.stringify({
        email: email.toLocaleLowerCase(),
        password: password,
        token: token,
      });
    } else if (val == 2) {
      data = JSON.stringify({
        email: email.toLocaleLowerCase(),
        password: password,
        firstName: firstName,
        lastName: lastName,
        mobile: mobile,
        token: token,
        companyName: companyName,
      });
    }
    that.setState({isLoading: true});
    api
      .loginAPI(data, val)
      .then((json) => {
        let data = JSON.stringify(json.data);
        that.setState({isLoading: false});
        if (json.status == 200) {
          if (val == 1) {
            setTimeout(() => {
              Toast.show('Logged in Successfully');
            }, 0);
          } else {
            setTimeout(() => {
              Toast.show('User Registered Successfully');
            }, 0);
          }
          auth.setValue(AS_USER_TOKEN, json.data.token);
          auth.setValue(AS_INITIAL_ROUTE, 'HomeStack');
          this.setState({modalVisible: false, signupModalVisible: false});
          that.props.saveUserDetail(json.data);

          this.props.navigation.dispatch(
            CommonActions.reset({
              index: 0,
              routes: [{name: 'HomeStack'}],
            }),
          );
        } else {
          setTimeout(() => {
            Toast.show(json.data.message);
          }, 0);
        }
      })
      .catch(function (error) {
        that.setState({isLoading: false});
        setTimeout(() => {
          Toast.show(
            error.response.data ? error.response.data.message : 'error',
          );
        }, 0);
      });
  };

  render() {
    return (
      <View style={Styles.container}>
        <ImageBackground source={IMAGES.bg} style={Styles.backgroundImage}>
          <SafeAreaView style={{flex: 1}}>
            <Image style={styles.image} source={IMAGES.logo_with_text} />
            <View style={{alignContent: 'center', height: 150, marginTop: 20}}>
              <Pages
                onScrollEnd={(index) => this.setState({index})}
                ref={this.updateRef}
                indicatorColor="#FFFFFF"
                indicatorOpacity={0.2}>
                <Splash1 />
                <Splash1 />
                <Splash1 />
              </Pages>
            </View>
            <View
              style={{
                alignSelf: 'center',
                bottom: 80,
                position: 'absolute',
              }}>
              <CustomButton
                text={'Sign Up'}
                bg={COLOR.WHITE}
                onPress={this.handleSignup}
                labelColor={COLOR.LIGHT_BLUE}
              />
              <View style={{marginTop: 20}}>
                <CustomButton
                  text={'Log In'}
                  bg={COLOR.WHITE}
                  onPress={this.handleLogin}
                  labelColor={COLOR.LIGHT_BLUE}
                />
              </View>
            </View>
            <LoginScreen
              handleGetin={() => this.handleGetin(1)}
              handleBack={this.handleBack}
              forgetPassword={this.forgetPassword}
              handleState={this.updateMasterState}
              email={this.state.email}
              password={this.state.password}
              modalVisible={this.state.modalVisible}
              showLoader={this.state.isLoading}
            />
            <SignupScreen
              handleSignup={() => this.handleGetin(2)}
              handleTermsCondition={this.handleTermsCondition}
              termsConditionChecked={this.state.termsConditionChecked}
              handleState={this.updateMasterState}
              handleBack={this.handleBack}
              email={this.state.email}
              password={this.state.password}
              confirmPassword={this.state.confirmPassword}
              firstName={this.state.firstName}
              lastName={this.state.lastName}
              mobile={this.state.mobile}
              companyName={this.state.companyName}
              modalVisible={this.state.signupModalVisible}
              showLoader={this.state.isLoading}
            />

          </SafeAreaView>
        </ImageBackground>
      </View>
    );
  }
}

const Splash1 = () => {
  return (
    <View
      style={{
        height: heightPercentageToDP(55),
        alignItems: 'center',
      }}>
      <View style={{marginTop: 20, alignItems: 'center'}}>
        <Text
          style={[
            Styles.subheading_label,
            {textAlign: 'center', color: COLOR.WHITE},
          ]}>
          Matchless deals for wheels
        </Text>
        <Text
          style={[
            Styles.body_label,
            {
              color: COLOR.WHITE,
              marginTop: 10,
              textAlign: 'center',
              width: null,
            },
          ]}>
          Sell your cars easy and fast with us.{'\n'}We assure you best price.
        </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  bottomCircle: {
    height: wp('15%'),
    width: wp('15%'),
    borderRadius: wp('15%') / 2,
    borderWidth: 0.3,
    backgroundColor: COLOR.GREEN,
    opacity: 1,
    shadowRadius: 0.3,
    alignItems: 'center',
    justifyContent: 'center',
  },
  circleImage: {
    alignSelf: 'center',
    width: 25,
    height: 25,
    resizeMode: 'contain',
  },
  image: {
    width: 150,
    height: 150,
    resizeMode: 'contain',
    alignSelf: 'center',
    marginVertical: 50,
  },
});

const mapDispatchToProps = (dispatch) => {
  return {
    saveUserDetail: (data) => {
      dispatch(actions.saveUserDetail(data));
    },
  };
};

export default connect(null, mapDispatchToProps)(SplashScreen);
