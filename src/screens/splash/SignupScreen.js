import React, {useState} from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  Modal,
  ImageBackground,
  TouchableOpacity,
  Linking,
} from 'react-native';

import IMAGES from '../../res/styles/Images';
import Styles from '../../res/styles/Styles';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import COLOR from '../../res/styles/Color';
import {FloatingTitleTextInputField} from '../../commonView/FloatingTextField';
import {CustomButton} from '../../commonView/CustomButton';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {U_TERMS_CONDITION} from '../../res/Constants';
import {SafeAreaView} from 'react-native-safe-area-context';
import Indicator from '../../commonView/ActivityIndicator';

export default function SignupScreen(props) {
  const [showPass, setShowPass] = useState(true);
  const [showConfirmPass, setShowConfirmPass] = useState(true);

  return (
    <Modal
      animationType={'slide'}
      transparent={true}
      visible={props.modalVisible}
      onRequestClose={() => {
        props.handleBack();
      }}>
      <Indicator loading={props.showLoader} />
      <View style={styles.modal}>
        <View style={styles.modalBody}>
          <ImageBackground
            style={Styles.backgroundImage}
            resizeMode={'cover'}
            source={IMAGES.bg}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                marginTop: 20,
              }}>
              <Image
                source={IMAGES.appicon}
                resizeMode={'contain'}
                style={styles.appicon_image}
              />
              <TouchableOpacity
                style={[Styles.back_arrow, {marginTop: 20, marginRight: 10}]}
                onPress={props.handleBack}>
                <Image
                  source={IMAGES.left_arrow_white}
                  resizeMode={'contain'}
                  style={[{height: 30, width: 45}]}
                />
              </TouchableOpacity>
            </View>
            <View
              style={{
                width: wp(90),
                alignSelf: 'center',
                marginVertical: 30,
              }}>
              <Text
                style={[
                  Styles.heading_label,
                  {color: COLOR.WHITE, marginBottom: 10},
                ]}>
                Welcome to Ask Wheels
              </Text>
              <Text style={[Styles.medium_label, {color: COLOR.WHITE}]}>
                Please enter your account credentials{'\n'}to login to Ask
                Wheels.
              </Text>
            </View>
            <SafeAreaView style={styles.white_view}>
              <KeyboardAwareScrollView
                  keyboardShouldPersistTaps={'always'}
                  enableResetScrollToCoords={false}
                style={{flex: 1, flexGrow: 1}}>
                <View style={styles.textfield_view}>
                  <View style={[styles.textfield_blueview, {marginBottom: 20}]}>
                    <FloatingTitleTextInputField
                      attrName="firstName"
                      title="Enter First Name"
                      value={props.firstName}
                      updateMasterState={props.handleState}
                    />
                  </View>
                  <View style={[styles.textfield_blueview, {marginBottom: 20}]}>
                    <FloatingTitleTextInputField
                      attrName="lastName"
                      title="Enter Last Name"
                      value={props.lastName}
                      updateMasterState={props.handleState}
                    />
                  </View>
                  <View style={[styles.textfield_blueview, {marginBottom: 20}]}>
                    <FloatingTitleTextInputField
                      attrName="email"
                      title="Email Address"
                      value={props.email}
                      keyboardType={'email-address'}
                      updateMasterState={props.handleState}
                    />
                  </View>
                  <View style={[styles.textfield_blueview, {marginBottom: 20}]}>
                    <FloatingTitleTextInputField
                      attrName="mobile"
                      title="Mobile Number"
                      value={props.mobile}
                      keyboardType={'number-pad'}
                      updateMasterState={props.handleState}
                    />
                  </View>
                  <View style={[styles.textfield_blueview, {marginBottom: 20}]}>
                    <FloatingTitleTextInputField
                      attrName="companyName"
                      title="Company Name"
                      value={props.companyName}
                      updateMasterState={props.handleState}
                    />
                  </View>

                  <View
                    style={[
                      styles.textfield_blueview,
                      {
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        marginBottom: 20,
                      },
                    ]}>
                    <View style={{flex: 1}}>
                      <FloatingTitleTextInputField
                        attrName="password"
                        title="Password"
                        isSecureTextEntry={showPass}
                        value={props.password}
                        placeholderTextColor={COLOR.RED}
                        updateMasterState={props.handleState}
                      />
                    </View>
                    <TouchableOpacity onPress={() => setShowPass(!showPass)}>
                      <Image
                        style={{height: 30, width: 35}}
                        source={
                          showPass
                            ? IMAGES.visibility_hidden
                            : IMAGES.visibility
                        }
                        resizeMode={'contain'}
                      />
                    </TouchableOpacity>
                  </View>
                  <View
                    style={[
                      styles.textfield_blueview,
                      {flexDirection: 'row', justifyContent: 'space-between'},
                    ]}>
                    <View style={{flex: 1}}>
                      <FloatingTitleTextInputField
                        attrName="confirmPassword"
                        title="Confirm Password"
                        isSecureTextEntry={showConfirmPass}
                        value={props.confirmPassword}
                        placeholderTextColor={COLOR.RED}
                        updateMasterState={props.handleState}
                      />
                    </View>
                    <TouchableOpacity
                      onPress={() => setShowConfirmPass(!showConfirmPass)}>
                      <Image
                        style={{height: 30, width: 35}}
                        source={
                          showConfirmPass
                            ? IMAGES.visibility_hidden
                            : IMAGES.visibility
                        }
                        resizeMode={'contain'}
                      />
                    </TouchableOpacity>
                  </View>
                  <View
                    style={{
                      marginVertical: 15,
                      width: wp(90),
                      alignSelf: 'center',
                      flexDirection: 'row',
                      alignItems: 'center',
                    }}>
                    <TouchableOpacity onPress={props.handleTermsCondition}>
                      <Image
                        style={{
                          height: 20,
                          width: 20,
                          borderRadius: 10,
                          marginRight: 10,
                          borderWidth: 1,
                          borderColor: COLOR.GRAY,
                        }}
                        source={
                          props.termsConditionChecked
                            ? IMAGES.tick_green
                            : IMAGES.circle_boundary
                        }
                      />
                    </TouchableOpacity>
                    <TouchableOpacity
                      onPress={() => {
                        Linking.openURL(U_TERMS_CONDITION);
                      }}>
                      <Text>
                        By clicking you agree to our{' '}
                        <Text
                          style={{
                            textDecorationStyle: 'solid',
                            color: COLOR.LIGHT_BLUE,
                          }}>
                          Terms & Conditions.
                        </Text>
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
                <View style={{marginBottom: 10}}>
                  <CustomButton
                    text={'Sign Up'}
                    bg={COLOR.LIGHT_BLUE}
                    onPress={props.handleSignup}
                    labelColor={COLOR.WHITE}
                  />
                </View>

                {/*<View*/}
                {/*  style={[*/}
                {/*    {*/}
                {/*      alignSelf: 'center',*/}
                {/*      marginBottom: 20,*/}
                {/*      flexDirection: 'row',*/}
                {/*    },*/}
                {/*  ]}>*/}
                {/*  <TouchableOpacity*/}
                {/*    style={[Styles.back_arrow, {borderRadius: 22.5}]}>*/}
                {/*    <Image*/}
                {/*      style={{height: 40, width: 40, borderRadius: 20}}*/}
                {/*      source={IMAGES.facebook}*/}
                {/*    />*/}
                {/*  </TouchableOpacity>*/}
                {/*  <TouchableOpacity*/}
                {/*    style={[*/}
                {/*      Styles.back_arrow,*/}
                {/*      {borderRadius: 22.5, marginHorizontal: 10},*/}
                {/*    ]}>*/}
                {/*    <Image*/}
                {/*      style={{height: 40, width: 40, borderRadius: 20}}*/}
                {/*      source={IMAGES.twitter}*/}
                {/*    />*/}
                {/*  </TouchableOpacity>*/}
                {/*  <TouchableOpacity*/}
                {/*    style={[Styles.back_arrow, {borderRadius: 22.5}]}>*/}
                {/*    <Image*/}
                {/*      style={{height: 40, width: 40, borderRadius: 20}}*/}
                {/*      source={IMAGES.google}*/}
                {/*    />*/}
                {/*  </TouchableOpacity>*/}
                {/*</View>*/}
              </KeyboardAwareScrollView>
            </SafeAreaView>
          </ImageBackground>
        </View>
      </View>
    </Modal>
  );
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    flex: 1,
  },
  appicon_image: {
    height: 70,
    width: 70,
    marginTop: 20,
    marginLeft: 20,
    overflow: 'hidden',
  },
  white_view: {
    width: wp(100),
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    marginVertical: 0,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    flex: 1,
  },
  textfield_view: {
    width: wp(90),
    alignSelf: 'center',
    marginVertical: 50,
  },
  textfield_blueview: {
    paddingHorizontal: 15,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: COLOR.BLUE_SHADOW,
    borderRadius: 10,
    height: 60,
    paddingTop: 5,
  },
  forgot_pass_view: {
    alignItems: 'center',
    flexDirection: 'row',
    marginVertical: 30,
    alignSelf: 'center',
  },
});
